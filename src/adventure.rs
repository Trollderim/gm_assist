use serde::{Serialize, Deserialize};
use crate::{ID, UniquelyIdentifiable};

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct Adventure {
    pub id: ID,
    pub name: String,
    pub description: String,
}

impl UniquelyIdentifiable for Adventure {
    fn get_id(&self) -> ID {
        return self.id.clone();
    }
}

pub type AdventureList = Vec<Adventure>;