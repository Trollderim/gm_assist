use serde::{Serialize, Deserialize};
use crate::{ID, UniquelyIdentifiable};

#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq)]
pub struct MapListEntry {
    pub id: ID,
    pub name: String,
}

impl UniquelyIdentifiable for MapListEntry {
    fn get_id(&self) -> ID {
        self.id.clone()
    }
}

pub type MapList = Vec<MapListEntry>;

/// Exchange structure for annotations on maps
#[derive(Serialize, Deserialize, Clone, Default, Debug, PartialEq)]
pub struct Annotation {
    /// Id of a single annotation. Used for identification purposes.
    pub id: ID,
    pub x: (u32, u32),
    pub y: (u32, u32),
    pub text: String,
    pub associated_map: Option<ID>,
}

impl Annotation {
    pub fn new(map_id: i32) -> Self {
        let mut annotation = Self::default();
        annotation.associated_map = Some(ID::from(map_id));
        return annotation;
    }
}

impl UniquelyIdentifiable for Annotation {
    fn get_id(&self) -> ID {
        self.id
    }
}

pub type AnnotationList = Vec<Annotation>;

pub type Point = (u32, u32);