use serde::{Serialize, Deserialize};
use crate::{ID, UniquelyIdentifiable};

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct Scene {
    pub id: ID,
    pub name: String,
    pub description: String,
    pub treasure_description: String,
    pub check_description: String,
}

impl UniquelyIdentifiable for Scene {
    fn get_id(&self) -> ID {
        return self.id.clone();
    }
}

pub type SceneList = Vec<Scene>;