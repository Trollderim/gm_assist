use serde::{Serialize, Deserialize};
use crate::{ID, UniquelyIdentifiable};

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct Campaign {
    pub id: ID,
    pub name: String,
    pub description: String,
}

impl UniquelyIdentifiable for Campaign {
    fn get_id(&self) -> ID {
        return self.id.clone();
    }
}

pub type CampaignList = Vec<Campaign>;