use serde::{Serialize, Deserialize};
use crate::{ID, UniquelyIdentifiable};
use std::cmp::Ordering;

#[derive(Serialize, Deserialize, Clone, Default, Eq, PartialEq)]
pub struct Event {
    pub id: ID,
    pub name: String,
    pub description: String,
    pub start_year: i64,
    pub start_month: Option<i16>,
    pub start_day: Option<i16>,
    pub end_year: Option<i64>,
    pub end_month: Option<i16>,
    pub end_day: Option<i16>,
}

impl UniquelyIdentifiable for Event {
    fn get_id(&self) -> ID {
        return self.id.clone();
    }
}

impl Ord for Event {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.start_year < other.start_year {
            Ordering::Less
        } else if self.start_year > other.start_year {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    }
}

impl PartialOrd for Event {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.start_year < other.start_year {
            Some(Ordering::Less)
        } else if self.start_year > other.start_year {
            Some(Ordering::Greater)
        } else {
            Some(Ordering::Equal)
        }
    }
}

pub fn format_date(event: &Event) -> String {
    format!("{}-{}-{} - {}-{}-{}", event.start_year,
            event.start_month.unwrap_or_else(|| 0),
            event.start_day.unwrap_or_else(|| 0),
            event.end_year.unwrap_or_else(|| 0),
            event.end_month.unwrap_or_else(|| 0),
            event.end_day.unwrap_or_else(|| 0),
            )
}

pub type Timeline = Vec<Event>;