use serde::{Serialize, Deserialize};

pub mod adventure;

pub mod campaign;

pub mod character;

pub mod location;

pub mod map;

pub mod scene;

pub mod timeline;

#[derive(Default, Clone, Serialize, Deserialize, Debug, Eq, PartialEq, Ord, PartialOrd, Copy)]
pub struct ID {
    id: i32,
}

impl ID {
    pub fn is_set(&self) -> bool {
        return self.id != 0;
    }

    pub fn value(&self) -> i32 {return self.id;}
}

impl From<i32> for ID {
    fn from(other: i32) -> Self {
        Self{
            id: other
        }
    }
}

impl Into<i32> for ID {
    fn into(self) -> i32 {
        self.id
    }
}

pub trait UniquelyIdentifiable {
    fn get_id(&self) -> ID;
}