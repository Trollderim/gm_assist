use serde::{Serialize, Deserialize};
use crate::{ID, UniquelyIdentifiable};

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct Location {
    pub id: ID,
    pub name: String,
    pub description: String,
}

impl UniquelyIdentifiable for Location {
    fn get_id(&self) -> ID {
        return self.id.clone();
    }
}

pub type LocationList = Vec<Location>;