use diesel::{Queryable, Insertable, AsChangeset};
use serde::{Deserialize, Serialize};

use crate::schema::adventures;
use crate::schema::adventure_scenes;

use gm_assist::adventure::Adventure;
use gm_assist::ID;

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Clone)]
#[table_name = "adventures"]
pub struct QueryableAdventure {
    pub id: i32,
    pub name: String,
    pub description: String,
}

impl From<Adventure> for QueryableAdventure {
    fn from(other: Adventure) -> Self {
        Self {
            id: other.id.value() as i32,
            name: other.name,
            description: other.description,
        }
    }
}

impl From<QueryableAdventure> for Adventure {
    fn from(other: QueryableAdventure) -> Self {
        Self {
            id: ID::from(other.id as i32),
            name: other.name.clone(),
            description: other.description,
        }
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "adventures"]
pub struct InsertableAdventure {
    pub name: String,
    pub description: String,
}

impl From<Adventure> for InsertableAdventure {
    fn from(other: Adventure) -> Self {
        Self {
            name: other.name,
            description: other.description,
        }
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "adventure_scenes"]
pub struct InsertableAdventureScene {
    pub adventure_id: i32,
    pub scene_id: i32,
}