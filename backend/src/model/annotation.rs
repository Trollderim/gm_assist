use diesel::{Queryable, Insertable, AsChangeset};
use serde::{Deserialize, Serialize};

use crate::schema::annotations;

use gm_assist::map::{Annotation};
use gm_assist::ID;

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Clone)]
#[table_name = "annotations"]
pub struct QueryableAnnotation {
    pub id: i32,
    pub x_left: i32,
    pub x_right: i32,
    pub y_top: i32,
    pub y_bottom: i32,
    pub content: String,
    pub map_id: Option<i32>,
}

impl QueryableAnnotation {
    pub fn from_annotation(other: Annotation) -> QueryableAnnotation {
        let map_id = match other.associated_map {
            None => {0},
            Some(map_id_other) => {map_id_other.value()},
        };

        QueryableAnnotation {
            id: other.id.value() as i32,
            x_left: other.x.0 as i32,
            x_right: other.x.1 as i32,
            y_top: other.y.0 as i32,
            y_bottom: other.y.1 as i32,
            content: other.text,
            map_id: Some(map_id)
        }
    }
}

impl From<QueryableAnnotation> for Annotation {
    fn from(queryable: QueryableAnnotation) -> Self {
        Annotation{
            id: ID::from(queryable.id),
            x: (queryable.x_left as u32, queryable.x_right as u32),
            y: (queryable.y_top as u32, queryable.y_bottom as u32),
            text: queryable.content.clone(),
            associated_map: queryable.map_id.and_then(|id| Some(ID::from(id)))
        }
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "annotations"]
pub struct InsertableAnnotation {
    pub x_left: i32,
    pub x_right: i32,
    pub y_top: i32,
    pub y_bottom: i32,
    pub content: String,
    pub map_id: Option<i32>,
}

impl From<Annotation> for InsertableAnnotation {
    fn from(annotation: Annotation) -> Self {
        Self {
            x_left: annotation.x.0 as i32,
            x_right: annotation.x.1 as i32,
            y_top: annotation.y.0 as i32,
            y_bottom: annotation.y.1 as i32,
            content: annotation.text.clone(),
            map_id: annotation.associated_map.and_then(|id| Some(id.value() as i32))
        }
    }
}