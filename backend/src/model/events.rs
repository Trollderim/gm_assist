use diesel::{Queryable, Insertable, AsChangeset};
use serde::{Deserialize, Serialize};

use crate::schema::events;

use gm_assist::ID;
use gm_assist::timeline::Event;

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Clone)]
#[table_name = "events"]
pub struct QueryableEvent {
    pub id: i32,
    pub name: String,
    pub description: String,
    pub start_year: i64,
    pub start_month: Option<i16>,
    pub start_day: Option<i16>,
    pub end_year: Option<i64>,
    pub end_month: Option<i16>,
    pub end_day: Option<i16>,
}

impl From<Event> for QueryableEvent {
    fn from(other: Event) -> Self {
        Self {
            id: other.id.into(),
            name: other.name,
            description: other.description,
            start_year: other.start_year,
            start_month: other.start_month,
            start_day: other.start_day,
            end_year: other.end_year,
            end_month: other.end_month,
            end_day: other.end_day,
        }
    }
}

impl From<QueryableEvent> for Event {
    fn from(other: QueryableEvent) -> Self {
        Self {
            id: ID::from(other.id),
            name: other.name.clone(),
            description: other.description,
            start_year: other.start_year,
            start_month: other.start_month,
            start_day: other.start_day,
            end_year: other.end_year,
            end_month: other.end_month,
            end_day: other.end_day,
        }
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "events"]
pub struct InsertableEvent {
    pub name: String,
    pub description: String,
    pub start_year: i64,
    pub start_month: Option<i16>,
    pub start_day: Option<i16>,
    pub end_year: Option<i64>,
    pub end_month: Option<i16>,
    pub end_day: Option<i16>,
}

impl From<Event> for InsertableEvent {
    fn from(other: Event) -> Self {
        Self {
            name: other.name,
            description: other.description,
            start_year: other.start_year,
            start_month: other.start_month,
            start_day: other.start_day,
            end_year: other.end_year,
            end_month: other.end_month,
            end_day: other.end_day,
        }
    }
}