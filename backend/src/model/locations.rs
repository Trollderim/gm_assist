use diesel::{Queryable, Insertable, AsChangeset};
use serde::{Deserialize, Serialize};

use crate::schema::locations;

use gm_assist::location::Location;
use gm_assist::ID;

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Clone)]
#[table_name = "locations"]
pub struct QueryableLocation {
    pub id: i32,
    pub name: String,
    pub description: String,
    pub parent_id: Option<i32>,
}

impl From<Location> for QueryableLocation {
    fn from(other: Location) -> Self {
        Self {
            id: other.id.into(),
            name: other.name,
            description: other.description,
            parent_id: None,
        }
    }
}

impl From<QueryableLocation> for Location {
    fn from(other: QueryableLocation) -> Self {
        Self {
            id: ID::from(other.id),
            name: other.name.clone(),
            description: other.description,
        }
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "locations"]
pub struct InsertableLocation {
    pub name: String,
    pub description: String,
}

impl From<Location> for InsertableLocation {
    fn from(other: Location) -> Self {
        Self {
            name: other.name,
            description: other.description,
        }
    }
}