use diesel::{Queryable, Insertable, AsChangeset};
use serde::{Deserialize, Serialize};

use crate::schema::characters;

use gm_assist::character::Character;
use gm_assist::ID;

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Clone)]
#[table_name = "characters"]
pub struct QueryableCharacter {
    pub id: i32,
    pub name: String,
    pub description: String,
}

impl From<Character> for QueryableCharacter {
    fn from(other: Character) -> Self {
        Self {
            id: other.id.into(),
            name: other.name,
            description: other.description,
        }
    }
}

impl From<QueryableCharacter> for Character {
    fn from(other: QueryableCharacter) -> Self {
        Self {
            id: ID::from(other.id),
            name: other.name.clone(),
            description: other.description,
        }
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "characters"]
pub struct InsertableCharacter {
    pub name: String,
    pub description: String,
}

impl From<Character> for InsertableCharacter {
    fn from(other: Character) -> Self {
        Self {
            name: other.name,
            description: other.description,
        }
    }
}