use diesel::{Queryable, Insertable, AsChangeset};
use serde::{Deserialize, Serialize};

use crate::schema::scenes;
use crate::schema::scene_maps;
use crate::schema::scene_characters;

use gm_assist::scene::Scene;
use gm_assist::ID;

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Clone)]
#[table_name = "scenes"]
pub struct QueryableScene {
    pub id: i32,
    pub name: String,
    pub description: String,
    pub check_description: String,
    pub treasure_description: String,
}

impl From<Scene> for QueryableScene {
    fn from(other: Scene) -> Self {
        Self {
            id: other.id.into(),
            name: other.name,
            description: other.description,
            check_description: other.check_description,
            treasure_description: other.treasure_description,
        }
    }
}

impl From<QueryableScene> for Scene {
    fn from(other: QueryableScene) -> Self {
        Self {
            id: ID::from(other.id),
            name: other.name.clone(),
            description: other.description,
            check_description: other.check_description,
            treasure_description: other.treasure_description,
        }
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "scenes"]
pub struct InsertableScene {
    pub name: String,
    pub description: String,
    pub check_description: String,
    pub treasure_description: String,
}

impl From<Scene> for InsertableScene {
    fn from(other: Scene) -> Self {
        Self {
            name: other.name,
            description: other.description,
            check_description: other.check_description,
            treasure_description: other.treasure_description,
        }
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "scene_maps"]
pub struct InsertableSceneMap {
    pub scene_id: i32,
    pub map_id: i32,
}

#[derive(Deserialize, Insertable)]
#[table_name = "scene_characters"]
pub struct InsertableSceneCharacter {
    pub scene_id: i32,
    pub character_id: i32,
}