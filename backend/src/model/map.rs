use diesel::{Queryable, Insertable, AsChangeset};
use serde::{Deserialize, Serialize};

use crate::schema::maps;

use gm_assist::map::MapListEntry;
use gm_assist::ID;

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Clone)]
#[table_name = "maps"]
pub struct QueryableMap {
    pub id: i32,
    pub name: String,
}

impl QueryableMap {
    pub fn from_map(other: MapListEntry) -> QueryableMap {
        QueryableMap {
            id: other.id.into(),
            name: other.name,
        }
    }
}

impl From<QueryableMap> for MapListEntry {
    fn from(other: QueryableMap) -> Self {
        Self {
            id: ID::from(other.id),
            name: other.name.clone(),
        }
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "maps"]
pub struct InsertableMap {
    pub name: String,
}

impl From<MapListEntry> for InsertableMap {
    fn from(other: MapListEntry) -> Self {
        Self {
            name: other.name
        }
    }
}