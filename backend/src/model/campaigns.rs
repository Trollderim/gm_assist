use diesel::{Queryable, Insertable, AsChangeset};
use serde::{Deserialize, Serialize};

use crate::schema::{campaigns, campaign_adventures};

use gm_assist::campaign::Campaign;
use gm_assist::ID;

#[derive(Serialize, Deserialize, AsChangeset, Queryable, Clone)]
#[table_name = "campaigns"]
pub struct QueryableCampaign {
    pub id: i32,
    pub name: String,
    pub description: String,
}

impl From<Campaign> for QueryableCampaign {
    fn from(other: Campaign) -> Self {
        Self {
            id: other.id.into(),
            name: other.name,
            description: other.description,
        }
    }
}

impl From<QueryableCampaign> for Campaign {
    fn from(other: QueryableCampaign) -> Self {
        Self {
            id: ID::from(other.id),
            name: other.name.clone(),
            description: other.description,
        }
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "campaigns"]
pub struct InsertableCampaign {
    pub name: String,
    pub description: String,
}

impl From<Campaign> for InsertableCampaign {
    fn from(other: Campaign) -> Self {
        Self {
            name: other.name,
            description: other.description,
        }
    }
}

#[derive(Deserialize, Insertable)]
#[table_name = "campaign_adventures"]
pub struct InsertableCampaignAdventure {
    pub campaign_id: i32,
    pub adventure_id: i32,
}