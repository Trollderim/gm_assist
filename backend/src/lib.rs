#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate diesel;

use rocket::{http::Method};
use rocket_cors::{Cors, AllowedOrigins, CorsOptions, AllowedHeaders};
use rocket::fairing::AdHoc;

mod model;
mod schema;
mod routes;

pub mod persistence_macros;

#[database("gm_assist")]
pub struct DbConn(diesel::SqliteConnection);

pub struct DataDir(String);

fn make_cors() -> Cors {
    let allowed_origins = AllowedOrigins::some_exact(&[ // 4.
        "http://127.0.0.1:8000",
        "http://localhost:8000",
    ]);

    CorsOptions { // 5.
        allowed_origins,
        allowed_methods: vec![Method::Delete, Method::Get, Method::Post, Method::Put].into_iter().map(From::from).collect(), // 1.
        allowed_headers: AllowedHeaders::some(&[
            "Authorization",
            "Accept",
            "Access-Control-Allow-Origin",
            "Content-Type",
        ]),
        allow_credentials: true,
        ..Default::default()
    }
        .to_cors()
        .expect("error while building CORS")
}

pub fn rocket() -> rocket::Rocket {
    rocket::ignite()
        .mount(
            "/",
            routes![
                routes::map::map,
                routes::map::maps,
                routes::map::create_map,
                routes::map::create_map_image,
                routes::map::delete_map,
                routes::adventure::create_adventure,
                routes::adventure::create_adventure_scene,
                routes::adventure::delete_adventure,
                routes::adventure::delete_adventure_scene,
                routes::adventure::get_adventures,
                routes::adventure::get_adventure_single,
                routes::adventure::get_adventure_scenes,
                routes::adventure::update_adventure,
                routes::campaign::create_campaign,
                routes::campaign::create_campaign_adventure,
                routes::campaign::delete_campaign,
                routes::campaign::delete_campaign_adventure,
                routes::campaign::get_campaigns,
                routes::campaign::get_campaign_single,
                routes::campaign::get_campaign_adventures,
                routes::campaign::update_campaign,
                routes::character::create_character,
                routes::character::delete_character,
                routes::character::get_characters,
                routes::character::get_character_single,
                routes::character::update_character,
                routes::event::create_event,
                routes::event::delete_event,
                routes::event::get_events,
                routes::event::get_event_single,
                routes::event::update_event,
                routes::location::create_location,
                routes::location::delete_location,
                routes::location::get_locations,
                routes::location::get_location_single,
                routes::location::update_location,
                routes::annotation::create_annotation,
                routes::annotation::delete_annotation,
                routes::annotation::map_annotations,
                routes::annotation::update_annotation,
                routes::scene::create_scene,
                routes::scene::create_scene_map,
                routes::scene::create_scene_character,
                routes::scene::delete_scene,
                routes::scene::delete_scene_map,
                routes::scene::delete_scene_character,
                routes::scene::get_scenes,
                routes::scene::get_scene_maps,
                routes::scene::get_scene_characters,
                routes::scene::get_scene_single,
                routes::scene::update_scene,
            ],
        )
        .attach(make_cors())
        .attach(DbConn::fairing())
        .attach(AdHoc::on_attach("Data Directory Config", |rocket| {
            let data_dir = rocket.config()
                .get_str("data_dir")
                .unwrap_or("data/")
                .to_string();

            Ok(rocket.manage(DataDir(data_dir)))
        }))
}