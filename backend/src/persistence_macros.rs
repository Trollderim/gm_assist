#[macro_export]
macro_rules! get_entry_with_highest_id {
    ($conn:expr, $table:expr) => {
        $table.order(id.desc()).first($conn).unwrap();
    };
}

#[macro_export]
macro_rules! load_from_db {
    ($conn:expr, $table:path) => {
        $table.load($conn).expect("Could not read from database");
    };
}

#[macro_export]
macro_rules! load_from_db_on_match {
    ($conn:expr, $table:path, $field:expr, $value:expr) => {
        $table
            .filter($field.eq($value))
            .load($conn)
            .expect("Could not read from database");
    };
}

#[macro_export]
macro_rules! insert_into_db {
    ($conn:expr, $table:expr, $aggregate:expr) => {
        diesel::insert_into($table)
            .values($aggregate)
            .execute($conn);
    };
}

#[macro_export]
macro_rules! update_db_entry {
    ($conn:expr, $table:expr, $aggregate:expr) => {
        diesel::update($table.find($aggregate.id))
            .set($aggregate)
            .execute($conn);
    };
}