use diesel::{self, prelude::*};

use rocket_contrib::json::{Json};

use crate::model::campaigns::{QueryableCampaign, InsertableCampaign, InsertableCampaignAdventure};

use gm_assist::campaign::{CampaignList, Campaign};

use crate::{get_entry_with_highest_id ,insert_into_db, load_from_db, load_from_db_on_match, update_db_entry};
use crate::{DbConn};
use gm_assist::adventure::AdventureList;
use crate::model::adventures::QueryableAdventure;

#[delete("/campaign", data="<campaign_json>")]
pub fn delete_campaign(conn: DbConn, campaign_json: Json<Campaign>) -> Option<Json<Campaign>> {
    use crate::schema::campaigns::dsl::*;
    use crate::schema::campaign_adventures::dsl::*;

    let campaign = QueryableCampaign::from(campaign_json.into_inner());

    let result = diesel::delete(campaigns.filter(
        crate::schema::campaigns::dsl::id.eq(campaign.id)))
        .execute(&conn.0);

    if result.is_ok() {
        diesel::delete(campaign_adventures.filter(
            crate::schema::campaign_adventures::dsl::campaign_id.eq(campaign.id))).execute(&conn.0)
            .expect("Could not remove adventure associations");
    }

    match result {
        Ok(_) => {Some(Json(QueryableCampaign::into(campaign)))},
        Err(_) => {None},
    }
}

#[delete("/campaign_adventure?<campaign_id_>&<adventure_id_>")]
pub fn delete_campaign_adventure(conn: DbConn, campaign_id_: i32, adventure_id_: i32) -> Option<String> {
    use crate::schema::campaign_adventures::dsl::*;

    let result = diesel::delete(campaign_adventures
        .filter(campaign_id.eq(campaign_id_))
        .filter(adventure_id.eq(adventure_id_)))
        .execute(&conn.0);

    match result {
        Ok(_) => {
            //let _query_adventures = adventures.::<QueryableAdventure>(&conn.0).expect("Could not read from database");
            Some("Deleted association".to_owned())
        },
        Err(_) => {None},
    }
}

#[get("/campaigns")]
pub fn get_campaigns(conn: DbConn) -> Json<CampaignList> {
    use crate::schema::campaigns::dsl::*;

    let _query_campaigns = load_from_db!(&conn.0, campaigns);
    let campaign_list = _query_campaigns.into_iter().map(|campaign: QueryableCampaign| campaign.into()).collect();

    Json(campaign_list)
}

#[get("/campaign/<campaign_id_>")]
pub fn get_campaign_single(conn: DbConn, campaign_id_: i32) -> Option<Json<Campaign>> {
    use crate::schema::campaigns::dsl::*;

    let _query_campaigns = load_from_db_on_match!(&conn.0, campaigns, id, campaign_id_);
    let campaign_list: Vec<Campaign> = _query_campaigns.into_iter().map(|campaign: QueryableCampaign| campaign.into()).collect();

    if campaign_list.is_empty() {
        None
    } else {
        let mut campaign_list = campaign_list;
        let first_campaign = campaign_list.pop().unwrap();
        Some(Json(first_campaign))
    }
}

#[get("/campaign/<id_>/adventures")]
pub fn get_campaign_adventures(conn: DbConn, id_: i32) -> Json<AdventureList> {
    use crate::schema::*;

    let adventures = adventures::table.inner_join(campaign_adventures::table)
        .filter(campaign_adventures::campaign_id.eq(id_))
        .select(adventures::all_columns)
        .load(&conn.0)
        .expect("Could not read from database");

    let adventure_list = adventures.into_iter().map(|adventure: QueryableAdventure| adventure.into()).collect();

    Json(adventure_list)
}

#[post("/campaign", data="<campaign_json>")]
pub fn create_campaign(conn: DbConn, campaign_json: Json<Campaign>) -> Option<Json<Campaign>> {
    use crate::schema::campaigns::dsl::*;

    let campaign = InsertableCampaign::from(campaign_json.into_inner());

    let result = insert_into_db!(&conn.0, campaigns, campaign);

    let new_campaign = get_entry_with_highest_id!(&conn.0, campaigns);

    match result {
        Ok(_) => {
            //let _query_campaigns = campaigns.::<QueryableCampaign>(&conn.0).expect("Could not read from database");
            Some(Json(QueryableCampaign::into(new_campaign)))
        },
        Err(_) => {None},
    }
}

#[put("/campaign_adventure?<campaign_id_>&<adventure_id_>")]
pub fn create_campaign_adventure(conn: DbConn, campaign_id_: i32, adventure_id_: i32) -> Option<String> {
    use crate::schema::campaign_adventures::dsl::*;

    let association_exists = diesel::dsl::select(diesel::dsl::exists(campaign_adventures
        .filter(campaign_id.eq(campaign_id_))
        .filter(adventure_id.eq(adventure_id_))))
        .get_result(&conn.0);

    if association_exists == Ok(true) {
        return Some("Association already exists".to_owned());
    }

    let campaign_adventure = InsertableCampaignAdventure { campaign_id: campaign_id_, adventure_id: adventure_id_};

    let result = insert_into_db!(&conn.0, campaign_adventures, campaign_adventure);

    match result {
        Ok(_) => {
            //let _query_adventures = adventures.::<QueryableAdventure>(&conn.0).expect("Could not read from database");
            Some("Created new association".to_owned())
        },
        Err(_) => {None},
    }
}

#[put("/campaign", data="<campaign_json>")]
pub fn update_campaign(conn: DbConn, campaign_json: Json<Campaign>) -> Option<Json<Campaign>> {
    use crate::schema::campaigns::dsl::*;

    let campaign = QueryableCampaign::from(campaign_json.into_inner());

    let result = update_db_entry!(&conn.0, campaigns, &campaign);

    match result {
        Ok(_) => {Some(Json(QueryableCampaign::into(campaign)))},
        Err(result) => {
            println!("{:?}", result);
            None},
    }
}