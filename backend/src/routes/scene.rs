use diesel::{self, prelude::*};

use rocket_contrib::json::{Json};

use crate::model::scenes::{QueryableScene, InsertableScene, InsertableSceneMap, InsertableSceneCharacter};

use gm_assist::scene::{SceneList, Scene};

use crate::{get_entry_with_highest_id ,insert_into_db, load_from_db, load_from_db_on_match, update_db_entry};
use crate::{DbConn};
use gm_assist::map::MapList;
use crate::model::map::QueryableMap;
use crate::model::characters::QueryableCharacter;
use gm_assist::character::CharacterList;

#[delete("/scene", data="<scene_json>")]
pub fn delete_scene(conn: DbConn, scene_json: Json<Scene>) -> Option<Json<Scene>> {
    use crate::schema::scenes::dsl::*;
    use crate::schema::adventure_scenes::dsl::*;
    use crate::schema::scene_characters::dsl::*;
    use crate::schema::scene_maps::dsl::*;

    let scene = QueryableScene::from(scene_json.into_inner());

    let result = diesel::delete(scenes.filter(
        crate::schema::scenes::dsl::id.eq(scene.id)))
        .execute(&conn.0);

    if result.is_ok() {
        diesel::delete(adventure_scenes.filter(
            crate::schema::adventure_scenes::dsl::scene_id.eq(scene.id))).execute(&conn.0)
            .expect("Could not remove adventure associations");
        diesel::delete(scene_characters.filter(
            crate::schema::scene_characters::dsl::scene_id.eq(scene.id))).execute(&conn.0)
            .expect("Could not remove adventure associations");
        diesel::delete(scene_maps.filter(
            crate::schema::scene_maps::dsl::scene_id.eq(scene.id))).execute(&conn.0)
            .expect("Could not remove adventure associations");
    }

    match result {
        Ok(_) => {Some(Json(QueryableScene::into(scene)))},
        Err(_) => {None},
    }
}

#[delete("/scene_map?<scene_id_>&<map_id_>")]
pub fn delete_scene_map(conn: DbConn, scene_id_: i32, map_id_: i32) -> Option<String> {
    use crate::schema::scene_maps::dsl::*;

    let result = diesel::delete(scene_maps
        .filter(scene_id.eq(scene_id_))
        .filter(map_id.eq(map_id_)))
        .execute(&conn.0);

    match result {
        Ok(_) => {
            //let _query_adventures = adventures.::<QueryableAdventure>(&conn.0).expect("Could not read from database");
            Some("Deleted association".to_owned())
        },
        Err(_) => {None},
    }
}

#[delete("/scene_character?<scene_id_>&<character_id_>")]
pub fn delete_scene_character(conn: DbConn, scene_id_: i32, character_id_: i32) -> Option<String> {
    use crate::schema::scene_characters::dsl::*;

    let result = diesel::delete(scene_characters
        .filter(scene_id.eq(scene_id_))
        .filter(character_id.eq(character_id_)))
        .execute(&conn.0);

    match result {
        Ok(_) => {
            //let _query_adventures = adventures.::<QueryableAdventure>(&conn.0).expect("Could not read from database");
            Some("Deleted association".to_owned())
        },
        Err(_) => {None},
    }
}

#[get("/scenes")]
pub fn get_scenes(conn: DbConn) -> Json<SceneList> {
    use crate::schema::scenes::dsl::*;

    let _query_scenes = load_from_db!(&conn.0, scenes);
    let scene_list = _query_scenes.into_iter().map(|scene: QueryableScene| scene.into()).collect();

    Json(scene_list)
}

#[get("/scene/<scene_id>")]
pub fn get_scene_single(conn: DbConn, scene_id: i32) -> Option<Json<Scene>> {
    use crate::schema::scenes::dsl::*;

    let _query_scenes = load_from_db_on_match!(&conn.0, scenes, id, scene_id);
    let scene_list: Vec<Scene> = _query_scenes.into_iter().map(|scene: QueryableScene| scene.into()).collect();

    if scene_list.is_empty() {
        None
    } else {
        let mut scene_list = scene_list;
        let first_scene = scene_list.pop().unwrap();
        Some(Json(first_scene))
    }
}

#[get("/scene/<id_>/maps")]
pub fn get_scene_maps(conn: DbConn, id_: i32) -> Json<MapList> {
    use crate::schema::*;

    let maps = maps::table.inner_join(scene_maps::table)
        .filter(scene_maps::scene_id.eq(id_))
        .select(maps::all_columns)
        .load(&conn.0)
        .expect("Could not read from database");

    let map_list = maps.into_iter().map(|map: QueryableMap| map.into()).collect();

    Json(map_list)
}

#[get("/scene/<id_>/characters")]
pub fn get_scene_characters(conn: DbConn, id_: i32) -> Json<CharacterList> {
    use crate::schema::*;

    let characters = characters::table.inner_join(scene_characters::table)
        .filter(scene_characters::scene_id.eq(id_))
        .select(characters::all_columns)
        .load(&conn.0)
        .expect("Could not read from database");

    let character_list = characters.into_iter().map(|character: QueryableCharacter| character.into()).collect();

    Json(character_list)
}

#[post("/scene", data="<scene_json>")]
pub fn create_scene(conn: DbConn, scene_json: Json<Scene>) -> Option<Json<Scene>> {
    use crate::schema::scenes::dsl::*;

    let scene = InsertableScene::from(scene_json.into_inner());

    let result = insert_into_db!(&conn.0, scenes, scene);

    let new_scene = get_entry_with_highest_id!(&conn.0, scenes);

    match result {
        Ok(_) => {
            //let _query_scenes = scenes.::<QueryableScene>(&conn.0).expect("Could not read from database");
            Some(Json(QueryableScene::into(new_scene)))
        },
        Err(_) => {None},
    }
}

#[put("/scene", data="<scene_json>")]
pub fn update_scene(conn: DbConn, scene_json: Json<Scene>) -> Option<Json<Scene>> {
    use crate::schema::scenes::dsl::*;

    let scene = QueryableScene::from(scene_json.into_inner());

    let result = update_db_entry!(&conn.0, scenes, &scene);

    match result {
        Ok(_) => {Some(Json(QueryableScene::into(scene)))},
        Err(result) => {
            println!("{:?}", result);
            None},
    }
}

#[put("/scene_map?<scene_id_>&<map_id_>")]
pub fn create_scene_map(conn: DbConn, scene_id_: i32, map_id_: i32) -> Option<String> {
    use crate::schema::scene_maps::dsl::*;

    let association_exists = diesel::dsl::select(diesel::dsl::exists(scene_maps
        .filter(scene_id.eq(scene_id_))
        .filter(map_id.eq(map_id_))))
        .get_result(&conn.0);

    if association_exists == Ok(true) {
        return Some("Association already exists".to_owned());
    }

    let scene_map = InsertableSceneMap { scene_id: scene_id_, map_id: map_id_};

    let result = insert_into_db!(&conn.0, scene_maps, scene_map);

    match result {
        Ok(_) => {
            //let _query_maps = maps.::<QueryableAdventure>(&conn.0).expect("Could not read from database");
            Some("Created new association".to_owned())
        },
        Err(_) => {None},
    }
}

#[put("/scene_character?<scene_id_>&<character_id_>")]
pub fn create_scene_character(conn: DbConn, scene_id_: i32, character_id_: i32) -> Option<String> {
    use crate::schema::scene_characters::dsl::*;

    let association_exists = diesel::dsl::select(diesel::dsl::exists(scene_characters
        .filter(scene_id.eq(scene_id_))
        .filter(character_id.eq(character_id_))))
        .get_result(&conn.0);

    if association_exists == Ok(true) {
        return Some("Association already exists".to_owned());
    }

    let scene_character = InsertableSceneCharacter { scene_id: scene_id_, character_id: character_id_};

    let result = insert_into_db!(&conn.0, scene_characters, scene_character);

    match result {
        Ok(_) => {
            //let _query_characters = characters.::<QueryableAdventure>(&conn.0).expect("Could not read from database");
            Some("Created new association".to_owned())
        },
        Err(_) => {None},
    }
}
