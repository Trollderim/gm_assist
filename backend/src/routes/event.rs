use diesel::{self, prelude::*};

use rocket_contrib::json::{Json};

use crate::model::events::{QueryableEvent, InsertableEvent};

use gm_assist::timeline::{Timeline, Event};

use crate::{get_entry_with_highest_id ,insert_into_db, load_from_db, load_from_db_on_match, update_db_entry};
use crate::{DbConn};

#[delete("/event", data="<event_json>")]
pub fn delete_event(conn: DbConn, event_json: Json<Event>) -> Option<Json<Event>> {
    use crate::schema::events::dsl::*;

    let event = QueryableEvent::from(event_json.into_inner());

    let result = diesel::delete(events.filter(id.eq(event.id)))
        .execute(&conn.0);

    match result {
        Ok(_) => {Some(Json(QueryableEvent::into(event)))},
        Err(_) => {None},
    }
}

#[get("/events")]
pub fn get_events(conn: DbConn) -> Json<Timeline> {
    use crate::schema::events::dsl::*;

    let _query_events = load_from_db!(&conn.0, events);
    let event_list = _query_events.into_iter().map(|event: QueryableEvent| event.into()).collect();

    Json(event_list)
}

#[get("/event/<event_id>")]
pub fn get_event_single(conn: DbConn, event_id: i32) -> Option<Json<Event>> {
    use crate::schema::events::dsl::*;

    let _query_events = load_from_db_on_match!(&conn.0, events, id, event_id);
    let event_list: Vec<Event> = _query_events.into_iter().map(|event: QueryableEvent| event.into()).collect();

    if event_list.is_empty() {
        None
    } else {
        let mut event_list = event_list;
        let first_event = event_list.pop().unwrap();
        Some(Json(first_event))
    }
}

#[post("/event", data="<event_json>")]
pub fn create_event(conn: DbConn, event_json: Json<Event>) -> Option<Json<Event>> {
    use crate::schema::events::dsl::*;

    let event = InsertableEvent::from(event_json.into_inner());

    let result = insert_into_db!(&conn.0, events, event);

    match result {
        Ok(_) => {
            let new_event = get_entry_with_highest_id!(&conn.0, events);

            //let _query_events = events.::<QueryableEvent>(&conn.0).expect("Could not read from database");
            Some(Json(QueryableEvent::into(new_event)))
        },
        Err(_) => {None},
    }
}

#[put("/event", data="<event_json>")]
pub fn update_event(conn: DbConn, event_json: Json<Event>) -> Option<Json<Event>> {
    use crate::schema::events::dsl::*;

    let event = QueryableEvent::from(event_json.into_inner());

    let result = update_db_entry!(&conn.0, events, &event);

    match result {
        Ok(_) => {Some(Json(QueryableEvent::into(event)))},
        Err(result) => {
            println!("{:?}", result);
            None},
    }
}