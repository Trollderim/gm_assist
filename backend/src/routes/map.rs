use rocket::{response::NamedFile, Data, State};
use rocket_contrib::json::Json;
use gm_assist::map::{MapList, MapListEntry};
use std::path::Path;
use crate::{DbConn, DataDir};
use crate::model::map::{QueryableMap, InsertableMap};
use crate::{get_entry_with_highest_id, insert_into_db, load_from_db};
use crate::diesel::RunQueryDsl;
use crate::diesel::query_dsl::methods::OrderDsl;
use crate::diesel::ExpressionMethods;
use diesel::query_dsl::filter_dsl::FilterDsl;

#[delete("/map", data="<map_json>")]
pub fn delete_map(conn: DbConn, map_json: Json<MapListEntry>) -> Option<Json<MapListEntry>> {
    use crate::schema::maps::dsl::*;
    use crate::schema::scene_maps::dsl::*;

    let map_list_entry = QueryableMap::from_map(map_json.into_inner());

    let result = diesel::delete(maps.filter(
        crate::schema::maps::dsl::id.eq(map_list_entry.id)))
        .execute(&conn.0);

    if result.is_ok() {
        diesel::delete(scene_maps.filter(
            crate::schema::scene_maps::dsl::map_id.eq(map_list_entry.id))).execute(&conn.0)
            .expect("Could not remove adventure associations");
    }

    match result {
        Ok(_) => {Some(Json(QueryableMap::into(map_list_entry)))},
        Err(_) => {None},
    }
}

#[get("/map/all")]
pub fn maps(conn: DbConn) -> Json<MapList> {
    use crate::schema::maps::dsl::*;

    let query_maps = load_from_db!(&conn.0, maps);
    let map_list = query_maps.into_iter().map(|map: QueryableMap| map.into()).collect();

    Json(map_list)
}

#[get("/map/<id>/image")]
pub fn map(id: u64, data_dir: State<DataDir>) -> Result<NamedFile, String> {
    let file_name = get_file_name(id as i32, &data_dir.0);
    let file_path = Path::new(file_name.as_str());
    let image_file = NamedFile::open(Path::new(file_path));

    match image_file {
        Ok(image) => {Ok(image)},
        Err(error) => {println!("Could not open file at: {:?}, Error: {:?}", file_path, error);
            Err("Could not load image!".to_owned())
        },
    }
}

#[post("/map", data="<map_json>")]
pub fn create_map(conn: DbConn, map_json: Json<MapListEntry>) -> Option<Json<MapListEntry>> {
    use crate::schema::maps::dsl::*;

    let map = InsertableMap::from(map_json.into_inner());

    let result = insert_into_db!(&conn.0, maps, map);

    let new_map = get_entry_with_highest_id!(&conn.0, maps);

    match result {
        Ok(_) => {
            //let _query_annotations = annotations.::<QueryableAnnotation>(&conn.0).expect("Could not read from database");
            Some(Json(QueryableMap::into(new_map)))
        },
        Err(_) => {None},
    }
}

#[post("/map/<id>/image", data="<new_image>")]
pub fn create_map_image(id: u64, new_image: Data, data_dir: State<DataDir>) -> Result<String, std::io::Error> {
    let file_name = get_file_name(id as i32, &data_dir.0);
    let file_path = Path::new(file_name.as_str());
    let image_file = std::fs::File::create(Path::new(file_path));

    match image_file {
        Ok(_) => {new_image.stream_to_file(file_path).map(|n| n.to_string())},
        Err(err) => {Err(err)},
    }
}

fn get_file_name(id: i32, data_dir: &String) -> String {
    format!("target/{}/maps/{}", data_dir, id)
}