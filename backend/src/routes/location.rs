use diesel::{self, prelude::*};

use rocket_contrib::json::{Json};

use crate::model::locations::{QueryableLocation, InsertableLocation};

use gm_assist::location::{LocationList, Location};

use crate::{get_entry_with_highest_id ,insert_into_db, load_from_db, load_from_db_on_match, update_db_entry};
use crate::{DbConn};

#[delete("/location", data="<location_json>")]
pub fn delete_location(conn: DbConn, location_json: Json<Location>) -> Option<Json<Location>> {
    use crate::schema::locations::dsl::*;

    let location = QueryableLocation::from(location_json.into_inner());

    let result = diesel::delete(locations.filter(id.eq(location.id)))
        .execute(&conn.0);

    match result {
        Ok(_) => {Some(Json(QueryableLocation::into(location)))},
        Err(_) => {None},
    }
}

#[get("/locations")]
pub fn get_locations(conn: DbConn) -> Json<LocationList> {
    use crate::schema::locations::dsl::*;

    let _query_locations = load_from_db!(&conn.0, locations);
    let location_list = _query_locations.into_iter().map(|location: QueryableLocation| location.into()).collect();

    Json(location_list)
}

#[get("/location/<location_id>")]
pub fn get_location_single(conn: DbConn, location_id: i32) -> Option<Json<Location>> {
    use crate::schema::locations::dsl::*;

    let _query_locations = load_from_db_on_match!(&conn.0, locations, id, location_id);
    let location_list: Vec<Location> = _query_locations.into_iter().map(|location: QueryableLocation| location.into()).collect();

    if location_list.is_empty() {
        None
    } else {
        let mut location_list = location_list;
        let first_location = location_list.pop().unwrap();
        Some(Json(first_location))
    }
}

#[post("/location", data="<location_json>")]
pub fn create_location(conn: DbConn, location_json: Json<Location>) -> Option<Json<Location>> {
    use crate::schema::locations::dsl::*;

    let location = InsertableLocation::from(location_json.into_inner());

    let result = insert_into_db!(&conn.0, locations, location);

    let new_location = get_entry_with_highest_id!(&conn.0, locations);

    match result {
        Ok(_) => {
            //let _query_locations = locations.::<QueryableLocation>(&conn.0).expect("Could not read from database");
            Some(Json(QueryableLocation::into(new_location)))
        },
        Err(_) => {None},
    }
}

#[put("/location", data="<location_json>")]
pub fn update_location(conn: DbConn, location_json: Json<Location>) -> Option<Json<Location>> {
    use crate::schema::locations::dsl::*;

    let location = QueryableLocation::from(location_json.into_inner());

    let result = update_db_entry!(&conn.0, locations, &location);

    match result {
        Ok(_) => {Some(Json(QueryableLocation::into(location)))},
        Err(result) => {
            println!("{:?}", result);
            None},
    }
}