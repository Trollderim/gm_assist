use diesel::{self, prelude::*};

use rocket_contrib::json::{Json};

use crate::model::annotation::{QueryableAnnotation, InsertableAnnotation};

use gm_assist::map::{AnnotationList, Annotation};

use crate::{get_entry_with_highest_id ,insert_into_db, load_from_db_on_match, update_db_entry};
use crate::{DbConn};

#[delete("/map/annotation", data="<annotation_json>")]
pub fn delete_annotation(conn: DbConn, annotation_json: Json<Annotation>) -> Option<Json<Annotation>> {
    use crate::schema::annotations::dsl::*;

    let annotation = QueryableAnnotation::from_annotation(annotation_json.into_inner());

    let result = diesel::delete(annotations.filter(id.eq(annotation.id)))
        .execute(&conn.0);

    match result {
        Ok(_) => {Some(Json(QueryableAnnotation::into(annotation)))},
        Err(_) => {None},
    }
}

#[get("/map/<id_>/annotations")]
pub fn map_annotations(conn: DbConn, id_: u64) -> Json<AnnotationList> {
    use crate::schema::annotations::dsl::*;

    let _query_annotations = load_from_db_on_match!(&conn.0, annotations, map_id, id_ as i32);
    let annotation_list = _query_annotations.into_iter().map(|annotation: QueryableAnnotation| annotation.into()).collect();

    Json(annotation_list)
}

#[post("/map/annotation", data="<annotation_json>")]
pub fn create_annotation(conn: DbConn, annotation_json: Json<Annotation>) -> Option<Json<Annotation>> {
    use crate::schema::annotations::dsl::*;

    let annotation = InsertableAnnotation::from(annotation_json.into_inner());

    let result = insert_into_db!(&conn.0, annotations, annotation);

    let new_annotation = get_entry_with_highest_id!(&conn.0, annotations);

    match result {
        Ok(_) => {
            //let _query_annotations = annotations.::<QueryableAnnotation>(&conn.0).expect("Could not read from database");
            Some(Json(QueryableAnnotation::into(new_annotation)))
        },
        Err(_) => {None},
    }
}

#[put("/map/annotation", data="<annotation_json>")]
pub fn update_annotation(conn: DbConn, annotation_json: Json<Annotation>) -> Option<Json<Annotation>> {
    use crate::schema::annotations::dsl::*;

    let annotation = QueryableAnnotation::from_annotation(annotation_json.into_inner());

    let result = update_db_entry!(&conn.0, annotations, &annotation);

    match result {
        Ok(_) => {Some(Json(QueryableAnnotation::into(annotation)))},
        Err(result) => {
            println!("{:?}", result);
            None},
    }
}