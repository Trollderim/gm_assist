use diesel::{self, prelude::*};

use rocket_contrib::json::{Json};

use crate::model::adventures::{QueryableAdventure, InsertableAdventure, InsertableAdventureScene};

use gm_assist::adventure::{AdventureList, Adventure};

use crate::{get_entry_with_highest_id ,insert_into_db, load_from_db, load_from_db_on_match, update_db_entry};
use crate::{DbConn};
use gm_assist::scene::{SceneList, Scene};
use crate::model::scenes::QueryableScene;
use gm_assist::ID;

#[delete("/adventure", data="<adventure_json>")]
pub fn delete_adventure(conn: DbConn, adventure_json: Json<Adventure>) -> Option<Json<Adventure>> {
    use crate::schema::adventures::dsl::*;
    use crate::schema::adventure_scenes::dsl::*;
    use crate::schema::campaign_adventures::dsl::*;

    let adventure = QueryableAdventure::from(adventure_json.into_inner());

    let result = diesel::delete(adventures.filter(
        crate::schema::adventures::dsl::id.eq(adventure.id)))
        .execute(&conn.0);

    if result.is_ok() {
        diesel::delete(adventure_scenes.filter(
            crate::schema::adventure_scenes::dsl::adventure_id.eq(adventure.id))).execute(&conn.0)
            .expect("Could not remove adventure associations");
        diesel::delete(campaign_adventures.filter(
            crate::schema::campaign_adventures::dsl::adventure_id.eq(adventure.id))).execute(&conn.0)
            .expect("Could not remove adventure associations");
    }

    match result {
        Ok(_) => {Some(Json(QueryableAdventure::into(adventure)))},
        Err(_) => {None},
    }
}

#[delete("/adventure_scene?<adventure_id_>&<scene_id_>")]
pub fn delete_adventure_scene(conn: DbConn, adventure_id_: i32, scene_id_: i32) -> Option<String> {
    use crate::schema::adventure_scenes::dsl::*;

    let result = diesel::delete(adventure_scenes
        .filter(adventure_id.eq(adventure_id_))
        .filter(scene_id.eq(scene_id_)))
        .execute(&conn.0);

    match result {
        Ok(_) => {
            //let _query_adventures = adventures.::<QueryableAdventure>(&conn.0).expect("Could not read from database");
            Some("Created new association".to_owned())
        },
        Err(_) => {None},
    }
}

/// Returns a list of stored adventures but only with their name and Id
#[get("/adventures")]
pub fn get_adventures(conn: DbConn) -> Json<AdventureList> {
    use crate::schema::adventures::dsl::*;

    let _query_adventures = load_from_db!(&conn.0, adventures);
    let adventure_list = _query_adventures.into_iter().map(|adventure: QueryableAdventure| Adventure {
        id: ID::from(adventure.id),
        name: adventure.name,
        description: "".to_string()
    }).collect();

    Json(adventure_list)
}

#[get("/adventure/<adventure_id>")]
pub fn get_adventure_single(conn: DbConn, adventure_id: i32) -> Option<Json<Adventure>> {
    use crate::schema::adventures::dsl::*;

    let _query_adventures = load_from_db_on_match!(&conn.0, adventures, id, adventure_id);
    let adventure_list: Vec<Adventure> = _query_adventures.into_iter().map(|adventure: QueryableAdventure| adventure.into()).collect();

    if adventure_list.is_empty() {
        None
    } else {
        let mut adventure_list = adventure_list;
        let first_adventure = adventure_list.pop().unwrap();
        Some(Json(first_adventure))
    }
}

#[get("/adventure/<id_>/scenes")]
pub fn get_adventure_scenes(conn: DbConn, id_: i32) -> Json<SceneList> {
    use crate::schema::*;

    let scenes = scenes::table.inner_join(adventure_scenes::table)
        .filter(adventure_scenes::adventure_id.eq(id_))
        .select(scenes::all_columns)
        .load(&conn.0)
        .expect("Could not read from database");

    let scene_list = scenes.into_iter().map(|scene: QueryableScene| Scene {
        id: ID::from(scene.id),
        name: scene.name,
        description: "".to_string(),
        treasure_description: "".to_string(),
        check_description: "".to_string()
    }).collect();

    Json(scene_list)
}

#[post("/adventure", data="<adventure_json>")]
pub fn create_adventure(conn: DbConn, adventure_json: Json<Adventure>) -> Option<Json<Adventure>> {
    use crate::schema::adventures::dsl::*;

    let adventure = InsertableAdventure::from(adventure_json.into_inner());

    let result = insert_into_db!(&conn.0, adventures, adventure);

    let new_adventure = get_entry_with_highest_id!(&conn.0, adventures);

    match result {
        Ok(_) => {
            //let _query_adventures = adventures.::<QueryableAdventure>(&conn.0).expect("Could not read from database");
            Some(Json(QueryableAdventure::into(new_adventure)))
        },
        Err(_) => {None},
    }
}

#[put("/adventure_scene?<adventure_id_>&<scene_id_>")]
pub fn create_adventure_scene(conn: DbConn, adventure_id_: i32, scene_id_: i32) -> Option<String> {
    use crate::schema::adventure_scenes::dsl::*;

    let association_exists = diesel::dsl::select(diesel::dsl::exists(adventure_scenes
                .filter(adventure_id.eq(adventure_id_))
                .filter(scene_id.eq(scene_id_))))
        .get_result(&conn.0);

    if association_exists == Ok(true) {
        return Some("Association already exists".to_owned());
    }

    let adventure_scene = InsertableAdventureScene { adventure_id: adventure_id_, scene_id: scene_id_ };

    let result = insert_into_db!(&conn.0, adventure_scenes, adventure_scene);

    match result {
        Ok(_) => {
            //let _query_adventures = adventures.::<QueryableAdventure>(&conn.0).expect("Could not read from database");
            Some("Created new association".to_owned())
        },
        Err(_) => {None},
    }
}

#[put("/adventure", data="<adventure_json>")]
pub fn update_adventure(conn: DbConn, adventure_json: Json<Adventure>) -> Option<Json<Adventure>> {
    use crate::schema::adventures::dsl::*;

    let adventure = QueryableAdventure::from(adventure_json.into_inner());

    let result = update_db_entry!(&conn.0, adventures, &adventure);

    match result {
        Ok(_) => {Some(Json(QueryableAdventure::into(adventure)))},
        Err(result) => {
            println!("{:?}", result);
            None},
    }
}