use diesel::{self, prelude::*};

use rocket_contrib::json::{Json};

use crate::model::characters::{QueryableCharacter, InsertableCharacter};

use gm_assist::character::{CharacterList, Character};

use crate::{get_entry_with_highest_id ,insert_into_db, load_from_db, load_from_db_on_match, update_db_entry};
use crate::{DbConn};

#[delete("/character", data="<character_json>")]
pub fn delete_character(conn: DbConn, character_json: Json<Character>) -> Option<Json<Character>> {
    use crate::schema::characters::dsl::*;
    use crate::schema::scene_characters::dsl::*;

    let character = QueryableCharacter::from(character_json.into_inner());

    let result = diesel::delete(characters.filter(
        crate::schema::characters::dsl::id.eq(character.id)))
        .execute(&conn.0);

    if result.is_ok() {
        diesel::delete(scene_characters.filter(
            crate::schema::scene_characters::dsl::character_id.eq(character.id))).execute(&conn.0)
            .expect("Could not remove adventure associations");
    }

    match result {
        Ok(_) => {Some(Json(QueryableCharacter::into(character)))},
        Err(_) => {None},
    }
}

#[get("/characters")]
pub fn get_characters(conn: DbConn) -> Json<CharacterList> {
    use crate::schema::characters::dsl::*;

    let _query_characters = load_from_db!(&conn.0, characters);
    let character_list = _query_characters.into_iter().map(|character: QueryableCharacter| character.into()).collect();

    Json(character_list)
}

#[get("/character/<character_id>")]
pub fn get_character_single(conn: DbConn, character_id: i32) -> Option<Json<Character>> {
    use crate::schema::characters::dsl::*;

    let _query_characters = load_from_db_on_match!(&conn.0, characters, id, character_id);
    let character_list: Vec<Character> = _query_characters.into_iter().map(|character: QueryableCharacter| character.into()).collect();

    if character_list.is_empty() {
        None
    } else {
        let mut character_list = character_list;
        let first_character = character_list.pop().unwrap();
        Some(Json(first_character))
    }
}

#[post("/character", data="<character_json>")]
pub fn create_character(conn: DbConn, character_json: Json<Character>) -> Option<Json<Character>> {
    use crate::schema::characters::dsl::*;

    let character = InsertableCharacter::from(character_json.into_inner());

    let result = insert_into_db!(&conn.0, characters, character);

    let new_character = get_entry_with_highest_id!(&conn.0, characters);

    match result {
        Ok(_) => {
            //let _query_characters = characters.::<QueryableCharacter>(&conn.0).expect("Could not read from database");
            Some(Json(QueryableCharacter::into(new_character)))
        },
        Err(_) => {None},
    }
}

#[put("/character", data="<character_json>")]
pub fn update_character(conn: DbConn, character_json: Json<Character>) -> Option<Json<Character>> {
    use crate::schema::characters::dsl::*;

    let character = QueryableCharacter::from(character_json.into_inner());

    let result = update_db_entry!(&conn.0, characters, &character);

    match result {
        Ok(_) => {Some(Json(QueryableCharacter::into(character)))},
        Err(result) => {
            println!("{:?}", result);
            None},
    }
}