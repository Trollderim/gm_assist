#[cfg(test)]
mod test {
    use rocket::{local::Client, http::{Status, ContentType}};
    use gm_assist::map::{AnnotationList, Annotation};
    use gm_assist::ID;
    use backend::rocket;

    #[test]
    fn a_whole_process_can_be_played_through() {
        let rocket = backend::rocket();
        let client = Client::new(rocket).expect("valid rocket instance");

        assert_size_eq(&client, 1);

        let annotation_inserted = test_insertion(&client);

        assert_size_eq(&client,2);

        let annotation_updated = test_update(&client, annotation_inserted);

        assert_size_eq(&client,2);

        let _annotation_deleted = test_deletion(&client, annotation_updated);

        assert_size_eq(&client,1);
    }

    fn assert_size_eq(client: &Client, expected_size: usize) {
        let req = client.get("/map/1/annotations");
        let response = req.dispatch();

        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.content_type(), Some(ContentType::JSON));

        let mut response = response;

        let annotation_list = response.body_string().expect("Empty String found, although data should be transferred");
        let annotation_list: AnnotationList = serde_json::from_str(annotation_list.as_str()).expect("Caught malformed answer");
        assert!(annotation_list.len() == expected_size);
    }

    fn test_insertion(client: &Client) -> Annotation {
        let annotation_to_be_inserted = Annotation {
            id: ID::default(),
            x: (10, 20),
            y: (10, 200),
            text: "This is a test annotation".to_string(),
            associated_map: None
        };

        let req = client.post("/map/annotation");
        let req = req.body(serde_json::to_string(&annotation_to_be_inserted).unwrap());
        let response = req.dispatch();

        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.content_type(), Some(ContentType::JSON));

        let mut response = response;

        let annotation_inserted =  response.body_string().expect("Empty String found, although data should be transferred");
        let annotation_inserted: Annotation = serde_json::from_str(annotation_inserted.as_str()).expect("Caught malformed answer");

        assert_eq!(annotation_inserted.x, annotation_to_be_inserted.x);
        assert_eq!(annotation_inserted.y, annotation_to_be_inserted.y);
        assert_eq!(annotation_inserted.text, annotation_to_be_inserted.text);

        return annotation_inserted;
    }

    fn test_update(client: &Client, annotation_to_change: Annotation) -> Annotation {
        let mut annotation_to_be_updated = annotation_to_change;
        annotation_to_be_updated.text = "This is an updated text".to_owned();
        annotation_to_be_updated.x.0 = 32;
        annotation_to_be_updated.x.1 = 64;
        annotation_to_be_updated.y.0 = 128;
        annotation_to_be_updated.y.1 = 256;
        let annotation_to_be_updated = annotation_to_be_updated;

        let req = client.put("/map/annotation");
        let req = req.body(serde_json::to_string(&annotation_to_be_updated).unwrap());
        let response = req.dispatch();

        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.content_type(), Some(ContentType::JSON));

        let mut response = response;

        let annotation_updated =  response.body_string().expect("Empty String found, although data should be transferred");
        let annotation_updated: Annotation = serde_json::from_str(annotation_updated.as_str()).expect("Caught malformed answer");

        assert_eq!(annotation_updated, annotation_to_be_updated);

        return annotation_updated;
    }

    fn test_deletion(client: &Client, annotation_to_delete: Annotation) {
        let annotation_to_be_deleted = annotation_to_delete;
        let req = client.delete("/map/annotation");
        let req = req.body(serde_json::to_string(&annotation_to_be_deleted).unwrap());
        let response = req.dispatch();

        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.content_type(), Some(ContentType::JSON));

        let mut response = response;

        let annotation_deleted =  response.body_string().expect("Empty String found, although data should be transferred");
        let annotation_deleted: Annotation = serde_json::from_str(annotation_deleted.as_str()).expect("Caught malformed answer");

        assert_eq!(annotation_to_be_deleted, annotation_deleted);
    }
}