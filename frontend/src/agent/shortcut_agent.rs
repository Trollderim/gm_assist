use yew::prelude::*;
use std::collections::HashSet;
use yew::agent::{HandlerId, Agent, AgentLink, Context};
use yew::services::keyboard::{KeyboardService, KeyListenerHandle};
use yew::utils::document;
use crate::agent::shortcut_agent::Msg::KeypressRegistered;
use serde::{Serialize, Deserialize};
use wasm_bindgen::__rt::std::collections::HashMap;

pub struct ShortcutAgent {
    #[allow(dead_code)]
    keyboard_handler: KeyListenerHandle,
    link: AgentLink<Self>,
    subscribers: HashSet<HandlerId>,
    keymap_ctrl_modified: std::collections::HashMap<String, AvailableShortcutTriggers>,
    keymap_unmodified: std::collections::HashMap<String, AvailableShortcutTriggers>,
}

pub enum Msg {
    KeypressRegistered(KeyboardEvent),
}

#[derive(Serialize, Deserialize, Copy, Clone, Debug)]
pub enum AvailableShortcutTriggers {
    Create,
    Edit,
    Help,
    Submit,
}

impl Agent for ShortcutAgent {
    type Reach = Context;
    type Message = Msg;
    type Input = ();
    type Output = AvailableShortcutTriggers;

    fn create(link: AgentLink<Self>) -> Self {
        let keyboard_handler = KeyboardService::register_key_down(&document(), link.callback(|event| KeypressRegistered(event)));

        let keymap_ctrl_modified = Self::create_keymap_control_modified();
        let keymap_unmodified = Self::create_keymap_unmodified();

        Self {
            keyboard_handler,
            link,
            subscribers: Default::default(),
            keymap_ctrl_modified,
            keymap_unmodified,
        }
    }

    fn update(&mut self, msg: Self::Message) {
        match msg {
            KeypressRegistered(event) => {
                self.handle_ctrl_modified_shortcuts(&event);

                let currently_in_text_input = Self::is_trigger_in_text_input_element();

                if !currently_in_text_input {
                    self.handle_unmodified_shortcuts(&event);
                }
            },
        }
    }

    fn connected(&mut self, id: HandlerId) {
        self.subscribers.insert(id);
    }

    fn handle_input(&mut self, _: Self::Input, _: HandlerId) {
        unimplemented!()
    }

    fn disconnected(&mut self, id: HandlerId) {
        self.subscribers.remove(&id);
    }
}

impl ShortcutAgent {
    fn create_keymap_control_modified() -> HashMap<String, AvailableShortcutTriggers> {
        let mut keymap_ctrl_modified = std::collections::HashMap::new();

        keymap_ctrl_modified.insert("Enter".to_owned(), AvailableShortcutTriggers::Submit);

        keymap_ctrl_modified
    }

    fn create_keymap_unmodified() -> HashMap<String, AvailableShortcutTriggers> {
        let mut keymap_unmodified = std::collections::HashMap::new();

        keymap_unmodified.insert("a".to_owned(), AvailableShortcutTriggers::Create  );
        keymap_unmodified.insert("e".to_owned(), AvailableShortcutTriggers::Edit);
        keymap_unmodified.insert("?".to_owned(), AvailableShortcutTriggers::Help);

        keymap_unmodified
    }

    fn trigger_shortcut(&self, shortcut: AvailableShortcutTriggers) {
        for subscriber in self.subscribers.iter() {
            self.link.respond(*subscriber,
                              shortcut);
        }
    }

    pub fn render_available_shortcuts() -> Html {
        let keymap_ctrl_modified = Self::create_keymap_control_modified();
        let keymap_ctrl_list_entries = Self::render_keymap(&keymap_ctrl_modified);

        let keymap_unmodified = Self::create_keymap_unmodified();
        let keymap_unmodified_list_entries = Self::render_keymap(&keymap_unmodified);

        html! {
            <div class="help shortcut">
                <div class="ctrl">
                    <h2>{"Ctrl Shortcuts"}</h2>
                    {keymap_ctrl_list_entries}
                </div>
                <div class="unmodified">
                    <h2>{"Unmodified Shortcuts"}</h2>
                    {keymap_unmodified_list_entries}
                </div>
            </div>
        }
    }

    fn render_keymap(keymap_unmodified: &HashMap<String, AvailableShortcutTriggers>) -> Html {
        keymap_unmodified.iter().map(|keypair|
            html! {
                    <li>{format!("{} - {:?}", keypair.0, keypair.1)}</li>
                    }).collect::<Html>()
    }

    fn handle_ctrl_modified_shortcuts(&self, event: &KeyboardEvent) {
        if event.ctrl_key() {
            match self.keymap_ctrl_modified.get(&event.key()) {
                None => {},
                Some(trigger) => {
                    self.trigger_shortcut(*trigger);
                },
            }
        }
    }

    fn handle_unmodified_shortcuts(&self, event: &KeyboardEvent) {
        if !event.ctrl_key() {
            match self.keymap_unmodified.get(&event.key()) {
                None => {},
                Some(trigger) => {
                    self.trigger_shortcut(*trigger);
                },
            }
        }
    }

    fn is_trigger_in_text_input_element() -> bool {
        let active_element = document().active_element();

        match active_element {
            Some(temp) => {
                let input_tags = vec!["INPUT", "TEXTAREA"];

                if input_tags.contains(&temp.tag_name().as_str()) {
                    true
                } else {
                   false
                }
            },
            None => {
                false
            },
        }
    }
}

pub trait ShortcutComponent: Component {
    fn create_agent(link: ComponentLink<Self>) -> Box<dyn Bridge<ShortcutAgent>> {
        let callback = Self::create_callback(link);
        ShortcutAgent::bridge(callback)
    }

    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers>;
}
