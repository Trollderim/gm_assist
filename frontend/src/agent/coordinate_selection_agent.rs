use yew::services::ConsoleService;
use gm_assist::map::Point;
use yew::agent::{Agent, HandlerId, Context, AgentLink};

use serde::{Deserialize, Serialize};
use std::collections::HashSet;

pub struct CoordinateSelection {
    link: AgentLink<Self>,
    first_coordinate: Point,
    current_mouse_coordinate: Point,
    second_coordinate: Option<Point>,
    subscribers: HashSet<HandlerId>,
}

/// This is the main delivering struct to select coordinates within a div.
///
/// The mouse_up variable is None as long as the mouse button is pressed.
/// If the mouse button is lifted, the respective coordinate will recorded.
#[derive(Serialize, Deserialize, Debug)]
pub struct CoordinateSelectionOutput {
    pub mouse_down: Point,
    pub current_mouse_position: Point,
    pub mouse_up: Option<Point>
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Request {
    MouseDown (Point),
    MouseMove(Point),
    MouseUp (Point),
}

impl Agent for CoordinateSelection {
    type Reach = Context;
    type Message = ();
    type Input = Request;
    type Output = CoordinateSelectionOutput;

    fn create(link: AgentLink<Self>) -> Self {
        ConsoleService::new().info("Create!");

        Self {
            link,
            first_coordinate: (0, 0),
            current_mouse_coordinate: (0, 0),
            second_coordinate: None,
            subscribers: HashSet::new(),
        }
    }

    fn update(&mut self, _: Self::Message) {

    }

    fn connected(&mut self, id: HandlerId) {
        self.subscribers.insert(id);
    }

    fn handle_input(&mut self, msg: Self::Input, _id: HandlerId) {
        match msg {
            Request::MouseDown(point) => {
                self.first_coordinate = point;
                self.current_mouse_coordinate = point;
            },
            Request::MouseUp(point) => {
                self.second_coordinate = Some(point);

                for subscriber in self.subscribers.iter() {
                    self.link.respond(*subscriber,
                                      CoordinateSelectionOutput{mouse_down: self.first_coordinate,
                                                    current_mouse_position: self.current_mouse_coordinate,
                                                    mouse_up: self.second_coordinate});
                }
            },
            Request::MouseMove(point) => {
                self.current_mouse_coordinate = point;

                for subscriber in self.subscribers.iter() {
                    self.link.respond(*subscriber, CoordinateSelectionOutput{mouse_down: self.first_coordinate,
                        current_mouse_position: self.current_mouse_coordinate,
                        mouse_up: None});
                }
            }
        }
    }

    fn disconnected(&mut self, id: HandlerId) {
        self.subscribers.remove(&id);
    }
}