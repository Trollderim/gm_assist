use gm_assist::adventure::Adventure;
use yew::prelude::*;
use yew::services::fetch::{FetchTask, Response};
use crate::{string::{ERROR_DATA_NOT_PRESENT, NAV_SCENES, TEXT_EDIT_ADVENTURE, TEXT_ADD_ADVENTURE_SCENE}};
use anyhow::Error;
use yew::format::{Json};
use yew::services::ConsoleService;
use crate::component::detail::DetailComponent;
use crate::component::scene::scene_root::*;
use crate::component::entity_choose::*;
use gm_assist::ID;
use crate::agent::shortcut_agent::{ShortcutComponent, ShortcutAgent, AvailableShortcutTriggers};
use crate::entities::{Entities, EntityList};
use crate::api::requests::association::{create_adventure_scene_association_task, AvailableActions};
use crate::create_default_callback;

pub struct AdventureDetail {
    link: ComponentLink<Self>,
    adventure: Option<Adventure>,
    on_edit_click: Callback<Adventure>,
    #[allow(dead_code)]
    task: FetchTask,
    #[allow(dead_code)]
    fetch_task: Option<FetchTask>,
    scene_choose_active: bool,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub id: i32,
    pub on_edit_click: Callback<Adventure>,
}

pub enum Msg {
    AdventureRequestComplete(Adventure),
    AdventureRequestFailed,
    Edit,
    SceneAssociationAdd(ID),
    SceneAssociationRemove(ID),
    SceneAssociationRequestSucceeded,
    SceneAssociationRequestFailed,
    ToggleSceneSelection,
    Noop,
}

impl Component for AdventureDetail {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_get_task_single_item(link.clone(), &props.id.into());

        Self {
            link: link.clone(),
            adventure: None,
            on_edit_click: props.on_edit_click,
            task,
            fetch_task: None,
            scene_choose_active: false,
            shortcut_agent: Self::create_agent(link.clone()),
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::AdventureRequestComplete(adventure) => {self.adventure = Some(adventure); true},
            Msg::AdventureRequestFailed => {ConsoleService::new().error("Could not get Adventure data!"); true},
            Msg::Edit => {self.on_edit_click.emit(self.adventure.clone().unwrap()); true}
            Msg::SceneAssociationAdd(scene) => {
                let callback = create_default_callback!(self.link, Msg::SceneAssociationRequestSucceeded, Msg::SceneAssociationRequestFailed);

                self.fetch_task = Some(create_adventure_scene_association_task(
                    AvailableActions::Add,
                    self.adventure.as_ref().unwrap().id,
                    scene,
                    callback
                ));

                false
            }
            Msg::SceneAssociationRemove(scene) => {
                let callback = create_default_callback!(self.link, Msg::SceneAssociationRequestSucceeded, Msg::SceneAssociationRequestFailed);

                self.fetch_task = Some(create_adventure_scene_association_task(
                    AvailableActions::Remove,
                    self.adventure.as_ref().unwrap().id,
                    scene,
                    callback
                ));

                false
            }
            Msg::SceneAssociationRequestSucceeded => {
                false
            }
            Msg::SceneAssociationRequestFailed => {
                false
            }
            Msg::ToggleSceneSelection => {
                self.scene_choose_active = !self.scene_choose_active;
                true
            }
            Msg::Noop => {false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        match &self.adventure {
            None => {html! {
                <div class="error">
                    {ERROR_DATA_NOT_PRESENT}
                </div>
            }},
            Some(adventure) => {
                let markdown_interpretation = crate::markdown::render_markdown(adventure.description.as_str());
                let on_edit_click = self.link.callback(|_| Msg::Edit);

                html! {
                    <div class="adventure detail">
                        <button id="edit-adventure" onclick=on_edit_click>{TEXT_EDIT_ADVENTURE}</button>
                        <h1 class="name">{&adventure.name}</h1>
                        <div class="description">
                            {markdown_interpretation}
                        </div>
                        {self.render_scene_list()}
                    </div>
                }
            },
        }
    }
}

impl AdventureDetail {
    fn render_scene_list(&self) -> Html {
        let on_create_adventure_scene_button_clicked =
            self.link.callback(|_| Msg::ToggleSceneSelection);

        html! {
            <div class="container embedded-content scene">
                <h2>{NAV_SCENES}</h2>
                <button onclick=on_create_adventure_scene_button_clicked>{TEXT_ADD_ADVENTURE_SCENE}</button>
                {self.render_scene_choose_element()}
                <SceneRoot id=None adventure_id=Some(self.adventure.clone().unwrap().id) />
            </div>
        }
    }

    fn render_scene_choose_element(&self) -> Html {
        if self.scene_choose_active {
            let on_entity_chosen = self.link.callback(|id| Msg::SceneAssociationAdd(id));
            let on_already_associated_entity_chosen = self.link.callback(|id| Msg::SceneAssociationRemove(id));

            html! {
                <div class="choose scene">
                    <EntityChoose entity=Entities::Adventure(self.adventure.clone().unwrap())
                        entity_type=EntityList::SceneList(vec![])
                        on_entity_chosen=on_entity_chosen
                        on_already_associated_entity_chosen=on_already_associated_entity_chosen />
                </div>
            }
        } else {
            html! {
                <span>
                </span>
            }
        }
    }
}

impl DetailComponent for AdventureDetail {
    type Detaillable = Adventure;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Detaillable, Error>>>> {
        link.callback(|response: Response<Json<Result<Adventure, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::AdventureRequestComplete(body);
                }
            }
            Msg::AdventureRequestFailed
        })
    }

    fn get_endpoint(id: &ID) -> String {
        format!("/adventure/{}", id.value())
    }
}

impl ShortcutComponent for AdventureDetail {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Edit => {Msg::Edit},
                _ => {Msg::Noop},
            }
        })
    }
}