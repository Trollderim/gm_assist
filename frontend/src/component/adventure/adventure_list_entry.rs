use yew::prelude::*;
use gm_assist::adventure::Adventure;
use crate::string::TEXT_DELETE;
use yew::format::Json;
use yew::services::{ConsoleService, fetch::{FetchTask, Response}};
use anyhow::Error;
use crate::component::delete::DeleteComponent;

pub struct AdventureListEntry {
    adventure: Adventure,
    link: ComponentLink<Self>,
    delete_task: Option<FetchTask>,
}

pub enum Msg {
    Delete,
    DeleteFailed,
    DeleteSucceeded,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub adventure: Adventure,
}

impl Component for AdventureListEntry {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            adventure: props.adventure,
            link,
            delete_task: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Delete => {
                self.delete_task = Some(self.create_delete_task(self.link.clone()));
                true
            }
            Msg::DeleteFailed => {
                ConsoleService::new().info("Delete of adventure failed!");
                true
            }
            Msg::DeleteSucceeded => {
                true
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.adventure = props.adventure;
        true
    }

    fn view(&self) -> Html {
        let link = format!("/#adventure/{}", self.adventure.id.value());
        let on_delete_click = self.link.callback(|_| Msg::Delete);

        html! {
            <li class="adventure list-entry">
                <a href={link}>
                    {self.adventure.name.clone()}
                </a>
                <button class="delete" onclick=on_delete_click>{TEXT_DELETE}</button>
            </li>
        }
    }
}

impl DeleteComponent for AdventureListEntry {
    type Deletable = Adventure;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Deletable, Error>>>> {
        link.callback(|response: Response<Json<Result<Self::Deletable, Error>>>| {
            if let (meta, Json(Ok(_))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::DeleteSucceeded;
                }
            }
            Msg::DeleteFailed
        })
    }

    fn get_endpoint() -> String {
        "/adventure".to_owned()
    }

    fn get_delete_variable(&self) -> &Self::Deletable {
        &self.adventure
    }
}