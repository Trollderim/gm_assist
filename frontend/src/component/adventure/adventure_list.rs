use gm_assist::adventure::{AdventureList};
use yew::prelude::*;
use yew::format::{Json};
use yew::services::fetch::{Response, FetchTask};
use anyhow::Error;
use yew::services::ConsoleService;
use crate::component::adventure::adventure_list_entry::*;
use crate::component::list::{ListComponent, ReferencedListComponent};
use gm_assist::ID;

pub struct AdventureListView {
    #[allow(dead_code)]
    link: ComponentLink<Self>,
    #[allow(dead_code)]
    fetch: FetchTask,
    adventure_list: AdventureList,
}

pub enum Msg {
    FetchAdventureListComplete(AdventureList),
    FetchAdventureListFailed,
}

#[derive(Properties, Clone, Copy)]
pub struct Props {
    pub campaign_id: Option<ID>,
}

impl Component for AdventureListView {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_corresponding_fetch_task(link.clone(),
                                                         props.campaign_id);

        AdventureListView {
            link,
            fetch: task,
            adventure_list: vec![],
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::FetchAdventureListComplete(_adventure_list) => {
                self.adventure_list = _adventure_list;
                true // Indicate that the Component should re-render
            }
            _ => {
                ConsoleService::new().error("Error in fetching resource!");
                false
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        self.render_list(None)
    }
}

impl ListComponent for AdventureListView {
    type Listable = AdventureList;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<AdventureList, Error>>>> {
        link.callback(|response: Response<Json<Result<AdventureList, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::FetchAdventureListComplete(body);
                }
            }
            Msg::FetchAdventureListFailed
        })
    }

    fn get_endpoint() -> String {
        "/adventures".to_owned()
    }

    fn render_list_entries(&self) -> Html {
        {self.adventure_list.iter().map(|adventure|
            html! {
                    <AdventureListEntry adventure=adventure />
                }).collect::<Html>()
        }
    }
}

impl ReferencedListComponent for AdventureListView {
    type Listable = AdventureList;

    fn get_referenced_endpoint(reference_id: ID) -> String {
        format!("/campaign/{}/adventures", reference_id.value())
    }
}