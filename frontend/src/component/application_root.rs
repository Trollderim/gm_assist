use crate::route::RouterTarget;
use yew::{Component, ComponentLink, Html, html, virtual_dom::VNode, Bridge, Callback};
use yew_router::prelude::*;

use crate::component::adventure::adventure_root::*;
use crate::component::campaign::campaign_root::*;
use crate::component::character::character_root::*;
use crate::component::help::*;
use crate::component::location::location_root::*;
use crate::component::map::map_root::*;
use crate::component::navigation::*;
use crate::component::scene::scene_root::*;
use crate::component::timeline::timeline_root::*;
use crate::agent::shortcut_agent::{ShortcutAgent, ShortcutComponent, AvailableShortcutTriggers};
use crate::component::application_root::Msg::Noop;

pub struct ApplicationRoot {
    route_service: RouteService<()>,
    route: Route<()>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
}

pub enum Msg {
    RouteChanged(Route<()>),
    #[allow(dead_code)]
    ChangeRoute(RouterTarget),
    Noop,
}

impl Component for ApplicationRoot {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let mut route_service: RouteService<()> = RouteService::new();
        let route = route_service.get_route();
        let callback = link.callback(Msg::RouteChanged);
        route_service.register_callback(callback);

        Self {
            route_service,
            route,
            shortcut_agent: Self::create_agent(link),
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::RouteChanged(route) => {self.route = route},
            Msg::ChangeRoute(route) => {
                self.route = route.into();
                self.route_service.set_route(&self.route.route, ());
            }
            Msg::Noop => {return false;},
        };

        true
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let current_router_target = RouterTarget::switch(self.route.clone())
            .unwrap_or_else(|| RouterTarget::Error);

        html! {
            <div class="root">
                <div class="top-menu">
                    <span class="branding logo">{"GM Assist"}</span>
                    <Navigation current_route=current_router_target />
                </div>

                <div class ="root__main-child">
                    {self.get_matching_child_component()}
                </div>
            </div>
        }
    }
}

impl ApplicationRoot {
    fn get_matching_child_component(&self) -> Html {
        match RouterTarget::switch(self.route.clone()) {
            Some(RouterTarget::Error) => {
                html! {
                    <div class="uk-position-center", uk-icon="icon: ban; ratio: 3",></div>
                }
            },
            Some(RouterTarget::Loading) => {
                html! {
                    <div class="uk-position-center", uk-icon="icon: cloud-download; ratio: 3",></div>
                }
            },
            Some(RouterTarget::AdventureListView) => {
                html! {
                    <AdventureRoot id=None campaign_id=None />
                }
            },
            Some(RouterTarget::AdventureDetail(id)) => {
                html! {
                    <AdventureRoot id=id campaign_id=None />
                }
            },
            Some(RouterTarget::CampaignListView) => {
                html! {
                    <CampaignRoot id=None />
                }
            },
            Some(RouterTarget::CampaignDetail(id)) => {
                html! {
                    <CampaignRoot id=id />
                }
            },
            Some(RouterTarget::CharacterListView) => {
                html! {
                    <CharacterRoot id=None scene_id=None />
                }
            },
            Some(RouterTarget::CharacterDetail(id)) => {
                html! {
                    <CharacterRoot id=id scene_id=None />
                }
            },
            Some(RouterTarget::HelpView) => {
                html! {
                    <HelpComponent />
                }
            },
            Some(RouterTarget::LocationListView) => {
                html! {
                    <LocationRoot id=None />
                }
            },
            Some(RouterTarget::LocationDetail(id)) => {
                html! {
                    <LocationRoot id=id />
                }
            },
            Some(RouterTarget::MapListView) => {
                html! {
                    <MapRoot id=None scene_id=None/>
                }
            },
            Some(RouterTarget::MapSingleView(id)) => {
                html! {
                    <MapRoot id=id scene_id=None/>
                }
            },
            Some(RouterTarget::SceneListView) => {
                html! {
                    <SceneRoot id=None adventure_id=None />
                }
            },
            Some(RouterTarget::SceneDetail(id)) => {
                html! {
                    <SceneRoot id=id adventure_id=None />
                }
            },
            Some(RouterTarget::TimelineListView) => {
                html! {
                    <TimelineRoot id=None />
                }
            },
            Some(RouterTarget::TimelineDetail(id)) => {
                html! {
                    <TimelineRoot id=id />
                }
            },
            None => { VNode::from("404") }
        }
    }
}

impl ShortcutComponent for ApplicationRoot {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut: AvailableShortcutTriggers| {
            match shortcut {
                AvailableShortcutTriggers::Help => {Msg::ChangeRoute(RouterTarget::HelpView)},
                _ => {Noop}
            }
        })
    }
}