use gm_assist::campaign::{CampaignList};
use yew::{Component, ComponentLink, ShouldRender, Html, html, Callback};
use yew::format::{Json};
use yew::services::fetch::{Response, FetchTask};
use anyhow::Error;
use yew::services::ConsoleService;
use crate::component::campaign::campaign_list_entry::*;
use crate::component::list::ListComponent;

pub struct CampaignListView {
    #[allow(dead_code)]
    link: ComponentLink<Self>,
    #[allow(dead_code)]
    fetch: FetchTask,
    campaign_list: CampaignList,
}

pub enum Msg {
    FetchCampaignListComplete(CampaignList),
    FetchCampaignListFailed,
}

impl Component for CampaignListView {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_get_list_task(link.clone());

        CampaignListView {
            link,
            fetch: task,
            campaign_list: vec![],
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::FetchCampaignListComplete(_campaign_list) => {
                self.campaign_list = _campaign_list;
                true // Indicate that the Component should re-render
            }
            _ => {
                ConsoleService::new().error("Error in fetching resource!");
                false
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        self.render_list(None)
    }
}

impl ListComponent for CampaignListView {
    type Listable = CampaignList;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<CampaignList, Error>>>> {
        link.callback(|response: Response<Json<Result<CampaignList, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::FetchCampaignListComplete(body);
                }
            }
            Msg::FetchCampaignListFailed
        })
    }

    fn get_endpoint() -> String {
        "/campaigns".to_owned()
    }

    fn render_list_entries(&self) -> Html {
        {self.campaign_list.iter().map(|campaign|
            html! {
                    <CampaignListEntry campaign=campaign />
                }).collect::<Html>()
        }
    }
}