use gm_assist::campaign::Campaign;
use yew::prelude::*;
use yew::services::fetch::{FetchTask, Response};
use crate::{string::{ERROR_DATA_NOT_PRESENT, TEXT_EDIT_CAMPAIGN, NAV_ADVENTURES, TEXT_ADD_CAMPAIGN_ADVENTURE}};
use anyhow::Error;
use yew::format::{Json};
use yew::services::{ConsoleService};
use crate::component::{entity_choose::*, detail::DetailComponent, adventure::adventure_root::*};
use gm_assist::ID;
use crate::agent::shortcut_agent::{ShortcutComponent, AvailableShortcutTriggers, ShortcutAgent};
use crate::entities::*;
use crate::api::requests::association::{create_campaign_adventure_association_task, AvailableActions};
use crate::create_default_callback;

pub struct CampaignDetail {
    adventure_choose_active: bool,
    link: ComponentLink<Self>,
    campaign: Option<Campaign>,
    on_edit_click: Callback<Campaign>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
    #[allow(dead_code)]
    task: Option<FetchTask>,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub id: i32,
    pub on_edit_click: Callback<Campaign>,
}

pub enum Msg {
    CampaignRequestComplete(Campaign),
    CampaignRequestFailed,
    Edit,
    AdventureAssociationAdd(ID),
    AdventureAssociationRemove(ID),
    AdventureAssociationRequestSucceeded,
    AdventureAssociationRequestFailed,
    ToggleAdventureSelection,
    Noop,
}

impl Component for CampaignDetail {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_get_task_single_item(link.clone(), &props.id.into());

        Self {
            adventure_choose_active: false,
            link: link.clone(),
            campaign: None,
            on_edit_click: props.on_edit_click,
            shortcut_agent: Self::create_agent(link.clone()),
            task: Some(task),
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::CampaignRequestComplete(campaign) => {self.campaign = Some(campaign); true},
            Msg::CampaignRequestFailed => {ConsoleService::new().error("Could not get Campaign data!"); true},
            Msg::Edit => {self.on_edit_click.emit(self.campaign.clone().unwrap()); true}
            Msg::AdventureAssociationAdd(adventure) => {
                let callback = create_default_callback!(self.link, Msg::AdventureAssociationRequestSucceeded, Msg::AdventureAssociationRequestFailed);

                self.task = Some(create_campaign_adventure_association_task(
                    AvailableActions::Add,
                    self.campaign.as_ref().unwrap().id,
                    adventure,
                    callback
                ));

                false
            }
            Msg::AdventureAssociationRemove(adventure) => {
                let callback = create_default_callback!(self.link, Msg::AdventureAssociationRequestSucceeded, Msg::AdventureAssociationRequestFailed);

                self.task = Some(create_campaign_adventure_association_task(
                    AvailableActions::Remove,
                    self.campaign.as_ref().unwrap().id,
                    adventure,
                    callback
                ));

                false
            }
            Msg::Noop => {false}
            Msg::AdventureAssociationRequestSucceeded => {true}
            Msg::AdventureAssociationRequestFailed => {false}
            Msg::ToggleAdventureSelection => {self.adventure_choose_active = !self.adventure_choose_active; true}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        match &self.campaign {
            None => {html! {
                <div class="error">
                    {ERROR_DATA_NOT_PRESENT}
                </div>
            }},
            Some(campaign) => {
                let markdown_interpretation = crate::markdown::render_markdown(campaign.description.as_str());
                let on_edit_click = self.link.callback(|_| Msg::Edit);

                html! {
                    <div class="campaign detail">
                        <button id="edit-campaign" onclick=on_edit_click>{TEXT_EDIT_CAMPAIGN}</button>
                        <h1 class="name">{&campaign.name}</h1>
                        <div class="description">
                            {markdown_interpretation}
                        </div>
                        {self.render_adventure_list()}
                    </div>
                }
            },
        }
    }
}

impl CampaignDetail {
    fn render_adventure_list(&self) -> Html {
        let on_create_campaign_adventure_button_clicked =
            self.link.callback(|_| Msg::ToggleAdventureSelection);

        html! {
            <div class="container embedded-content adventure">
                <h2>{NAV_ADVENTURES}</h2>
                <button onclick=on_create_campaign_adventure_button_clicked>{TEXT_ADD_CAMPAIGN_ADVENTURE}</button>
                {self.render_adventure_choose_element()}
                <AdventureRoot id=None campaign_id=Some(self.campaign.clone().unwrap().id) />
            </div>
        }
    }

    fn render_adventure_choose_element(&self) -> Html {
        if self.adventure_choose_active {
            let on_entity_chosen = self.link.callback(|id| Msg::AdventureAssociationAdd(id));
            let on_already_associated_entity_chosen = self.link.callback(|id| Msg::AdventureAssociationRemove(id));

            html! {
                    <div class="choose adventure">
                        <EntityChoose entity=Entities::Campaign(self.campaign.clone().unwrap())
                            entity_type=EntityList::AdventureList(vec![])
                            on_entity_chosen=on_entity_chosen
                            on_already_associated_entity_chosen=on_already_associated_entity_chosen />
                    </div>
                }
        } else {
            html! {
                    <span>
                    </span>
                }
        }
    }
}

impl DetailComponent for CampaignDetail {
    type Detaillable = Campaign;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Detaillable, Error>>>> {
        link.callback(|response: Response<Json<Result<Campaign, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::CampaignRequestComplete(body);
                }
            }
            Msg::CampaignRequestFailed
        })
    }

    fn get_endpoint(id: &ID) -> String {
        format!("/campaign/{}", id.value())
    }
}

impl ShortcutComponent for CampaignDetail {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Edit => {Msg::Edit},
                _ => {Msg::Noop},
            }
        })
    }
}