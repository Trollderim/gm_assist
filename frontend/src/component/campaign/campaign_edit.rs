use yew::{Component, ComponentLink, Html, html, Properties, InputData, services::fetch::{Response, FetchTask}, Callback, Bridge};

use crate::string::{*, TEXT_CANCEL, TEXT_SAVE};
use yew::format::{Json};
use anyhow::Error;
use yew::services::ConsoleService;
use gm_assist::campaign::Campaign;
use crate::component::edit::{EditComponent};
use gm_assist::{ID};
use crate::agent::shortcut_agent::{ShortcutComponent, AvailableShortcutTriggers, ShortcutAgent};

/// This view is used to persist Annotations
pub struct CampaignEdit {
    campaign: Campaign,
    link: ComponentLink<Self>,
    save_task: Option<FetchTask>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
    on_close: Callback<ID>,
}

#[derive(Clone, Properties)]
pub struct Props {
    pub campaign: Campaign,
    pub on_close: Callback<ID>,
}

pub enum Msg {
    Close(Option<ID>),
    Save,
    SavingFailed,

    UpdateName(String),
    UpdateDescription(String),
    Noop,
}

impl Component for CampaignEdit {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        CampaignEdit {
            campaign: props.campaign,
            link: link.clone(),
            save_task: None,
            shortcut_agent: Self::create_agent(link.clone()),
            on_close: props.on_close,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Close(id) => {
                let id_to_emit = match id {
                    None => {ID::default()},
                    Some(id) => {id},
                };
                self.on_close.emit(id_to_emit);

                true
            },
            Msg::Save => {
                self.save_task = Some(self.create_edit_task(self.link.clone()));
                true},
            Msg::UpdateName(new_name) => {self.campaign.name = new_name; false}
            Msg::UpdateDescription(new_description) => {self.campaign.description = new_description; false}
            Msg::SavingFailed => {ConsoleService::new().error("Could not save message to database!"); false}
            Msg::Noop => {false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let on_close_click = self.link.callback(|_| Msg::Close(None));
        let on_save_click = self.link.callback(|_| Msg::Save);

        let on_name_input = self.link.callback(|e: InputData| Msg::UpdateName(e.value));
        let on_description_input = self.link.callback(|e: InputData| Msg::UpdateDescription(e.value));

        html! {
            <div class="campaign edit full-page">
                <h1>{self.get_label()}</h1>
                <form>
                    <label for="name">{"Name:"}</label>
                    <input type="text" id="campaign-name" value=&self.campaign.name oninput=on_name_input name="name"/>
                    <textarea id="description" class="styled" value=&self.campaign.description oninput=on_description_input />
                    <input type="button" onclick=on_save_click value={TEXT_SAVE} />
                    <input type="button" onclick=on_close_click value={TEXT_CANCEL} />
                </form>
            </div>
        }

    }
}

impl EditComponent for CampaignEdit {
    type Editable = Campaign;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Editable, Error>>>> {
        link.callback(|response: Response<Json<Result<Self::Editable, Error>>>| {
            if let (meta, Json(Ok(campaign))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::Close(Some(campaign.id));
                }
            }
            Msg::SavingFailed
        })
    }

    fn get_create_label() -> String {
        TEXT_ADD_CAMPAIGN.to_owned()
    }

    fn get_edit_label() -> String {
        TEXT_EDIT_CAMPAIGN.to_owned()
    }

    fn get_endpoint() -> String {
        "/campaign".to_owned()
    }

    fn get_save_variable(&self) -> &Campaign {
        &self.campaign
    }
}

impl ShortcutComponent for CampaignEdit {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Submit => {Msg::Save},
                _ => {Msg::Noop},
            }
        })
    }
}