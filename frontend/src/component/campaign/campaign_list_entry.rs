use yew::prelude::*;
use gm_assist::campaign::Campaign;
use crate::string::TEXT_DELETE;
use yew::format::Json;
use yew::services::{ConsoleService, fetch::{FetchTask, Response}};
use anyhow::Error;
use crate::component::delete::DeleteComponent;

pub struct CampaignListEntry {
    campaign: Campaign,
    link: ComponentLink<Self>,
    delete_task: Option<FetchTask>,
}

pub enum Msg {
    Delete,
    DeleteFailed,
    DeleteSucceeded,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub campaign: Campaign,
}

impl Component for CampaignListEntry {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            campaign: props.campaign,
            link,
            delete_task: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Delete => {
                self.delete_task = Some(self.create_delete_task(self.link.clone()));
                true
            }
            Msg::DeleteFailed => {
                ConsoleService::new().info("Delete of campaign failed!");
                true
            }
            Msg::DeleteSucceeded => {
                true
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.campaign = props.campaign;
        true
    }

    fn view(&self) -> Html {
        let link = format!("/#campaign/{}", self.campaign.id.value());
        let on_delete_click = self.link.callback(|_| Msg::Delete);

        html! {
            <li class="campaign list-entry">
                <a href={link}>
                    {self.campaign.name.clone()}
                </a>
                <button class="delete" onclick=on_delete_click>{TEXT_DELETE}</button>
            </li>
        }
    }
}

impl DeleteComponent for CampaignListEntry {
    type Deletable = Campaign;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Deletable, Error>>>> {
        link.callback(|response: Response<Json<Result<Self::Deletable, Error>>>| {
            if let (meta, Json(Ok(_))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::DeleteSucceeded;
                }
            }
            Msg::DeleteFailed
        })
    }

    fn get_endpoint() -> String {
        "/campaign".to_owned()
    }

    fn get_delete_variable(&self) -> &Self::Deletable {
        &self.campaign
    }
}