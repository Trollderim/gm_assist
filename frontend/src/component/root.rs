use yew::Html;
use gm_assist::ID;
use crate::entities::Entities;

pub enum AvailableChildren {
    Create,
    Detail(ID),
    Edit(Entities),
    List,
    Loading,
}

/// This trait is intended to be used for root components
///
/// Some assumption about root components:
/// + They have an arbitrary amount of child views
/// + Any combination of those child views is valid, but there is mainly only one active
/// + The root component is mainly responsible for switching between those child views
pub trait Root {
    fn render_active_child_view(&self) -> Html;

    fn is_referenced_root_component(&self) -> bool;

    fn get_child_based_on_id (id: Option<i32>) -> AvailableChildren {
        match id {
            None => { AvailableChildren::List },
            Some(id_) => {
                let id_ = ID::from(id_);

                if id_ == ID::default()
                {
                    AvailableChildren::List
                } else {
                    AvailableChildren::Detail(id_)
                }
            },
        }
    }
}

/// This trait is intended to be used for elements which get embedded into other components.
///
/// One example for intended usage is the Scene Root. It should be possible to embed
/// scenes within adventures. Further operations on those should be referenced to their
/// embedding components.
pub trait EmbeddedRoot: Root {
    fn get_close_child(&self, own_id: ID) -> AvailableChildren {
        if self.is_referenced_root_component() {
            Self::get_child_based_on_id(None)
        } else {
            Self::get_child_based_on_id(Some(own_id.value()))
        }
    }

    fn add_new_association_with_parent(&mut self, own_id: ID);
}