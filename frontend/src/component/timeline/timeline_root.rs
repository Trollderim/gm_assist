use yew::prelude::*;
use crate::component::timeline::timeline_list::*;
use crate::component::timeline::event::event_detail::*;
use crate::component::timeline::event::event_edit::*;
use crate::string::TEXT_ADD_EVENT;
use gm_assist::ID;
use yew_router::service::RouteService;
use yew_router::route::Route;
use crate::component::root::Root;
use crate::agent::shortcut_agent::{ShortcutComponent, AvailableShortcutTriggers, ShortcutAgent};

pub struct TimelineRoot {
    active_child: AvailableChildren,
    link: ComponentLink<Self>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
}

enum AvailableChildren {
    List,
    Detail(ID),
    Edit(gm_assist::timeline::Event),
    Create,
}

pub enum Msg {
    EditOpen(gm_assist::timeline::Event),
    EditClose(ID),
    CreateOpen,
    Noop,
}

#[derive(Properties, Clone, Copy)]
pub struct Props {
    pub id: Option<i32>,
}

impl Component for TimelineRoot {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let initial_active_child = Self::get_child_based_on_id(props.id);

        Self {
            active_child: initial_active_child,
            link: link.clone(),
            shortcut_agent: Self::create_agent(link.clone())
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::EditOpen(event) => {
                self.active_child = AvailableChildren::Edit(event);
                true
            },
            Msg::EditClose(id) => {
                self.active_child = Self::get_child_based_on_id(Some(id.value()));

                // This is a little ugly. But I had a problem where after a new timeline create,
                // the new link was still /timeline. This opens the new timeline, but
                // on cannot access the timeline list by the nav bar anymore, because
                // the browser does not follow the link anymore.
                match self.active_child {
                    AvailableChildren::Detail(_) => {
                        RouteService::new().set_route(format!("/#event/{}", id.value()).as_str(), Route::new_no_state(""));
                    },
                    _ => {}
                }
                true
            },
            Msg::CreateOpen => {
                RouteService::new().set_route(format!("/#event/new").as_str(), Route::new_no_state(""));
                self.active_child = AvailableChildren::Create;
                true
            },
            Msg::Noop => {false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        self.active_child = Self::get_child_based_on_id(_props.id);

        true
    }

    fn view(&self) -> Html {
        html!{
            {self.render_active_child_view()}
        }
    }
}

impl TimelineRoot {
    fn get_child_based_on_id (id: Option<i32>) -> AvailableChildren{
        match id {
            None => { AvailableChildren::List },
            Some(id_) => {
                let id_ = ID::from(id_);

                if id_ == ID::default()
                {
                    AvailableChildren::List
                } else {
                    AvailableChildren::Detail(id_)
                }
            },
        }
    }
}

impl Root for TimelineRoot {
    fn render_active_child_view(&self) -> Html {
        let on_edit_close = self.link.callback(|id| Msg::EditClose(id));
        let on_edit_click = self.link.callback(|timeline| Msg::EditOpen(timeline));

        match &self.active_child {
            AvailableChildren::List => {
                let on_create_button_clicked = self.link.callback(|_| Msg::CreateOpen);

                html! {
                    <div>
                        <button class="create" onclick = on_create_button_clicked>{TEXT_ADD_EVENT}</button>
                        <TimelineView :/>
                    </div>
                }
            },
            AvailableChildren::Detail(id) => {
                html! {<EventDetail id=id.value() on_edit_click=on_edit_click />}
            },
            AvailableChildren::Edit(event_) => {
                html! {
                <div class="full-page">
                    <EventEdit event=event_ on_close=on_edit_close />
                </div>
                }
            },
            AvailableChildren::Create => {
                html! {
                <div class="full-page">
                    <EventEdit event=gm_assist::timeline::Event::default() on_close=on_edit_close />
                </div>
                }
            },
        }
    }

    fn is_referenced_root_component(&self) -> bool {
        false
    }
}

impl ShortcutComponent for TimelineRoot {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Create => {Msg::CreateOpen},
                _ => {Msg::Noop},
            }
        })
    }
}