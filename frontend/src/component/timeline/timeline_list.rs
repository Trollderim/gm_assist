use gm_assist::timeline::{Timeline, format_date};
use yew::{Component, ComponentLink, ShouldRender, Html, html, Callback, InputData};
use yew::format::{Json};
use yew::services::fetch::{Response, FetchTask};
use anyhow::Error;
use yew::services::ConsoleService;
use crate::component::timeline::timeline_list_entry::*;
use crate::component::list::{ListComponent, SortableListComponent, FilterableListComponent};
use core::fmt;

pub struct TimelineView {
    current_filter_term: String,
    #[allow(dead_code)]
    link: ComponentLink<Self>,
    #[allow(dead_code)]
    fetch: FetchTask,
    sorted_by: SortableBy,
    timeline: Timeline,
}

pub enum Msg {
    ChangeFilterTerm(String),
    ChangeSortMethod(SortableBy),
    FetchTimelineComplete(Timeline),
    FetchTimelineFailed,
}

impl Component for TimelineView {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_get_list_task(link.clone());

        TimelineView {
            current_filter_term: String::default(),
            link,
            fetch: task,
            sorted_by: SortableBy::Date,
            timeline: vec![],
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::ChangeFilterTerm(filter) => {
                self.current_filter_term  = filter;
                true
            }
            Msg::ChangeSortMethod(sort_by) => {
                if self.sorted_by != sort_by {
                    self.sorted_by = sort_by;
                    true
                } else {
                    false
                }
            }
            Msg::FetchTimelineComplete(_timeline_list) => {
                self.timeline = _timeline_list;
                true // Indicate that the Component should re-render
            }
            _ => {
                ConsoleService::new().error("Error in fetching resource!");
                false
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let on_filter_input = self.link.callback(|input: InputData| Msg::ChangeFilterTerm(input.value));
        let on_select = self.link.callback(|item| Msg::ChangeSortMethod(item));
        let entries = vec![SortableBy::Date, SortableBy::Id, SortableBy::Name];

        let header = html! {
            <span>
                {Self::render_filter_dialogue(on_filter_input)}
                {Self::render_sort_dialogue(on_select, entries, self.sorted_by)}
            </span>
        };

        self.render_list(Some(header))
    }
}

impl ListComponent for TimelineView {
    type Listable = Timeline;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Timeline, Error>>>> {
        link.callback(|response: Response<Json<Result<Timeline, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::FetchTimelineComplete(body);
                }
            }
            Msg::FetchTimelineFailed
        })
    }

    fn get_endpoint() -> String {
        "/events".to_owned()
    }

    fn render_list_entries(&self) -> Html {
        let time_line_filtered = Self::filter_list_by_term(&self.timeline, self.current_filter_term.as_str());
        let time_line_sorted = Self::sort_listable(&time_line_filtered, self.sorted_by);
        time_line_sorted.iter().map(|event|
        html! {
                <TimelineEntry event=event />
            }).collect::<Html>()

    }
}

impl FilterableListComponent for TimelineView {
    fn filter_list_by_term(list_to_filter: &Self::Listable, filter_term: &str) -> Self::Listable {
        list_to_filter.iter()
            .filter(|item|
                format_date(item).contains(filter_term) ||
                item.name.to_lowercase().contains(filter_term.to_lowercase().as_str()))
            .map(|location| location.clone())
            .collect()
    }
}

#[derive(Clone, Copy, PartialEq)]
pub enum SortableBy {
    Date,
    Id,
    Name,
}

impl fmt::Display for SortableBy {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SortableBy::Id => write!(f, "{}", "Id"),
            SortableBy::Name => write!(f, "{}", "Name"),
            SortableBy::Date => write!(f, "{}", "Date"),
        }
    }
}

impl SortableListComponent for TimelineView {
    type SortOptions = SortableBy;

    fn sort_listable(listable: &Self::Listable, sort_by: Self::SortOptions) -> Self::Listable {
        let mut listable_copy = listable.clone();

        match sort_by {
            SortableBy::Id => { listable_copy.sort_by(|a, b| a.id.cmp(&b.id)); },
            SortableBy::Name => { listable_copy.sort_by(|a, b| a.name.partial_cmp(&b.name).unwrap()); },
            SortableBy::Date => { listable_copy.sort_by(|a, b| a.partial_cmp(&b).unwrap()); }
        }

        listable_copy
    }
}