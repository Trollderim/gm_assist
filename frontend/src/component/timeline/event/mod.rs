use gm_assist::timeline::Event;
use yew::{html, Html};

pub mod event_detail;

pub mod event_edit;

/// Render a given date of an event
pub fn render_date(event: &Event) -> Html {
    html! {
            <span class="dates">
                <span class="date start">
                    <span class="year">{&event.start_year}</span>
                    {render_month_and_day(&event.start_month, &event.start_day)}
                </span>

                {render_end_date(event)}
            </span>
        }
}

fn render_month_and_day(month: &Option<i16>, day: &Option<i16>) -> Html{
    if month.is_some() {
        html! {
            <span>
                <span class="date-link">{"-"}</span>
                <span class="month">{&month.unwrap_or(0)}</span>
                <span class="date-link">{"-"}</span>
                <span class="month">{&day.unwrap_or(0)}</span>
            </span>
        }
    } else  {
        html! {

        }
    }
}

fn render_end_date(event: &Event) -> Html {
    if event.end_year.is_some() {
        html! {
            <span class="date end">
                <span class="linker">{"-"}</span>

                <span class="year">{&event.end_year.unwrap_or(0)}</span>
                {render_month_and_day(&event.end_month, &event.end_day)}
            </span>
        }
    } else {
        html! {

        }
    }
}