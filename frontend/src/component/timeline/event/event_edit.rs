use yew::{Component, ComponentLink, Html, html, Properties, InputData, services::fetch::{Response, FetchTask}, Callback, Bridge};

use crate::string::{TEXT_CANCEL, TEXT_SAVE, TEXT_ADD_EVENT, TEXT_EDIT_EVENT};
use yew::format::{Json};
use anyhow::Error;
use yew::services::ConsoleService;
use gm_assist::timeline::Event;
use crate::component::edit::{EditComponent};
use gm_assist::{ID};
use crate::agent::shortcut_agent::{AvailableShortcutTriggers, ShortcutComponent, ShortcutAgent};

/// This view is used to persist Annotations
pub struct EventEdit {
    event: Event,
    link: ComponentLink<Self>,
    save_task: Option<FetchTask>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
    on_close: Callback<ID>,
}

#[derive(Clone, Properties)]
pub struct Props {
    pub event: Event,
    pub on_close: Callback<ID>,
}

pub enum Msg {
    Close(Option<ID>),
    Save,
    SavingFailed,

    UpdateName(String),
    UpdateDescription(String),

    UpdateStartYear(i64),
    UpdateStartMonth(i16),
    UpdateStartDay(i16),
    UpdateEndYear(i64),
    UpdateEndMonth(i16),
    UpdateEndDay(i16),

    Noop,
}

impl Component for EventEdit {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        EventEdit {
            event: props.event,
            link: link.clone(),
            save_task: None,
            shortcut_agent: Self::create_agent(link.clone()),
            on_close: props.on_close,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Close(id) => {
                let id_to_emit = match id {
                    None => {ID::default()},
                    Some(id) => {id},
                };
                self.on_close.emit(id_to_emit);

                true
            },
            Msg::Save => {
                self.save_task = Some(self.create_edit_task(self.link.clone()));
                true},
            Msg::UpdateName(new_name) => {self.event.name = new_name; false}
            Msg::UpdateDescription(new_description) => {self.event.description = new_description; false}
            Msg::SavingFailed => {ConsoleService::new().error("Could not save message to database!"); false}
            Msg::UpdateStartYear(start_year) => {self.event.start_year = start_year; false}
            Msg::UpdateStartMonth(start_month) => {self.event.start_month = Some(start_month); false}
            Msg::UpdateStartDay(start_day) => {self.event.start_day = Some(start_day); false}
            Msg::UpdateEndYear(end_year) => {self.event.end_year = Some(end_year); false}
            Msg::UpdateEndMonth(end_month) => {self.event.end_month = Some(end_month); false}
            Msg::UpdateEndDay(end_day) => {self.event.end_day = Some(end_day); false}
            Msg::Noop => {false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let on_close_click = self.link.callback(|_| Msg::Close(None));
        let on_save_click = self.link.callback(|_| Msg::Save);

        let on_name_input = self.link.callback(|e: InputData| Msg::UpdateName(e.value));
        let on_description_input = self.link.callback(|e: InputData| Msg::UpdateDescription(e.value));

        html! {
            <div class="event edit full-page">
                <h1>{self.get_label()}</h1>
                <form>
                    <label for="name">{"Name:"}</label>
                    <input type="text" id="event-name" value=&self.event.name oninput=on_name_input name="name"/>
                    {self.render_date_inputs()}
                    <textarea id="description" class="styled" value=&self.event.description oninput=on_description_input />
                    <input type="button" onclick=on_save_click value={TEXT_SAVE} />
                    <input type="button" onclick=on_close_click value={TEXT_CANCEL} />
                </form>
            </div>
        }

    }
}

impl EditComponent for EventEdit {
    type Editable = Event;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Editable, Error>>>> {
        link.callback(|response: Response<Json<Result<Self::Editable, Error>>>| {
            if let (meta, Json(Ok(event))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::Close(Some(event.id));
                }
            }
            Msg::SavingFailed
        })
    }

    fn get_create_label() -> String {
        TEXT_ADD_EVENT.to_owned()
    }

    fn get_edit_label() -> String {
        TEXT_EDIT_EVENT.to_owned()
    }

    fn get_endpoint() -> String {
        "/event".to_owned()
    }

    fn get_save_variable(&self) -> &Event {
        &self.event
    }
}

impl EventEdit {
    fn render_date_inputs(&self) -> Html {
        let on_start_year_input = self.link.callback(|e: InputData| Msg::UpdateStartYear(e.value.parse::<i64>().unwrap()));
        let on_start_month_input = self.link.callback(|e: InputData| Msg::UpdateStartMonth(e.value.parse::<i16>().unwrap()));
        let on_start_day_input = self.link.callback(|e: InputData| Msg::UpdateStartDay(e.value.parse::<i16>().unwrap()));
        let on_end_year_input = self.link.callback(|e: InputData| Msg::UpdateEndYear(e.value.parse::<i64>().unwrap()));
        let on_end_month_input = self.link.callback(|e: InputData| Msg::UpdateEndMonth(e.value.parse::<i16>().unwrap()));
        let on_end_day_input = self.link.callback(|e: InputData| Msg::UpdateEndDay(e.value.parse::<i16>().unwrap()));

        html! {
            <div class="dates input">
                <div class="date start">
                    {"From:"}
                    <input type="number" placeholder="Year" value=self.event.start_year oninput=on_start_year_input />
                    <input type="number" placeholder="Month" oninput=on_start_month_input />
                    <input type="number" placeholder="Day" oninput=on_start_day_input />
                </div>
                <div class="date end">
                    {"Till:"}
                    <input type="number" placeholder="Year" oninput=on_end_year_input />
                    <input type="number" placeholder="Month" oninput=on_end_month_input />
                    <input type="number" placeholder="Day" oninput=on_end_day_input />
                </div>
            </div>
        }
    }
}

impl ShortcutComponent for EventEdit {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Submit => {Msg::Save},
                _ => {Msg::Noop},
            }
        })
    }
}