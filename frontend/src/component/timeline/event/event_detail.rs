use yew::prelude::*;
use yew::services::fetch::{FetchTask, Response};
use crate::{string::{ERROR_DATA_NOT_PRESENT, TEXT_EDIT_EVENT}};
use anyhow::Error;
use yew::format::{Json};
use yew::services::ConsoleService;
use crate::component::detail::DetailComponent;
use crate::component::timeline::event::render_date;
use gm_assist::ID;
use crate::agent::shortcut_agent::{ShortcutAgent, ShortcutComponent, AvailableShortcutTriggers};

pub struct EventDetail {
    link: ComponentLink<Self>,
    event: Option<gm_assist::timeline::Event>,
    on_edit_click: Callback<gm_assist::timeline::Event>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
    #[allow(dead_code)]
    task: FetchTask,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub id: i32,
    pub on_edit_click: Callback<gm_assist::timeline::Event>,
}

pub enum Msg {
    EventRequestComplete(gm_assist::timeline::Event),
    EventRequestFailed,
    Edit,
    Noop,
}

impl Component for EventDetail {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_get_task_single_item(link.clone(), &props.id.into());

        Self {
            link: link.clone(),
            event: None,
            on_edit_click: props.on_edit_click,
            shortcut_agent: Self::create_agent(link.clone()),
            task,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::EventRequestComplete(event) => {self.event = Some(event); true},
            Msg::EventRequestFailed => {ConsoleService::new().error("Could not get event data!"); true},
            Msg::Edit => {self.on_edit_click.emit(self.event.clone().unwrap()); true}
            Msg::Noop => {false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        match &self.event {
            None => {html! {
                <div class="error">
                    {ERROR_DATA_NOT_PRESENT}
                </div>
            }},
            Some(event) => {
                let markdown_interpretation = crate::markdown::render_markdown(event.description.as_str());
                let on_edit_click = self.link.callback(|_| Msg::Edit);

                html! {
                    <div class="event detail">
                        <button id="edit-event" onclick=on_edit_click>{TEXT_EDIT_EVENT}</button>
                        <h1 class="name">{&event.name}</h1>
                        {render_date(self.event.as_ref().unwrap())}
                        <div class="description">
                            {markdown_interpretation}
                        </div>
                    </div>
                }
            },
        }
    }
}

impl DetailComponent for EventDetail {
    type Detaillable = gm_assist::timeline::Event;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Detaillable, Error>>>> {
        link.callback(|response: Response<Json<Result<gm_assist::timeline::Event, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::EventRequestComplete(body);
                }
            }
            Msg::EventRequestFailed
        })
    }

    fn get_endpoint(id: &ID) -> String {
        format!("/event/{}", id.value())
    }
}

impl ShortcutComponent for EventDetail {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Edit => {Msg::Edit},
                _ => {Msg::Noop},
            }
        })
    }
}