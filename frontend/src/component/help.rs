use yew::prelude::*;
use crate::agent::shortcut_agent::ShortcutAgent;

pub struct HelpComponent;

impl Component for HelpComponent {
    type Message = ();
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self{}
    }

    fn update(&mut self, _: Self::Message) -> bool {
        false
    }

    fn change(&mut self, _: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let shortcut_description = ShortcutAgent::render_available_shortcuts();

        html! {
            <div class="root__main-child div__help">
                <h1>{"Shortcuts:"}</h1>
                {shortcut_description}
            </div>
        }
    }
}