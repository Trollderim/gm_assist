use gm_assist::scene::{SceneList};
use yew::prelude::*;
use yew::services::fetch::{Response, FetchTask};
use anyhow::Error;
use yew::services::ConsoleService;
use crate::component::scene::scene_list_entry::*;
use crate::component::list::{ListComponent, ReferencedListComponent};
use gm_assist::ID;
use yew::format::Json;

pub struct SceneListView {
    #[allow(dead_code)]
    link: ComponentLink<Self>,
    #[allow(dead_code)]
    fetch: FetchTask,
    scene_list: SceneList,
}

pub enum Msg {
    FetchSceneListComplete(SceneList),
    FetchSceneListFailed,
}

#[derive(Properties, Clone, Copy)]
pub struct Props {
    pub adventure_id: Option<ID>,
}

impl Component for SceneListView {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_corresponding_fetch_task(link.clone(), props.adventure_id);

        SceneListView {
            link,
            fetch: task,
            scene_list: vec![],
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::FetchSceneListComplete(_scene_list) => {
                self.scene_list = _scene_list;
                true // Indicate that the Component should re-render
            }
            _ => {
                ConsoleService::new().error("Error in fetching resource!");
                false
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        self.render_list(None)
    }
}

impl ListComponent for SceneListView {
    type Listable = SceneList;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<SceneList, Error>>>> {
        link.callback(|response: Response<Json<Result<SceneList, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::FetchSceneListComplete(body);
                }
            }
            Msg::FetchSceneListFailed
        })
    }

    fn get_endpoint() -> String {
        "/scenes".to_owned()
    }

    fn render_list_entries(&self) -> Html {
        {self.scene_list.iter().map(|scene|
            html! {
                    <SceneListEntry scene=scene />
                }).collect::<Html>()
        }
    }
}

impl ReferencedListComponent for SceneListView {
    type Listable = SceneList;

    fn get_referenced_endpoint(reference_id: ID) -> String {
        format!("/adventure/{}/scenes", reference_id.value())
    }
}
