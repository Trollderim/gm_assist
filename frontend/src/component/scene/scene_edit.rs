use yew::{Component, ComponentLink, Html, html, Properties, InputData, services::fetch::{Response, FetchTask}, Callback, Bridge};

use crate::string::{*, TEXT_CANCEL, TEXT_SAVE};
use yew::format::{Json};
use anyhow::Error;
use yew::services::ConsoleService;
use gm_assist::scene::Scene;
use crate::component::edit::{EditComponent};
use gm_assist::{ID};
use crate::agent::shortcut_agent::{ShortcutAgent, ShortcutComponent, AvailableShortcutTriggers};

/// This view is used to persist Annotations
pub struct SceneEdit {
    scene: Scene,
    link: ComponentLink<Self>,
    save_task: Option<FetchTask>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
    on_close: Callback<ID>,
}

#[derive(Clone, Properties)]
pub struct Props {
    pub scene: Scene,
    pub on_close: Callback<ID>,
}

pub enum Msg {
    Close(Option<ID>),
    Save,
    SavingFailed,

    UpdateName(String),
    UpdateDescription(String),
    UpdateCheckDescription(String),
    UpdateTreasureDescription(String),

    Noop,
}

impl Component for SceneEdit {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        SceneEdit {
            scene: props.scene,
            link: link.clone(),
            save_task: None,
            shortcut_agent: Self::create_agent(link.clone()),
            on_close: props.on_close,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Close(id) => {
                let id_to_emit = match id {
                    None => {ID::default()},
                    Some(id) => {id},
                };
                self.on_close.emit(id_to_emit);

                true
            },
            Msg::Save => {
                self.save_task = Some(self.create_edit_task(self.link.clone()));
                true},
            Msg::UpdateName(new_name) => {self.scene.name = new_name; false}
            Msg::UpdateDescription(new_description) => {self.scene.description = new_description; false}
            Msg::UpdateCheckDescription(new_description) => {self.scene.check_description = new_description; false}
            Msg::UpdateTreasureDescription(new_description) => {self.scene.treasure_description = new_description; false}
            Msg::SavingFailed => {ConsoleService::new().error("Could not save message to database!"); false}
            Msg::Noop => {false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let on_close_click = self.link.callback(|_| Msg::Close(None));
        let on_save_click = self.link.callback(|_| Msg::Save);

        let on_name_input = self.link.callback(|e: InputData| Msg::UpdateName(e.value));
        let on_description_input = self.link.callback(|e: InputData| Msg::UpdateDescription(e.value));

        html! {
            <div class="scene edit full-page">
                <h1>{self.get_label()}</h1>
                <form>
                    <label for="name">{"Name:"}</label>
                    <input type="text" id="scene-name" value=&self.scene.name oninput=on_name_input name="name"/>
                    <textarea id="description" class="styled" value=&self.scene.description oninput=on_description_input />
                    {self.render_check_description_input()}
                    {self.render_treasure_description_input()}
                    <input type="button" onclick=on_save_click value={TEXT_SAVE} />
                    <input type="button" onclick=on_close_click value={TEXT_CANCEL} />
                </form>
            </div>
        }
    }
}

impl SceneEdit {
    fn render_check_description_input(&self) -> Html {
        let on_check_description_input = self.link.callback(|e: InputData| Msg::UpdateCheckDescription(e.value));

        html! {
            <div class="input check">
                <h2>{TEXT_CHECK}</h2>
                <textarea id="check_description" class="styled" value=&self.scene.check_description oninput=on_check_description_input />
            </div>
        }
    }

    fn render_treasure_description_input(&self) -> Html {
        let on_treasure_description_input = self.link.callback(|e: InputData| Msg::UpdateTreasureDescription(e.value));

        html! {
            <div class="input treasure">
                <h2>{TEXT_TREASURE}</h2>
                <textarea id="treasure_description" class="styled" value=&self.scene.treasure_description oninput=on_treasure_description_input />
            </div>
        }
    }
}

impl EditComponent for SceneEdit {
    type Editable = Scene;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Editable, Error>>>> {
        link.callback(|response: Response<Json<Result<Self::Editable, Error>>>| {
            if let (meta, Json(Ok(scene))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::Close(Some(scene.id));
                }
            }
            Msg::SavingFailed
        })
    }

    fn get_create_label() -> String {
        TEXT_ADD_SCENE.to_owned()
    }

    fn get_edit_label() -> String {
        TEXT_EDIT_SCENE.to_owned()
    }

    fn get_endpoint() -> String {
        "/scene".to_owned()
    }

    fn get_save_variable(&self) -> &Scene {
        &self.scene
    }
}

impl ShortcutComponent for SceneEdit {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Submit => {Msg::Save},
                _ => {Msg::Noop},
            }
        })
    }
}