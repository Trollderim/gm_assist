use yew::prelude::*;
use gm_assist::scene::Scene;
use crate::string::TEXT_DELETE;
use yew::format::Json;
use yew::services::{ConsoleService, fetch::{FetchTask, Response}};
use anyhow::Error;
use crate::component::delete::DeleteComponent;
use crate::component::scene::scene_detail::*;

pub struct SceneListEntry {
    scene: Scene,
    is_expanded: bool,
    link: ComponentLink<Self>,
    delete_task: Option<FetchTask>,
}

pub enum Msg {
    Delete,
    DeleteFailed,
    DeleteSucceeded,
    Edit,
    ToggleDetail,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub scene: Scene,
}

impl Component for SceneListEntry {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            scene: props.scene,
            is_expanded: false,
            link,
            delete_task: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Delete => {
                self.delete_task = Some(self.create_delete_task(self.link.clone()));
                true
            }
            Msg::DeleteFailed => {
                ConsoleService::new().info("Delete of scene failed!");
                true
            }
            Msg::DeleteSucceeded => {
                true
            }
            Msg::Edit => {
                false
            }
            Msg::ToggleDetail => {
                self.is_expanded = !self.is_expanded;
                true
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.scene = props.scene;
        true
    }

    fn view(&self) -> Html {
        let link = format!("/#scene/{}", self.scene.id.value());
        let on_delete_click = self.link.callback(|_| Msg::Delete);
        let on_header_click = self.link.callback(|_| Msg::ToggleDetail);

        html! {
            <li class="scene list-entry">
                <div class="entry-header" onclick=on_header_click>
                    <a href={link}>
                        {self.scene.name.clone()}
                    </a>
                    <button class="delete" onclick=on_delete_click>{TEXT_DELETE}</button>
                </div>
                <div class="entry-body">
                    {self.render_detail_view()}
                </div>
            </li>
        }
    }
}

impl SceneListEntry {
    fn render_detail_view(&self) -> Html{
        let on_edit_click = self.link.callback(|_| Msg::Edit);

        if self.is_expanded {
            html! {
                <SceneDetail id=self.scene.id.value() on_edit_click=on_edit_click />
            }
        } else {
            html! {
                <div>
                </div>
            }
        }
    }
}

impl DeleteComponent for SceneListEntry {
    type Deletable = Scene;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Deletable, Error>>>> {
        link.callback(|response: Response<Json<Result<Self::Deletable, Error>>>| {
            if let (meta, Json(Ok(_))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::DeleteSucceeded;
                }
            }
            Msg::DeleteFailed
        })
    }

    fn get_endpoint() -> String {
        "/scene".to_owned()
    }

    fn get_delete_variable(&self) -> &Self::Deletable {
        &self.scene
    }
}