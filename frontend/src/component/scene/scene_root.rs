use yew::{prelude::*, services::fetch::{FetchTask, Response}};
use anyhow::Error;
use crate::component::scene::scene_detail::*;
use crate::component::scene::scene_edit::*;
use crate::component::scene::scene_list::*;
use crate::component::loading::*;
use crate::string::TEXT_ADD_SCENE;
use gm_assist::scene::Scene;
use gm_assist::ID;
use yew_router::service::RouteService;
use yew_router::route::Route;
use crate::component::root::*;
use crate::agent::shortcut_agent::{ShortcutComponent, AvailableShortcutTriggers, ShortcutAgent};
use crate::entities::Entities;
use crate::api::requests::association::{create_adventure_scene_association_task, AvailableActions};
use crate::create_default_callback;

pub struct SceneRoot {
    active_child: AvailableChildren,
    link: ComponentLink<Self>,
    adventure_id: Option<ID>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
    fetch_task: Option<FetchTask>,
}

pub enum Msg {
    AssociationAdded,
    EditOpen(Scene),
    EditClose(ID),
    CreateOpen,
    Noop,
}

#[derive(Properties, Clone, Copy)]
pub struct Props {
    pub id: Option<i32>,
    pub adventure_id: Option<ID>,
}

impl Component for SceneRoot {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let initial_active_child = Self::get_child_based_on_id(props.id);

        Self {
            active_child: initial_active_child,
            link: link.clone(),
            adventure_id: props.adventure_id,
            shortcut_agent: Self::create_agent(link.clone()),
            fetch_task: None
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::AssociationAdded => {
                self.active_child = AvailableChildren::List;
                true
            }
            Msg::EditOpen(scene) => {
                self.active_child = AvailableChildren::Edit(Entities::Scene(scene));
                true
            },
            Msg::EditClose(id) => {
                self.active_child = self.get_close_child(id);

                self.add_new_association_with_parent(id);

                // This is a little ugly. But I had a problem where after a new scene create,
                // the new link was still /scene. This opens the new scene, but
                // on cannot access the scene list by the nav bar anymore, because
                // the browser does not follow the link anymore.
                match self.active_child {
                    AvailableChildren::Detail(_) => {
                        RouteService::new().set_route(format!("/#scene/{}", id.value()).as_str(), Route::new_no_state(""));
                    },
                    _ => {}
                }
                true
            },
            Msg::CreateOpen => {
                if !self.is_referenced_root_component() {
                    RouteService::new().set_route(format!("/#scene/new").as_str(), Route::new_no_state(""));
                }
                self.active_child = AvailableChildren::Create;
                true
            },
            Msg::Noop => {false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        self.active_child = Self::get_child_based_on_id(_props.id);

        true
    }

    fn view(&self) -> Html {
        html!{
            {self.render_active_child_view()}
        }
    }
}

impl SceneRoot {
    fn get_child_based_on_id (id: Option<i32>) -> AvailableChildren{
        match id {
            None => { AvailableChildren::List },
            Some(id_) => {
                let id_ = ID::from(id_);

                if id_ == ID::default()
                {
                    AvailableChildren::List
                } else {
                    AvailableChildren::Detail(id_)
                }
            },
        }
    }
}

impl Root for SceneRoot {
    fn render_active_child_view(&self) -> Html {
        let on_edit_close = self.link.callback(|id| Msg::EditClose(id));
        let on_edit_click = self.link.callback(|scene| Msg::EditOpen(scene));

        match &self.active_child {
            AvailableChildren::List => {
                let on_create_button_clicked = self.link.callback(|_| Msg::CreateOpen);

                html! {
                    <div>
                        <button class="create" onclick = on_create_button_clicked>{TEXT_ADD_SCENE}</button>
                        <SceneListView adventure_id=self.adventure_id />
                    </div>
                }
            },
            AvailableChildren::Detail(id) => {
                html! {<SceneDetail id=id.value() on_edit_click=on_edit_click />}
            },
            AvailableChildren::Edit(scene_) => {
                if let Entities::Scene(scene) = scene_ {
                    html! {
                        <div class="full-page">
                            <SceneEdit scene=scene on_close=on_edit_close />
                        </div>
                    }
                } else {
                    html! {<div></div>}
                }
            },
            AvailableChildren::Create => {
                html! {
                <div class="full-page">
                    <SceneEdit scene=Scene::default() on_close=on_edit_close />
                </div>
                }
            },
            AvailableChildren::Loading => {
                html! {<LoadingComponent : />}
            }
        }
    }

    fn is_referenced_root_component(&self) -> bool {
        self.adventure_id != None
    }
}

impl EmbeddedRoot for SceneRoot {
    fn add_new_association_with_parent(&mut self, own_id: ID) {
        if self.is_referenced_root_component() && own_id != ID::default() {
            self.fetch_task = Some(create_adventure_scene_association_task(
                AvailableActions::Add,
                self.adventure_id.expect("No scene id present, but should be present"),
                own_id,
                create_default_callback!(self.link, Msg::AssociationAdded, Msg::Noop))
            );

            self.active_child = AvailableChildren::Loading;
        };
    }
}

impl ShortcutComponent for SceneRoot {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Create => {Msg::CreateOpen},
                _ => {Msg::Noop},
            }
        })
    }
}