use gm_assist::scene::Scene;
use yew::prelude::*;
use yew::services::fetch::{FetchTask, Response};
use crate::{string::{ERROR_DATA_NOT_PRESENT, NAV_CHARACTERS, NAV_MAPS, TEXT_CHECK,
                     TEXT_ADD_SCENE_CHARACTER, TEXT_ADD_SCENE_MAP,
                     TEXT_EDIT_SCENE, TEXT_TREASURE}};
use anyhow::Error;
use yew::format::{Json};
use yew::services::{ConsoleService};
use crate::component::detail::DetailComponent;
use gm_assist::ID;
use crate::agent::shortcut_agent::{ShortcutComponent, AvailableShortcutTriggers, ShortcutAgent};
use crate::component::character::character_root::*;
use crate::component::map::map_root::*;
use crate::component::entity_choose::*;
use crate::entities::*;
use crate::api::requests::association::{create_scene_map_association_task, create_scene_character_association_task, AvailableActions};
use crate::{create_default_callback, create_default_callback_with_return_parameter};

pub struct SceneDetail {
    link: ComponentLink<Self>,
    scene: Option<Scene>,
    on_edit_click: Callback<Scene>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
    #[allow(dead_code)]
    task: FetchTask,
    character_choose_active: bool,
    map_choose_active: bool,
    fetch_task: Option<FetchTask>,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub id: i32,
    pub on_edit_click: Callback<Scene>,
}

pub enum Msg {
    SceneRequestComplete(Scene),
    SceneRequestFailed,
    Edit,
    CharacterAssociationAdd(ID),
    CharacterAssociationRemove(ID),
    CharacterAssociationRequestSucceeded,
    CharacterAssociationRequestFailed,
    ToggleCharacterSelection,
    MapAssociationAdd(ID),
    MapAssociationRemove(ID),
    MapAssociationRequestSucceeded,
    MapAssociationRequestFailed,
    ToggleMapSelection,
    Noop,
}

impl Component for SceneDetail {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_get_task_single_item(link.clone(), &props.id.into());

        Self {
            link: link.clone(),
            scene: None,
            on_edit_click: props.on_edit_click,
            shortcut_agent: Self::create_agent(link.clone()),
            task,
            character_choose_active: false,
            map_choose_active: false,
            fetch_task: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::SceneRequestComplete(scene) => {self.scene = Some(scene); true},
            Msg::SceneRequestFailed => {ConsoleService::new().error("Could not get Scene data!"); true},
            Msg::Edit => {self.on_edit_click.emit(self.scene.clone().unwrap()); true}
            Msg::Noop => {false}
            Msg::CharacterAssociationAdd(character) => {
                let callback = create_default_callback!(self.link, Msg::CharacterAssociationRequestSucceeded, Msg::CharacterAssociationRequestFailed);

                self.fetch_task = Some(create_scene_character_association_task(
                    AvailableActions::Add,
                    self.scene.as_ref().unwrap().id,
                    character,
                    callback
                ));

                false
            }
            Msg::CharacterAssociationRemove(character) => {
                let callback = create_default_callback!(self.link, Msg::CharacterAssociationRequestSucceeded, Msg::CharacterAssociationRequestFailed);

                self.fetch_task = Some(create_scene_character_association_task(
                    AvailableActions::Remove,
                    self.scene.as_ref().unwrap().id,
                    character,
                    callback
                ));

                false
            }
            Msg::CharacterAssociationRequestSucceeded => {
                false
            }
            Msg::CharacterAssociationRequestFailed => {
                false
            }
            Msg::ToggleCharacterSelection => {
                self.character_choose_active = !self.character_choose_active;
                true
            }
            Msg::MapAssociationAdd(map) => {
                let callback = create_default_callback!(self.link, Msg::MapAssociationRequestSucceeded, Msg::MapAssociationRequestFailed);

                self.fetch_task = Some(create_scene_map_association_task(
                    AvailableActions::Add,
                    self.scene.clone().expect("No scene Id present when creating association").id,
                                                                         map, callback));

                false
            }
            Msg::MapAssociationRemove(map) => {
                let callback = create_default_callback!(self.link, Msg::MapAssociationRequestSucceeded, Msg::MapAssociationRequestFailed);

                self.fetch_task = Some(create_scene_map_association_task(
                    AvailableActions::Remove,
                    self.scene.clone().expect("No scene Id present when creating association").id,
                    map, callback));

                false
            }
            Msg::MapAssociationRequestSucceeded => {
                false
            }
            Msg::MapAssociationRequestFailed => {
                false
            }
            Msg::ToggleMapSelection => {
                self.map_choose_active = !self.map_choose_active;
                true
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        match &self.scene {
            None => {html! {
                <div class="error">
                    {ERROR_DATA_NOT_PRESENT}
                </div>
            }},
            Some(scene) => {
                let markdown_interpretation = crate::markdown::render_markdown(scene.description.as_str());
                let on_edit_click = self.link.callback(|_| Msg::Edit);

                html! {
                    <div class="scene detail">
                        <button id="edit-scene" onclick=on_edit_click>{TEXT_EDIT_SCENE}</button>
                        <h1 class="name">{&scene.name}</h1>
                        <div class="description">
                            {markdown_interpretation}
                        </div>
                        <div class="association container">
                            {self.render_treasure_description()}
                            {self.render_check_description()}
                            {self.render_characters()}
                            {self.render_maps()}
                        </div>
                    </div>
                }
            },
        }
    }
}

impl SceneDetail {
    fn render_treasure_description(&self) -> Html {
        let markdown_interpretation = crate::markdown::render_markdown(
            self.scene.as_ref().unwrap().treasure_description.as_str());

        html! {
            <div class="treasure description embedded-content bordered">
                <h2>{TEXT_TREASURE}</h2>
                <div>{markdown_interpretation}</div>
            </div>
        }
    }

    fn render_check_description(&self) -> Html {
        let markdown_interpretation = crate::markdown::render_markdown(
            self.scene.as_ref().unwrap().check_description.as_str());

        html! {
            <div class="check description embedded-content bordered">
                <h2>{TEXT_CHECK}</h2>
                <div>{markdown_interpretation}</div>
            </div>
        }
    }

    fn render_characters(&self) -> Html {
        let on_create_scene_character_button_clicked =
            self.link.callback(|_| Msg::ToggleCharacterSelection);

        html! {
            <div class="list character embedded-content">
                <h2>{NAV_CHARACTERS}</h2>
                <button onclick=on_create_scene_character_button_clicked>{TEXT_ADD_SCENE_CHARACTER}</button>
                {self.render_character_choose_element()}
                <CharacterRoot id=None scene_id=Some(self.scene.as_ref().unwrap().id) />
            </div>
        }
    }

    fn render_character_choose_element(&self) -> Html {
        if self.character_choose_active {
            let on_entity_chosen = self.link.callback(|id| Msg::CharacterAssociationAdd(id));
            let on_already_associated_entity_chosen = self.link.callback(|id| Msg::CharacterAssociationRemove(id));

            html! {
                <div class="choose character">
                    <EntityChoose entity=Entities::Scene(self.scene.clone().unwrap())
                        entity_type = EntityList::CharacterList(vec![])
                        on_entity_chosen=on_entity_chosen
                        on_already_associated_entity_chosen=on_already_associated_entity_chosen />
                </div>
            }
        } else {
            html! {
                <span>
                </span>
            }
        }
    }

    fn render_maps(&self) -> Html {
        let on_create_scene_map_button_clicked =
            self.link.callback(|_| Msg::ToggleMapSelection);

        html! {
            <div class="list map embedded-content">
                <h2>{NAV_MAPS}</h2>
                <button onclick=on_create_scene_map_button_clicked>{TEXT_ADD_SCENE_MAP}</button>
                {self.render_map_choose_element()}
                <MapRoot id=None scene_id=Some(self.scene.as_ref().unwrap().id) />
            </div>
        }
    }

    fn render_map_choose_element(&self) -> Html {
        if self.map_choose_active {
            let on_entity_chosen = self.link.callback(|id| Msg::MapAssociationAdd(id));
            let on_already_associated_entity_chosen = self.link.callback(|id| Msg::MapAssociationRemove(id));

            html! {
                <div class="choose map">
                    <EntityChoose entity=Entities::Scene(self.scene.clone().unwrap())
                        entity_type=EntityList::MapList(vec![])
                        on_entity_chosen=on_entity_chosen
                        on_already_associated_entity_chosen=on_already_associated_entity_chosen />
                </div>
            }
        } else {
            html! {
                <span>
                </span>
            }
        }
    }
}

impl DetailComponent for SceneDetail {
    type Detaillable = Scene;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Detaillable, Error>>>> {
        create_default_callback_with_return_parameter!(link, Msg::SceneRequestComplete, Msg::SceneRequestFailed, Scene)
    }

    fn get_endpoint(id: &ID) -> String {
        format!("/scene/{}", id.value())
    }
}

impl ShortcutComponent for SceneDetail {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Edit => {Msg::Edit},
                _ => {Msg::Noop},
            }
        })
    }
}