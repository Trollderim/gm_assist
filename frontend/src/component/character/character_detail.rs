use gm_assist::character::Character;
use yew::prelude::*;
use yew::services::fetch::{FetchTask, Response};
use crate::{string::{ERROR_DATA_NOT_PRESENT, TEXT_EDIT_CHARACTER}};
use anyhow::Error;
use yew::format::{Json};
use yew::services::ConsoleService;
use crate::component::detail::DetailComponent;
use gm_assist::ID;
use crate::agent::shortcut_agent::{ShortcutAgent, ShortcutComponent, AvailableShortcutTriggers};

pub struct CharacterDetail {
    link: ComponentLink<Self>,
    character: Option<Character>,
    on_edit_click: Callback<Character>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
    #[allow(dead_code)]
    task: FetchTask,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub id: ID,
    pub on_edit_click: Callback<Character>,
}

pub enum Msg {
    CharacterRequestComplete(Character),
    CharacterRequestFailed,
    Edit,
    Noop,
}

impl Component for CharacterDetail {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_get_task_single_item(link.clone(), &props.id.into());

        Self {
            link: link.clone(),
            character: None,
            on_edit_click: props.on_edit_click,
            shortcut_agent: Self::create_agent(link.clone()),
            task,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::CharacterRequestComplete(character) => {self.character = Some(character); true},
            Msg::CharacterRequestFailed => {ConsoleService::new().error("Could not get Character data!"); true},
            Msg::Edit => {self.on_edit_click.emit(self.character.clone().unwrap()); true}
            Msg::Noop => {false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        match &self.character {
            None => {html! {
                <div class="error">
                    {ERROR_DATA_NOT_PRESENT}
                </div>
            }},
            Some(character) => {
                let markdown_interpretation = crate::markdown::render_markdown(character.description.as_str());
                let on_edit_click = self.link.callback(|_| Msg::Edit);

                html! {
                    <div class="character detail">
                        <button id="edit-character" onclick=on_edit_click>{TEXT_EDIT_CHARACTER}</button>
                        <h1 class="name">{&character.name}</h1>
                        <div class="description">
                            {markdown_interpretation}
                        </div>
                    </div>
                }
            },
        }
    }
}

impl DetailComponent for CharacterDetail {
    type Detaillable = Character;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Detaillable, Error>>>> {
        link.callback(|response: Response<Json<Result<Character, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::CharacterRequestComplete(body);
                }
            }
            Msg::CharacterRequestFailed
        })
    }

    fn get_endpoint(id: &ID) -> String {
        format!("/character/{}", id.value())
    }
}

impl ShortcutComponent for CharacterDetail {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Edit => {Msg::Edit},
                _ => {Msg::Noop},
            }
        })
    }
}