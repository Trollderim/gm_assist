use yew::prelude::*;
use gm_assist::character::Character;
use crate::string::TEXT_DELETE;
use yew::format::Json;
use yew::services::{ConsoleService, fetch::{FetchTask, Response}};
use anyhow::Error;
use crate::component::delete::DeleteComponent;

pub struct CharacterListEntry {
    character: Character,
    link: ComponentLink<Self>,
    delete_task: Option<FetchTask>,
}

pub enum Msg {
    Delete,
    DeleteFailed,
    DeleteSucceeded,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub character: Character,
}

impl Component for CharacterListEntry {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            character: props.character,
            link,
            delete_task: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Delete => {
                self.delete_task = Some(self.create_delete_task(self.link.clone()));
                true
            }
            Msg::DeleteFailed => {
                ConsoleService::new().info("Delete of character failed!");
                true
            }
            Msg::DeleteSucceeded => {
                true
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.character = props.character;
        true
    }

    fn view(&self) -> Html {
        let link = format!("/#character/{}", self.character.id.value());
        let on_delete_click = self.link.callback(|_| Msg::Delete);

        html! {
            <li class="character list-entry">
                <a href={link}>
                    {self.character.name.clone()}
                </a>
                <button class="delete" onclick=on_delete_click>{TEXT_DELETE}</button>
            </li>
        }
    }
}

impl DeleteComponent for CharacterListEntry {
    type Deletable = Character;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Deletable, Error>>>> {
        link.callback(|response: Response<Json<Result<Self::Deletable, Error>>>| {
            if let (meta, Json(Ok(_))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::DeleteSucceeded;
                }
            }
            Msg::DeleteFailed
        })
    }

    fn get_endpoint() -> String {
        "/character".to_owned()
    }

    fn get_delete_variable(&self) -> &Self::Deletable {
        &self.character
    }
}