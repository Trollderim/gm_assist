use gm_assist::character::{CharacterList};
use yew::prelude::*;
use yew::{Component, ComponentLink, ShouldRender, Html, html, Callback};
use yew::format::{Json};
use yew::services::fetch::{Response, FetchTask};
use anyhow::Error;
use yew::services::ConsoleService;
use crate::component::character::character_list_entry::*;
use crate::component::character::character_root::*;
use crate::component::list::{ListComponent, ReferencedListComponent};
use gm_assist::ID;

pub struct CharacterListView {
    #[allow(dead_code)]
    link: ComponentLink<Self>,
    #[allow(dead_code)]
    fetch: FetchTask,
    character_list: CharacterList,
    scene_id: Option<ID>,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub scene_id: Option<ID>,
}

pub enum Msg {
    FetchCharacterListComplete(CharacterList),
    FetchCharacterListFailed,
}

impl Component for CharacterListView {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_corresponding_fetch_task(link.clone(),
            props.scene_id);

        CharacterListView {
            link,
            fetch: task,
            character_list: vec![],
            scene_id: props.scene_id,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::FetchCharacterListComplete(_character_list) => {
                self.character_list = _character_list;
                true // Indicate that the Component should re-render
            }
            _ => {
                ConsoleService::new().error("Error in fetching resource!");
                false
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        self.render_list(None)
    }
}

impl CharacterListView {
    fn render_detail(&self, id: ID) -> Html {
        if self.scene_id.is_some() {
            return html! {
                <div class="referenced detail">
                    <CharacterRoot id=Some(id.value()) scene_id=None />
                </div>
            }
        }

        html! {
            <div>
            </div>
        }
    }
}

impl ListComponent for CharacterListView {
    type Listable = CharacterList;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<CharacterList, Error>>>> {
        link.callback(|response: Response<Json<Result<CharacterList, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::FetchCharacterListComplete(body);
                }
            }
            Msg::FetchCharacterListFailed
        })
    }

    fn get_endpoint() -> String {
        "/characters".to_owned()
    }

    fn render_list_entries(&self) -> Html {
        {self.character_list.iter().map(|character|
            html! {
                    <div>
                        <CharacterListEntry character=character />
                        {self.render_detail(character.id)}
                    </div>
                }).collect::<Html>()
        }
    }
}

impl ReferencedListComponent for CharacterListView {
    type Listable = CharacterList;

    fn get_referenced_endpoint(reference_id: ID) -> String {
        format!("/scene/{}/characters", reference_id.value())
    }
}