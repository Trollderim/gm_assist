use crate::BACKEND_URL;
use yew::format::{Nothing, Json};
use serde::{Serialize};
use anyhow::Error;
use yew::{Callback, html, Html, ComponentLink, services::fetch::{Request, Response, FetchService, FetchTask}, Component, InputData};
use yew_components::Select;
use serde::de::DeserializeOwned;
use gm_assist::ID;

pub trait ListComponent: Component {
    type Listable: Serialize + DeserializeOwned;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Listable, Error>>>>;

    fn create_get_list_task(link: ComponentLink<Self>) -> FetchTask {
        let get_request = Request::get(BACKEND_URL.to_owned() + Self::get_endpoint().as_str())
            .body(Nothing)
            .expect("Failed to build get request!");

        let callback = Self::create_callback(link);

        FetchService::new().fetch(get_request, callback).unwrap()
    }

    fn get_endpoint() -> String;

    fn render_list(&self, header: Option<Html>) -> Html{
        let header = if header.is_some() {
            html! {
                <div class="list-header">
                    {header.unwrap()}
                </div>
            }
        } else {
            html! {

            }
        };

        html! {
            <div class="location-list-view list">
                {header}
                <ul class="list-body location-list">
                        {self.render_list_entries()}
                </ul>
            </div>
        }
    }

    fn render_list_entries(&self) -> Html;
}

pub trait FilterableListComponent: ListComponent {
    fn filter_list_by_term(list_to_filter: &Self::Listable, filter_term: &str) -> Self::Listable;

    fn render_filter_dialogue(on_filter_input: Callback<InputData>) -> Html {
        html! {
            <span>
                <label for="filter-dialogue">{"Filter by:"}</label>
                <input oninput=on_filter_input type="text" id="filter-dialogue" />
            </span>
        }
    }
}

pub trait ReferencedListComponent: Component + ListComponent {
    type Listable: Serialize + DeserializeOwned;

    fn create_corresponding_fetch_task(link: ComponentLink<Self>, reference_id: Option<ID>) -> FetchTask {
        match reference_id {
            None => {Self::create_get_list_task(link)},
            Some(reference_id) => {Self::create_referenced_get_list_task(reference_id, link)},
        }
    }

    fn create_referenced_get_list_task(reference_id: ID, link: ComponentLink<Self>) -> FetchTask {
        let get_request = Request::get(BACKEND_URL.to_owned() + Self::get_referenced_endpoint(reference_id).as_str())
            .body(Nothing)
            .expect("Failed to build get request!");

        let callback = Self::create_callback(link);

        FetchService::new().fetch(get_request, callback).unwrap()
    }

    fn get_referenced_endpoint(reference_id: ID) -> String;
}

pub trait SortableListComponent: Component + ListComponent{
    type SortOptions;

    fn sort_listable(listable: &Self::Listable, sort_by: Self::SortOptions) -> Self::Listable;

    fn render_sort_dialogue(on_select: Callback<Self::SortOptions>,
                            options: Vec<Self::SortOptions>,
                            initial_selection: Self::SortOptions) -> Html
        where <Self as crate::component::list::SortableListComponent>::SortOptions:
            std::cmp::PartialEq + std::clone::Clone + std::fmt::Display {
        html! {
            <span>
                <label for="sort-select">{"Sort by:"}</label>
                <Select<Self::SortOptions> options=options on_change=on_select selected=Some(initial_selection) id="sort-select" />
            </span>
        }
    }
}