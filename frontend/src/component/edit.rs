use serde::Serialize;
use crate::BACKEND_URL;
use yew::services::fetch::{FetchService, FetchTask, Request, Response};
use yew::prelude::*;
use gm_assist::{ID, UniquelyIdentifiable};
use serde::de::DeserializeOwned;
use yew::format::Json;
use anyhow::Error;

/// This trait is intended to provide generic functionality for all kinds of edit components
/// Edit components are components which can be used to edit properties of an object.
/// Currently, they are also used to create new objects.
pub trait EditComponent: Component {
    type Editable: DeserializeOwned + Serialize + UniquelyIdentifiable;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Editable, Error>>>>;

    fn create_edit_task(&self, link: ComponentLink<Self>) -> FetchTask {
        let message = serde_json::to_string(self.get_save_variable()).unwrap();

        let request = match self.get_edit_mode() {
            EditModes::Create => {
                Request::post(format!("{}{}", BACKEND_URL.to_owned(), Self::get_endpoint()))
            },
            EditModes::Edit => {
                Request::put(format!("{}{}", BACKEND_URL.to_owned(), Self::get_endpoint()))
            },
        };

        let request = request.header("Content-Type", "application/json")
            .header("Access-Control-Allow-Origin", "http://127.0.0.1:8000")
            .body(Ok(message))
            .expect("Could not build string");

        let callback = Self::create_callback(link);

        FetchService::new().fetch(request, callback).unwrap()
    }

    fn get_create_label() -> String;

    fn get_edit_label() -> String;

    fn get_label(&self) -> String {
        match self.get_edit_mode() {
            EditModes::Create => {
                Self::get_create_label()
            },
            EditModes::Edit => {
                Self::get_edit_label()
            },
        }
    }

    fn get_edit_mode(&self) -> EditModes {
        if self.get_save_variable().get_id() == ID::default() {
            EditModes::Create
        } else {
            EditModes::Edit
        }
    }

    fn get_endpoint() -> String;

    fn get_save_variable(&self) -> &Self::Editable;
}

/// Within this Web App, editing and creating entities is done in a similar fashion.
///
/// This enum serves as purpose to distinguish between them.
pub enum EditModes {
    Create,
    Edit
}