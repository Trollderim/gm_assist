use yew::prelude::*;
use crate::string::{NAV_MAPS, NAV_ADVENTURES, NAV_CAMPAIGN,
                    NAV_CHARACTERS, NAV_HELP, NAV_LOCATIONS, NAV_SCENES, NAV_TIMELINE};
use crate::route::RouterTarget;

enum NavRoutes {
    Maps,
    Adventures,
    Campaigns,
    Characters,
    Help,
    Locations,
    Scenes,
    Timeline,
    Home,
}

pub struct Navigation {
    active_route: NavRoutes
}

#[derive(Properties, Clone)]
pub struct Props {
    pub current_route: RouterTarget
}

impl Component for Navigation {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self {
            active_route: Self::determine_active_route(&props.current_route)
        }
    }

    fn update(&mut self, _: Self::Message) -> bool {
        false
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.active_route = Self::determine_active_route(&props.current_route);
        true
    }

    fn view(&self) -> Html {
        html! {
            <nav class="nav__menu">
                // TODO: Make the active class dependent on the currently active component
                <a href="#adventure/" class={match(self.active_route) {NavRoutes::Adventures =>{"active"}, _ => {""}}}>{NAV_ADVENTURES}</a>
                <a href="#campaign/" class={match(self.active_route) {NavRoutes::Campaigns =>{"active"}, _ => {""}}}>{NAV_CAMPAIGN}</a>
                <a href="#character/" class={match(self.active_route) {NavRoutes::Characters =>{"active"}, _ => {""}}}>{NAV_CHARACTERS}</a>
                <a href="#location/" class={match(self.active_route) {NavRoutes::Locations =>{"active"}, _ => {""}}}>{NAV_LOCATIONS}</a>
                <a href="#map/" class={match(self.active_route) {NavRoutes::Maps =>{"active"}, _ => {""}}}>{NAV_MAPS}</a>
                <a href="#scene/" class={match(self.active_route) {NavRoutes::Scenes =>{"active"}, _ => {""}}}>{NAV_SCENES}</a>
                <a href="#timeline/" class={match(self.active_route) {NavRoutes::Timeline =>{"active"}, _ => {""}}}>{NAV_TIMELINE}</a>
                <a href="#help/" class={match(self.active_route) {NavRoutes::Help =>{"help active"}, _ => {"help"}}}>{NAV_HELP}</a>
            </nav>
        }
    }
}

impl Navigation {
    fn determine_active_route(router_target: &RouterTarget) -> NavRoutes {
        match router_target {
            RouterTarget::Error | RouterTarget::Loading => {NavRoutes::Home},
            RouterTarget::AdventureDetail(_) | RouterTarget::AdventureListView => {NavRoutes::Adventures},
            RouterTarget::CampaignDetail(_) | RouterTarget::CampaignListView => {NavRoutes::Campaigns},
            RouterTarget::CharacterDetail(_) | RouterTarget::CharacterListView => {NavRoutes::Characters},
            RouterTarget::HelpView => {NavRoutes::Help},
            RouterTarget::LocationDetail(_) | RouterTarget::LocationListView => {NavRoutes::Locations},
            RouterTarget::MapSingleView(_) | RouterTarget::MapListView => {NavRoutes::Maps},
            RouterTarget::SceneDetail(_) | RouterTarget::SceneListView => {NavRoutes::Scenes},
            RouterTarget::TimelineDetail(_) | RouterTarget::TimelineListView => {NavRoutes::Timeline},
        }
    }
}