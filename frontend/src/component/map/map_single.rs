use yew::prelude::*;
use yew::format::{Nothing, Json};
use yew::services::fetch::{*, FetchTask};
use yew::Properties;
use anyhow::Error;
use gm_assist::map::{AnnotationList, Annotation, Point};

use crate::agent::coordinate_selection_agent::{CoordinateSelection, Request};
use crate::component::map::annotation_on_map::AnnotationOnMap;
use crate::component::map::annotation::annotation_edit::{AnnotationEdit};
use crate::BACKEND_URL;
use crate::string::TEXT_ADD_ANNOTATION;
use yew::agent::{Dispatcher, Dispatched};
use yew::services::ConsoleService;

pub struct MapSingle {
    id: i32,
    link: ComponentLink<MapSingle>,
    #[allow(dead_code)]
    task: FetchTask,
    annotations: AnnotationList,
    hide_annotations: bool,
    coordinate_selection: Dispatcher<CoordinateSelection>,
    show_annotation_creation_dialogue: bool,
}

pub enum Msg {
    FetchMapAnnotationsComplete(AnnotationList),
    FetchMapAnnotationsFailed,
    MouseDown(MouseEvent),
    MouseMove(MouseEvent),
    MouseUp(MouseEvent),
    CreateDialogueClosed,
    CreateDialogueOpened,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub id: i32
}

impl Component for MapSingle {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let endpoint = format!("{}/map/{}/annotations", BACKEND_URL, props.id);

        let get_request = yew::services::fetch::Request::get(endpoint)
            .body(Nothing)
            .expect("Failed to build get request!");
        let callback = link.callback(|response: Response<Json<Result<AnnotationList, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::FetchMapAnnotationsComplete(body);
                }
            }
            Msg::FetchMapAnnotationsFailed
        });
        let task = FetchService::new().fetch(get_request, callback).unwrap();

        let coordinate_selection = CoordinateSelection::dispatcher();

        MapSingle {
            id:props.id,
            link,
            task,
            annotations: vec![],
            hide_annotations: false,
            coordinate_selection,
            show_annotation_creation_dialogue: false,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::FetchMapAnnotationsComplete(annotations) => {
                self.annotations = annotations;
                true // Indicate that the Component should re-render
            }
            Msg::MouseDown(event) => {
                self.coordinate_selection.send(Request::MouseDown(Self::get_mouse_coordinate(event)));
                false
            }
            Msg::MouseMove(event) => {
                self.coordinate_selection.send(Request::MouseMove(Self::get_mouse_coordinate(event)));
                false
            }
            Msg::MouseUp(event) => {
                self.coordinate_selection.send(Request::MouseUp(Self::get_mouse_coordinate(event)));
                false
            }
            Msg::CreateDialogueClosed => {
                self.show_annotation_creation_dialogue = false;
                true
            }
            Msg::CreateDialogueOpened => {
                self.show_annotation_creation_dialogue = true;
                true
            }
            _ => {ConsoleService::new().error("Error in fetching resource!");
                false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let on_mouse_up = self.link.callback(|event| Msg::MouseUp(event));
        let on_mouse_down = self.link.callback(|event| Msg::MouseDown(event));
        let on_mouse_move = self.link.callback(|event| Msg::MouseMove(event));

        let image_src = format!("http://127.0.0.1:8080/map/{}/image", self.id);

        html! {
            <div class="map-single-view-container">
                <img src={image_src}
                    alt="This is a map your are currently viewing"
                    draggable="false"
                    onmousedown=on_mouse_down
                    onmouseup=on_mouse_up
                    onmousemove=on_mouse_move
                    />

                {self.render_annotations_on_map()}

                {self.render_annotation_new_action()}
            </div>
        }
    }
}

impl MapSingle {
    fn render_annotations_on_map(&self) -> Html {
        if !self.hide_annotations {
            {
                self.annotations.iter().map(|annotation_|
                    html! {
                <div>
                    <AnnotationOnMap annotation=annotation_ />
                 </div>
            }
                ).collect::<Html>()
            }
        } else {
            html! {
                <div/>
            }
        }
    }

    fn render_annotation_new_action(&self) -> Html {
        let on_close = self.link.callback(|_| Msg::CreateDialogueClosed);
        let on_create_button_clicked = self.link.callback(|_| Msg::CreateDialogueOpened);

        let create_button = html! {
            <input class="annotation button-create"
                type="button"
            onclick=on_create_button_clicked
            value={TEXT_ADD_ANNOTATION} />
        };

        let create_dialogue = if self.show_annotation_creation_dialogue {
            html! {
                <AnnotationEdit annotation=Annotation::new(self.id as i32) on_close=on_close />
            }
        } else {
            html! {
                <div>
                </div>
            }
        };


        return html! {
            <div>
                {create_button}
                {create_dialogue}
            </div>
        }
    }

    fn get_mouse_coordinate(event: MouseEvent) -> Point {
        (event.offset_x() as u32, event.offset_y() as u32)
    }
}