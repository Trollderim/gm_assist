use yew::prelude::*;
use gm_assist::map::Annotation;

use crate::component::map::annotation::annotation_expanded::{AnnotationExpanded};
use crate::component::map::annotation::annotation_edit::AnnotationEdit;

pub struct AnnotationOnMap {
    active_sub_component: AvailableSubcomponents,
    annotation: Annotation,
    component_link: ComponentLink<AnnotationOnMap>,
}

#[derive(Properties, Clone)]
pub struct AnnotationOnMapComponentProperties {
    pub annotation: Annotation,
}

pub enum Msg {
    Close,
    Edit,
    Show,
}

enum AvailableSubcomponents {
    Expanded,
    Edit,
    None,
}

impl Component for AnnotationOnMap {
    type Message = Msg;
    type Properties = AnnotationOnMapComponentProperties;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        AnnotationOnMap {
            active_sub_component: AvailableSubcomponents::None,
            annotation: props.annotation,
            component_link: link
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Show => {self.active_sub_component = AvailableSubcomponents::Expanded;
                true},
            Msg::Close => {self.active_sub_component = AvailableSubcomponents::None;
                true},
            Msg::Edit => {self.active_sub_component = AvailableSubcomponents::Edit;
                true},
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let onclick = self.component_link.callback(|_| Msg::Show);

        let style_string = self.get_style_string_for_map_marker();

        let expanded_view = self.get_expanded_view();

        html! {
            <div>
                <div class="annotation-on-map-marker" style={style_string}
                        onclick=onclick>
                </div>

                {expanded_view}
            </div>
        }
    }
}


impl AnnotationOnMap {
    fn get_expanded_view(&self) -> Html {
        match self.active_sub_component {
            AvailableSubcomponents::Expanded => {
                html! {
                    <AnnotationExpanded
                        annotation=self.annotation.clone()
                        on_close_click=self.component_link.callback(|_| Msg::Close)
                        on_edit_click=self.component_link.callback(|_| Msg::Edit) />
                }
            },
            AvailableSubcomponents::Edit => {html!{<AnnotationEdit annotation=self.annotation.clone()
                on_close=self.component_link.callback(|_| Msg::Close) />}},
            AvailableSubcomponents::None => {html! {}},
        }
    }

    fn get_style_string_for_map_marker(&self) -> String {
        let width = self.annotation.x.1 - self.annotation.x.0;
        let height = self.annotation.y.1 - self.annotation.y.0;

        format!("float:left; \
                position:absolute;\
                left:{}px;\
                top:{}px;\
                width:{}px;\
                height:{}px",
                self.annotation.x.0, self.annotation.y.0,
                width, height)
    }
}
