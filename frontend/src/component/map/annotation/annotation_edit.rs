use yew::{Component, ComponentLink, Html, html, Properties, InputData,
          services::fetch::{Response, FetchTask},
          Callback, Bridge, Bridged};

use crate::string::{*, TEXT_CANCEL, TEXT_SAVE};
use crate::component::map::annotation::AnnotationView;
use gm_assist::map::{Annotation};
use yew::format::{Json};
use anyhow::Error;
use yew::services::ConsoleService;
use crate::agent::coordinate_selection_agent::{CoordinateSelection, CoordinateSelectionOutput};
use crate::component::edit::EditComponent;
use crate::agent::shortcut_agent::{ShortcutAgent, ShortcutComponent, AvailableShortcutTriggers};

/// This view is used to persist Annotations
pub struct AnnotationEdit {
    annotation: Annotation,
    link: ComponentLink<Self>,
    save_task: Option<FetchTask>,
    on_close: Callback<()>,
    #[allow(dead_code)]
    coordinate_selection: Box<dyn Bridge<CoordinateSelection>>,
    coordinate_selection_active: bool,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
}

#[derive(Clone, Properties)]
pub struct Props {
    pub annotation: Annotation,
    pub on_close: Callback<()>,
}

pub enum Msg {
    Close,
    Save,
    SavingFailed,

    CoordinateSelection,
    UpdateCoordinates(CoordinateSelectionOutput),

    UpdateText(String),
    Noop,
}

impl Component for AnnotationEdit {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        ConsoleService::new().info("Created edit!");

        let callback = link.callback(Msg::UpdateCoordinates);
        let coordinate_selection = CoordinateSelection::bridge(callback);

        AnnotationEdit {
            annotation: props.annotation,
            link: link.clone(),
            save_task: None,
            on_close: props.on_close,
            coordinate_selection,
            coordinate_selection_active: false,
            shortcut_agent: Self::create_agent(link.clone()),
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Close => {self.on_close.emit(()); true},
            Msg::Save => {
                self.save_task = Some(self.create_edit_task(self.link.clone()));
                true},
            Msg::UpdateText(new_text) => {self.annotation.text = new_text; false},
            Msg::CoordinateSelection => {
                self.coordinate_selection_active = true;
                true
            }
            Msg::UpdateCoordinates(new_coordinates) => {
                if self.coordinate_selection_active {
                    self.annotation.x.0 = (new_coordinates.mouse_down).0;
                    self.annotation.y.0 = (new_coordinates.mouse_down).1;

                    match new_coordinates.mouse_up {
                        None => {
                            self.annotation.x.1 = (new_coordinates.current_mouse_position).0;
                            self.annotation.y.1 = (new_coordinates.current_mouse_position).1;
                        },
                        Some(mouse_up) => {
                            self.coordinate_selection_active = false;

                            self.annotation.x.1 = (mouse_up).0;
                            self.annotation.y.1 = (mouse_up).1;
                        },
                    }
                    true
                } else {
                    false
                }
            }
            Msg::SavingFailed => {ConsoleService::new().error("Could not save message to database!"); false}
            Msg::Noop => {false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let on_close_click = self.link.callback(|_| Msg::Close);
        let on_save_click = self.link.callback(|_| Msg::Save);

        let on_coordinate_selection_click = self.link.callback(|_| Msg::CoordinateSelection);
        let style_string = self.get_style_string_for_map_marker();

        let on_text_input = self.link.callback(|e: InputData| Msg::UpdateText(e.value));

        if !self.coordinate_selection_active {
            html! {
                <div class="annotation expanded edit" style={Self::get_positioning_style_string_for_annotation(&self.annotation)}>
                    <h1>{"Edit Annotation"}</h1>
                    <form>
                        <input type="button" onclick=on_coordinate_selection_click value={TEXT_COORDINATE_SELECTION}/>
                        <textarea id="content" value=&self.annotation.text oninput=on_text_input />
                        <input type="button" onclick=on_save_click value={TEXT_SAVE} />
                        <input type="button" onclick=on_close_click value={TEXT_CANCEL} />
                    </form>
                </div>
            }
        } else {
            html! {
                <div class="annotation-on-map-marker" style={style_string}>
                </div>
            }
        }

    }
}


impl AnnotationView for AnnotationEdit {}

impl AnnotationEdit {
    fn get_style_string_for_map_marker(&self) -> String {
        let width = self.annotation.x.1 - self.annotation.x.0;
        let height = self.annotation.y.1 - self.annotation.y.0;

        // The pointer event disabling is only applicable for the coordinate selection component
        format!("pointer-events: none;\
                float:left; \
                position:absolute;\
                left:{}px;\
                top:{}px;\
                width:{}px;\
                height:{}px",
                self.annotation.x.0, self.annotation.y.0,
                width, height)
    }
}

impl EditComponent for AnnotationEdit {
    type Editable = Annotation;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Editable, Error>>>> {
        link.callback(|response: Response<Json<Result<Annotation, Error>>>| {
            if let (meta, Json(Ok(_))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::Close;
                }
            }
            Msg::SavingFailed
        })
    }

    fn get_create_label() -> String {
        TEXT_ADD_ANNOTATION.to_owned()
    }

    fn get_edit_label() -> String {
        TEXT_EDIT_ANNOTATION.to_owned()
    }

    fn get_endpoint() -> String {
        "/map/annotation".to_owned()
    }

    fn get_save_variable(&self) -> &Self::Editable {
        &self.annotation
    }
}

impl ShortcutComponent for AnnotationEdit {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Submit => {Msg::Save},
                _ => {Msg::Noop},
            }
        })
    }
}