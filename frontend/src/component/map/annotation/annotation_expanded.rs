use yew::{Component, ComponentLink, Html, Callback, html, Properties};
use gm_assist::map::Annotation;
use crate::component::map::annotation::AnnotationView;
use yew::format::Json;
use anyhow::Error;
use yew::services::{fetch::{FetchTask, Response}, ConsoleService};
use crate::markdown::render_markdown;
use crate::component::delete::DeleteComponent;

pub struct AnnotationExpanded {
    annotation: Annotation,
    link: ComponentLink<Self>,
    delete_task: Option<FetchTask>,
    pub on_close_click: Callback<()>,
    pub on_edit_click: Callback<()>,
}

#[derive(Clone, Properties)]
pub struct Props {
    pub annotation: Annotation,
    pub on_close_click: Callback<()>,
    pub on_edit_click: Callback<()>,
}

pub enum Msg {
    Close,
    Delete,
    DeleteFailed,
    Edit,
}

impl Component for AnnotationExpanded {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        AnnotationExpanded {
            annotation: props.annotation,
            link,
            delete_task: None,
            on_close_click: props.on_close_click,
            on_edit_click: props.on_edit_click,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Close => {self.on_close_click.emit(()); true},
            Msg::Delete => {
                self.delete_task = Some(self.create_delete_task(self.link.clone()));
                true
            }
            Msg::DeleteFailed => {
                ConsoleService::new().info("Delete of annotation failed!");
                true
            }
            Msg::Edit => {self.on_edit_click.emit(()); true},
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let formatted_description = render_markdown(self.annotation.text.as_str());

        html! { 
                <div class="annotation expanded" style={Self::get_positioning_style_string_for_annotation(&self.annotation)}>
                    <div class="annotation toolbar">
                        <p onclick=self.link.callback(|_| Msg::Delete)>{"Delete [d]"}</p>
                        <p onclick=self.link.callback(|_| Msg::Edit)>{"Edit [e]"}</p>
                        <p onclick=self.link.callback(|_| Msg::Close)>{"Close [x]"}</p>
                    </div>
                    {formatted_description}
                </div>
            }
    }
}

impl AnnotationView for AnnotationExpanded {}

impl DeleteComponent for AnnotationExpanded {
    type Deletable = Annotation;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Deletable, Error>>>> {
        link.callback(|response: Response<Json<Result<Annotation, Error>>>| {
            if let (meta, Json(Ok(_))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::Close;
                }
            }
            Msg::DeleteFailed
        })
    }

    fn get_endpoint() -> String {
        "/map/annotation".to_owned()
    }

    fn get_delete_variable(&self) -> &Self::Deletable {
        &self.annotation
    }
}