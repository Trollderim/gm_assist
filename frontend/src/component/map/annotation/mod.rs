use gm_assist::map::Annotation;

pub mod annotation_expanded;

pub mod annotation_edit;

trait AnnotationView {
    fn get_positioning_style_string_for_annotation(annotation: &Annotation) -> String {
        format!("float:left; \
            position:absolute;\
            left:{}px;\
            top:{}px;",
                annotation.x.1, annotation.y.0)
    }
}