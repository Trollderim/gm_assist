use yew::prelude::*;
use yew::services::fetch::{FetchTask, FetchService, Request, Response};
use gm_assist::map::MapListEntry;
use yew::services::{ConsoleService, ReaderService};
use yew::services::reader::{File, FileData, ReaderTask};
use crate::string::{TEXT_SAVE, TEXT_ADD_MAP, TEXT_EDIT_MAP};
use crate::BACKEND_URL;
use yew::format::Json;
use anyhow::Error;
use gm_assist::ID;
use crate::component::edit::{EditComponent};
use crate::agent::shortcut_agent::{ShortcutAgent, ShortcutComponent, AvailableShortcutTriggers};

pub struct MapEdit {
    map_instance: MapListEntry,
    link: ComponentLink<Self>,
    save_task: Option<FetchTask>,
    #[allow(dead_code)]
    on_close: Callback<ID>,
    reader: ReaderService,
    image_file: Option<FileData>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
    task: Option<ReaderTask>,
}

#[derive(Clone, Properties)]
pub struct Props {
    pub map_instance: MapListEntry,
    pub on_close: Callback<ID>,
}

pub enum Msg {
    Close,
    Save,
    SaveSucceeded(MapListEntry),
    SavingFailed,

    ChoseFiles(Vec<File>),
    LoadedFile(FileData),

    UpdateText(String),

    Noop,
}

impl Component for MapEdit {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            map_instance: props.map_instance,
            link: link.clone(),
            save_task: None,
            on_close: props.on_close,
            reader: ReaderService::new(),
            image_file: None,
            shortcut_agent: Self::create_agent(link.clone()),
            task: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Close => {
                self.on_close.emit(self.map_instance.id);
                true
            },
            Msg::Save => {
                self.save_task = Some(self.create_edit_task(self.link.clone()));
                false},
            Msg::SavingFailed => {
                ConsoleService::new().error(format!("Could not save image for map with id: {}", self.map_instance.id.value()).as_str());
                false
            },
            Msg::ChoseFiles(images) => {
                if images.len() >= 1 {
                    let task = {
                        let callback = self.link.callback(Msg::LoadedFile);
                        self.reader.read_file(images.first().expect("No file read, although present.").clone(), callback).unwrap()
                    };
                    self.task = Some(task);
                }
                false},
            Msg::UpdateText(text) => {
                self.map_instance.name = text;
                false},
            Msg::LoadedFile(image) => {
                self.image_file = Some(image);
                ConsoleService::new().info(format!("Loaded file: {:?}", self.image_file.as_ref().expect("No file").name).as_str());
                true
            }
            Msg::SaveSucceeded(new_map) => {
                self.map_instance = new_map.clone();

                let image_save_request = Request::post(BACKEND_URL.to_owned() +
                    "/map/" + new_map.id.value().to_string().as_str() + "/image")
                    .header("Content-Type", "image")
                    .header("Access-Control-Allow-Origin", "http://127.0.0.1:8000")
                    .body(Ok(self.image_file.clone().expect("No image file!").content))
                    .expect("Could not build string");

                let callback = self.link.callback(|response: Response<Result<std::vec::Vec<u8>, Error>>| {
                    if let (meta, Ok(_)) = response.into_parts() {
                        if meta.status.is_success() {
                            return Msg::Close;
                        }
                    }
                    Msg::SavingFailed
                });
                ConsoleService::new().info("Image request sent");
                self.save_task = FetchService::new().fetch_binary(image_save_request, callback).ok();

                true
            }
            Msg::Noop => {false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let on_save_click = self.link.callback(|_| Msg::Save);

        let on_text_input = self.link.callback(|e: InputData| Msg::UpdateText(e.value));

        html! {
            <div>
                <div>
                    <h1>{self.get_label()}</h1>
                    {"Name:"}<input type="text"
                        oninput=on_text_input
                        placeholder="Name"/>
                    <input type="file" multiple=true onchange=self.link.callback(move |value| {
                            let mut result = Vec::new();
                            if let ChangeData::Files(files) = value {
                                let files = js_sys::try_iter(&files)
                                    .unwrap()
                                    .unwrap()
                                    .into_iter()
                                    .map(|v| File::from(v.unwrap()));
                                result.extend(files);
                            }
                            Msg::ChoseFiles(result)
                        })/>
                    <input type="button" value={TEXT_SAVE} onclick=on_save_click/>
                </div>
            </div>
        }
    }
}

impl EditComponent for MapEdit {
    type Editable = MapListEntry;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Editable, Error>>>> {
        link.callback(|response: Response<Json<Result<MapListEntry, Error>>>| {
            if let (meta, Json(Ok(new_map))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::SaveSucceeded(new_map);
                }
            }
            Msg::SavingFailed
        })
    }

    fn get_create_label() -> String {
        TEXT_ADD_MAP.to_owned()
    }

    fn get_edit_label() -> String {
        TEXT_EDIT_MAP.to_owned()
    }

    fn get_endpoint() -> String {
        "/map".to_owned()
    }

    fn get_save_variable(&self) -> &Self::Editable {
        &self.map_instance
    }
}

impl ShortcutComponent for MapEdit {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Submit => {Msg::Save},
                _ => {Msg::Noop},
            }
        })
    }
}