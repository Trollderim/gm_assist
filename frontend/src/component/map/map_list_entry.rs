use yew::prelude::*;
use gm_assist::map::MapListEntry;
use crate::string::TEXT_DELETE;
use crate::BACKEND_URL;
use yew::format::Json;
use yew::services::{ConsoleService, fetch::{Request, FetchService, FetchTask, Response}};
use anyhow::Error;

use crate::component::map::map_root::*;

pub struct MapListEntryComponent {
    map_list_entry: MapListEntry,
    link: ComponentLink<Self>,
    delete_task: Option<FetchTask>,
    is_expanded: bool,
}

pub enum Msg {
    Delete,
    DeleteFailed,
    DeleteSucceeded,
    ToggleDetail,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub map_list_entry: MapListEntry,
}

impl Component for MapListEntryComponent {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            map_list_entry: props.map_list_entry,
            link,
            delete_task: None,
            is_expanded: false,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Delete => {
                let message = serde_json::to_string(&self.map_list_entry).unwrap();

                let delete_request = Request::delete(BACKEND_URL.to_owned() +
                    "/map")
                    .header("Content-Type", "application/json")
                    .header("Access-Control-Allow-Origin", "http://127.0.0.1:8000")
                    .body(Ok(message))
                    .expect("Could not build string");
                let callback = self.link.callback(|response: Response<Json<Result<MapListEntry, Error>>>| {
                    if let (meta, Json(Ok(_))) = response.into_parts() {
                        if meta.status.is_success() {
                            return Msg::DeleteSucceeded;
                        }
                    }
                    Msg::DeleteFailed
                });

                self.delete_task = FetchService::new().fetch(delete_request, callback).ok();
                true
            }
            Msg::DeleteFailed => {
                ConsoleService::new().info("Delete of map failed!");
                true
            }
            Msg::DeleteSucceeded => {
                true
            }
            Msg::ToggleDetail => {
                self.is_expanded = !self.is_expanded;
                true
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let link = format!("/#map/{}", self.map_list_entry.id.value());
        let on_delete_click = self.link.callback(|_| Msg::Delete);
        let on_header_click = self.link.callback(|_| Msg::ToggleDetail);

        html! {
            <li class="map list-entry">
                <div class="entry-header" onclick=on_header_click>
                    <a href={link}>
                        {self.map_list_entry.name.clone()}
                    </a>
                    <button class="delete" onclick=on_delete_click>{TEXT_DELETE}</button>
                </div>
                <div class="entry-body">
                    {self.render_detail_view()}
                </div>
            </li>
        }
    }
}

impl MapListEntryComponent {
    fn render_detail_view(&self) -> Html{
        if self.is_expanded {
            html! {
                <MapRoot id=self.map_list_entry.id.value() scene_id=None />
            }
        } else {
            html! {
                <div>
                </div>
            }
        }
    }
}