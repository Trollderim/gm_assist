use gm_assist::map::{MapList};
use yew::{Component, ComponentLink, ShouldRender, Html, html, Callback};
use yew::prelude::*;
use yew::format::Json;
use yew::services::fetch::{Response, FetchTask};
use anyhow::Error;
use crate::component::map::map_list_entry::*;
use yew::services::ConsoleService;
use crate::component::list::{ListComponent, ReferencedListComponent};
use gm_assist::ID;

pub struct MapListView {
    #[allow(dead_code)]
    fetch: FetchTask,
    map_list: MapList,
}

pub enum Msg {
    FetchMapListComplete(MapList),
    FetchMapListFailed,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub scene_id: Option<ID>,
}

impl Component for MapListView {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        MapListView {
            fetch: Self::create_corresponding_fetch_task(link, props.scene_id),
            map_list: vec![],
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::FetchMapListComplete(_map_list) => {
                self.map_list = _map_list;
                true // Indicate that the Component should re-render
            }
            _ => {
                ConsoleService::new().error("Error in fetching resource!");
                false
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        self.render_list(None)
    }
}

impl ListComponent for MapListView {
    type Listable = MapList;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Listable, Error>>>> {
        link.callback(|response: Response<Json<Result<MapList, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::FetchMapListComplete(body);
                }
            }
            Msg::FetchMapListFailed
        })
    }

    fn get_endpoint() -> String {
        "/map/all".to_owned()
    }

    fn render_list_entries(&self) -> Html {
        {self.map_list.iter().map(|map_list_entry|
            html! {
                    <MapListEntryComponent map_list_entry=map_list_entry />
                }).collect::<Html>()
        }
    }
}

impl ReferencedListComponent for MapListView {
    type Listable = MapList;

    fn get_referenced_endpoint(reference_id: ID) -> String {
        format!("/scene/{}/maps", reference_id.value())
    }
}