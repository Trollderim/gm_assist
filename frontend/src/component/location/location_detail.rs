use gm_assist::location::Location;
use yew::prelude::*;
use yew::services::fetch::{FetchTask, Response};
use crate::{string::{ERROR_DATA_NOT_PRESENT, TEXT_EDIT_LOCATION}};
use anyhow::Error;
use yew::format::{Json};
use yew::services::ConsoleService;
use crate::component::detail::DetailComponent;
use gm_assist::ID;
use crate::agent::shortcut_agent::{AvailableShortcutTriggers, ShortcutComponent, ShortcutAgent};

pub struct LocationDetail {
    link: ComponentLink<Self>,
    location: Option<Location>,
    on_edit_click: Callback<Location>,
    #[allow(dead_code)]
    shortcut_agent: Box<dyn Bridge<ShortcutAgent>>,
    #[allow(dead_code)]
    task: FetchTask,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub id: i32,
    pub on_edit_click: Callback<Location>,
}

pub enum Msg {
    LocationRequestComplete(Location),
    LocationRequestFailed,
    Edit,
    Noop,
}

impl Component for LocationDetail {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_get_task_single_item(link.clone(), &props.id.into());

        Self {
            link: link.clone(),
            location: None,
            on_edit_click: props.on_edit_click,
            shortcut_agent: Self::create_agent(link.clone()),
            task,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::LocationRequestComplete(location) => {self.location = Some(location); true},
            Msg::LocationRequestFailed => {ConsoleService::new().error("Could not get Location data!"); true},
            Msg::Edit => {self.on_edit_click.emit(self.location.clone().unwrap()); true}
            Msg::Noop => {false}
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        match &self.location {
            None => {html! {
                <div class="error">
                    {ERROR_DATA_NOT_PRESENT}
                </div>
            }},
            Some(location) => {
                let markdown_interpretation = crate::markdown::render_markdown(location.description.as_str());
                let on_edit_click = self.link.callback(|_| Msg::Edit);

                html! {
                    <div class="location detail">
                        <button id="edit-location" onclick=on_edit_click>{TEXT_EDIT_LOCATION}</button>
                        <h1 class="name">{&location.name}</h1>
                        <div class="description">
                            {markdown_interpretation}
                        </div>
                    </div>
                }
            },
        }
    }
}

impl DetailComponent for LocationDetail {
    type Detaillable = Location;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Detaillable, Error>>>> {
        link.callback(|response: Response<Json<Result<Location, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::LocationRequestComplete(body);
                }
            }
            Msg::LocationRequestFailed
        })
    }

    fn get_endpoint(id: &ID) -> String {
        format!("/location/{}", id.value())
    }
}

impl ShortcutComponent for LocationDetail {
    fn create_callback(link: ComponentLink<Self>) -> Callback<AvailableShortcutTriggers> {
        link.callback(|shortcut| {
            match shortcut {
                AvailableShortcutTriggers::Edit => {Msg::Edit},
                _ => {Msg::Noop},
            }
        })
    }
}