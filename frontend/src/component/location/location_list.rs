use gm_assist::location::{LocationList};
use yew::{Component, ComponentLink, ShouldRender, Html, html, Callback, InputData};
use yew::format::{Json};
use yew::services::fetch::{Response, FetchTask};
use anyhow::Error;
use yew::services::ConsoleService;
use crate::component::location::location_list_entry::*;
use crate::component::list::{ListComponent, SortableListComponent, FilterableListComponent};
use core::fmt;

pub struct LocationListView {
    current_filter_term: String,
    #[allow(dead_code)]
    link: ComponentLink<Self>,
    #[allow(dead_code)]
    fetch: FetchTask,
    location_list: LocationList,
    sorted_by: SortableBy,
}

pub enum Msg {
    ChangeSortMethod(SortableBy),
    FetchLocationListComplete(LocationList),
    FetchLocationListFailed,
    FilterInput(String),
}

impl Component for LocationListView {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_get_list_task(link.clone());

        LocationListView {
            current_filter_term: "".to_owned(),
            link,
            fetch: task,
            location_list: vec![],
            sorted_by: SortableBy::Name,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::ChangeSortMethod(sort_by) => {
                if self.sorted_by != sort_by {
                    self.sorted_by = sort_by;
                    true
                } else {
                    false
                }
            }
            Msg::FetchLocationListComplete(_location_list) => {
                self.location_list = _location_list;
                true // Indicate that the Component should re-render
            }
            Msg::FilterInput(input) => {
                self.current_filter_term = input;
                true
            }
            _ => {
                ConsoleService::new().error("Error in fetching resource!");
                false
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        let on_filter_input = self.link.callback(|input: InputData| Msg::FilterInput(input.value));
        let on_select = self.link.callback(|item| Msg::ChangeSortMethod(item));
        let entries = vec![SortableBy::Id, SortableBy::Name];
        let header = html! {
            <span>
                {Self::render_filter_dialogue(on_filter_input)}
                {Self::render_sort_dialogue(on_select, entries, self.sorted_by)}
            </span>
        };

        self.render_list(Some(header))
    }
}

#[derive(Clone, Copy, PartialEq)]
pub enum SortableBy {
    Id,
    Name,
}

impl fmt::Display for SortableBy {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SortableBy::Id => write!(f, "{}", "Id"),
            SortableBy::Name => write!(f, "{}", "Name"),
        }
    }
}


impl ListComponent for LocationListView {
    type Listable = LocationList;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<LocationList, Error>>>> {
        link.callback(|response: Response<Json<Result<LocationList, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return Msg::FetchLocationListComplete(body);
                }
            }
            Msg::FetchLocationListFailed
        })
    }

    fn get_endpoint() -> String {
        "/locations".to_owned()
    }

    fn render_list_entries(&self) -> Html {
        let location_list = Self::filter_list_by_term(&self.location_list, self.current_filter_term.as_str());
        let location_list = Self::sort_listable(&location_list, self.sorted_by);

        location_list.iter().map(|location|
            html! {
                    <LocationListEntry location=location />
                }).collect::<Html>()
    }
}

impl FilterableListComponent for LocationListView {
    fn filter_list_by_term(list_to_filter: &Self::Listable, filter_term: &str) -> Self::Listable {
        list_to_filter.iter()
            .filter(|item| item.name.to_lowercase().contains(filter_term.to_lowercase().as_str()))
            .map(|location| location.clone())
            .collect()
    }
}

impl SortableListComponent for LocationListView {
    type SortOptions = SortableBy;

    fn sort_listable(listable: &LocationList, sort_by: SortableBy) -> LocationList {
        let mut listable_copy = listable.clone();

        match sort_by {
            SortableBy::Id => { listable_copy.sort_by(|a, b| a.id.cmp(&b.id)); },
            SortableBy::Name => { listable_copy.sort_by(|a, b| a.name.partial_cmp(&b.name).unwrap()); },
        }

        listable_copy
    }
}