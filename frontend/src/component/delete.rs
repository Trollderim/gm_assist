use serde::Serialize;
use crate::BACKEND_URL;
use yew::prelude::*;
use yew::services::fetch::{Request, Response, FetchTask, FetchService};
use gm_assist::{UniquelyIdentifiable};
use yew::format::Json;
use anyhow::Error;
use serde::de::DeserializeOwned;

/// This trait is intended to provide generic functionality for all kinds of edit components
/// Edit components are components which can be used to edit properties of an object.
/// Currently, they are also used to create new objects.
pub trait DeleteComponent: Component {
    type Deletable: DeserializeOwned + Serialize + UniquelyIdentifiable;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Deletable, Error>>>>;

    fn get_endpoint() -> String;

    fn get_delete_variable(&self) -> &Self::Deletable;

    fn create_delete_task(&self, link: ComponentLink<Self>) -> FetchTask {
        let message = serde_json::to_string(self.get_delete_variable()).unwrap();

        let delete_request = Request::delete(BACKEND_URL.to_owned() +
            Self::get_endpoint().as_str())
            .header("Content-Type", "application/json")
            .header("Access-Control-Allow-Origin", "http://127.0.0.1:8000")
            .body(Ok(message))
            .expect("Could not build string");

        let callback = Self::create_callback(link);

        FetchService::new().fetch(delete_request, callback).unwrap()
    }
}