use yew::prelude::*;
use yew::services::fetch::{Response, FetchTask};
use gm_assist::{ID, UniquelyIdentifiable};
use yew::format::{Json, Nothing};
use anyhow::Error;
use crate::entities::{EntityList, Entities};
use crate::{Named, BACKEND_URL};
use yew::services::{FetchService, fetch::Request, ConsoleService};
use gm_assist::scene::SceneList;
use gm_assist::adventure::AdventureList;
use gm_assist::character::CharacterList;
use gm_assist::campaign::CampaignList;
use gm_assist::map::MapList;

/// This component is used to create an association between various components
///
/// It requires the specification of a reference entity, as well as the type of reference to fetch.
pub struct EntityChoose {
    link: ComponentLink<Self>,
    on_entity_chosen: Callback<ID>,
    on_already_associated_entity_chosen: Callback<ID>,
    entity_list: EntityList,
    entities_already_referenced: Vec<ID>,
    #[allow(dead_code)]
    fetch_task: FetchTask,
    reference_entity: Entities,
}

pub enum Msg {
    ChoseItem(ID),
    FetchItemListComplete(Result<String, Error>),
    FetchReferencedListSucceeded(Result<String, Error>),
    FetchItemListFailed,
}

#[derive (Properties, Clone)]
pub struct Props {
    pub entity: Entities,
    pub entity_type: EntityList,
    pub on_entity_chosen: Callback<ID>,
    pub on_already_associated_entity_chosen: Callback<ID>,
}

impl Component for EntityChoose {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let task = Self::create_get_list_task(&props.entity_type, link.clone());

        Self {
            link,
            on_entity_chosen: props.on_entity_chosen,
            on_already_associated_entity_chosen: props.on_already_associated_entity_chosen,
            entity_list: props.entity_type,
            entities_already_referenced: vec![],
            fetch_task: task,
            reference_entity: props.entity,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::ChoseItem(id) => {
                if self.entities_already_referenced.contains(&id) {
                    self.on_already_associated_entity_chosen.emit(id);
                } else {
                    self.on_entity_chosen.emit(id);
                }
                true
            },
            Msg::FetchItemListComplete(item_list) => {
                self.entity_list = self.extract_entity_list_from_result(item_list);
                self.fetch_task = self.create_get_referenced_list_task();

                true // Indicate that the Component should re-render
            },
            Msg::FetchReferencedListSucceeded(item_list) => {
                let entity_list = self.extract_entity_list_from_result(item_list);
                self.entities_already_referenced = entity_list.collect_ids();
                true
            }
            _ => {
                true
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        self.render_list(None)
    }
}

impl EntityChoose {
    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Result<String, Error>>> {
        link.callback(|response: Response<Result<String, Error>>| {
            let (meta, body) = response.into_parts();

            if meta.status.is_success() {
                return Msg::FetchItemListComplete(body);
            }

            Msg::FetchItemListFailed
        })
    }

    fn get_endpoint(entity_list: &EntityList) -> String {
        entity_list.get_endpoint_name().to_owned()
    }

    fn render_list_entries(&self) -> Html {
        let on_entity_click = self.link.callback(|id| Msg::ChoseItem(id));

        match &self.entity_list {
            EntityList::AdventureList(adventure_list) => {adventure_list.iter().map(|entity| {
                let already_chosen = self.entities_already_referenced.contains(&entity.id);

                html! {
                <EntityChooseElement already_chosen=already_chosen entity=Entities::Adventure(entity.clone()) on_click=on_entity_click.clone() />
            }}).collect::<Html>()
            },
            EntityList::CampaignList(campaign_list) => {campaign_list.iter().map(|entity| {
                let already_chosen = self.entities_already_referenced.contains(&entity.id);
                html! {
                <EntityChooseElement already_chosen=already_chosen entity=Entities::Campaign(entity.clone()) on_click=on_entity_click.clone() />
            }}).collect::<Html>()
            },
            EntityList::CharacterList(character_list) => {character_list.iter().map(|entity| {
                let already_chosen = self.entities_already_referenced.contains(&entity.id);
                html! {
                <EntityChooseElement already_chosen=already_chosen entity=Entities::Character(entity.clone()) on_click=on_entity_click.clone() />
            }}).collect::<Html>()
            },
            EntityList::MapList(map_list) => {map_list.iter().map(|entity| {
                let already_chosen = self.entities_already_referenced.contains(&entity.id);
                html! {
                <EntityChooseElement already_chosen=already_chosen entity=Entities::Map(entity.clone()) on_click=on_entity_click.clone() />
            }}).collect::<Html>()
            },
            EntityList::SceneList(scene_list) => {scene_list.iter().map(|entity| {
                let already_chosen = self.entities_already_referenced.contains(&entity.id);
                html! {
                <EntityChooseElement already_chosen=already_chosen entity=Entities::Scene(entity.clone()) on_click=on_entity_click.clone() />
            }}).collect::<Html>()
            },
        }
    }

    fn create_get_list_task(entity_list: &EntityList, link: ComponentLink<Self>) -> FetchTask {
        let get_request = Request::get(BACKEND_URL.to_owned() + Self::get_endpoint(entity_list).as_str())
            .body(Nothing)
            .expect("Failed to build get request!");

        let callback = Self::create_callback(link);

        FetchService::new().fetch(get_request, callback).unwrap()
    }

    fn create_get_referenced_list_task(&self) -> FetchTask {
        let get_request = Request::get(BACKEND_URL.to_owned()
                + self.reference_entity.get_endpoint_name()
                + format!("/{}", self.reference_entity.get_id().value()).as_str()
                + self.entity_list.get_endpoint_name())
            .body(Nothing)
            .expect("Failed to build get request!");

        let callback = self.create_referenced_list_callback();

        FetchService::new().fetch(get_request, callback).unwrap()
    }

    fn extract_entity_list_from_result(&self, item_list: Result<String, Error>) -> EntityList {
        match self.entity_list {
            EntityList::AdventureList(_) => {
                let json = Json::<Result<AdventureList, Error>>::from(item_list);
                let result = gm_assist::adventure::AdventureList::from(json.0.unwrap());
                EntityList::AdventureList(result)
            },
            EntityList::CampaignList(_) => {
                let json = Json::<Result<CampaignList, Error>>::from(item_list);
                let result = gm_assist::campaign::CampaignList::from(json.0.unwrap());
                EntityList::CampaignList(result)
            },
            EntityList::CharacterList(_) => {
                let json = Json::<Result<CharacterList, Error>>::from(item_list);
                let result = gm_assist::character::CharacterList::from(json.0.unwrap());
                EntityList::CharacterList(result)
            },
            EntityList::MapList(_) => {
                let json = Json::<Result<MapList, Error>>::from(item_list);
                let result = gm_assist::map::MapList::from(json.0.unwrap());
                EntityList::MapList(result)
            },
            EntityList::SceneList(_) => {
                let json = Json::<Result<SceneList, Error>>::from(item_list);
                let result = gm_assist::scene::SceneList::from(json.0.unwrap());
                EntityList::SceneList(result)
            },
        }
    }

    fn create_referenced_list_callback(&self) -> Callback<Response<Result<String, Error>>> {
        self.link.callback(|response: Response<Result<String, Error>>| {
            let (meta, body) = response.into_parts();

            if meta.status.is_success() {
                return Msg::FetchReferencedListSucceeded(body);
            }

            Msg::FetchItemListFailed
        })
    }

    fn render_list(&self, header: Option<Html>) -> Html{
        let header = if header.is_some() {
            html! {
                <div class="list-header">
                    {header.unwrap()}
                </div>
            }
        } else {
            html! {

            }
        };

        html! {
            <div class="location-list-view list">
                {header}
                <ul class="list-body location-list">
                        {self.render_list_entries()}
                </ul>
            </div>
        }
    }
}

struct EntityChooseElement {
    chosen: bool,
    link: ComponentLink<Self>,
    entity: Entities,
    on_click: Callback<ID>,
}

#[derive(Properties, Clone)]
struct ElementProps {
    pub already_chosen: bool,
    pub entity: Entities,
    pub on_click: Callback<ID>
}

enum ElementMsg {
    EntityPicked,
}

impl Component for EntityChooseElement {
    type Message = ElementMsg;
    type Properties = ElementProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            chosen: props.already_chosen,
            link,
            entity: props.entity,
            on_click: props.on_click,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            ElementMsg::EntityPicked => {
                self.on_click.emit(self.entity.get_id());
                true
            },
        }
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        self.chosen = _props.already_chosen;
        true
    }

    fn view(&self) -> Html {
        let on_click = self.link.callback(|_| ElementMsg::EntityPicked);
        let chosen_class = if self.chosen {
            "chosen"
        } else {
            ""
        };

        let classes = format!("entity choose {}", chosen_class);


        ConsoleService::new().info(format!("Classes: {}", classes.as_str()).as_str());

        html! {
            <div class={classes} onclick=on_click>
                <p>{self.entity.name()}</p>
            </div>
        }
    }
}