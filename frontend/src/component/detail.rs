use yew::prelude::*;

use crate::BACKEND_URL;
use yew::format::{Nothing, Json};
use yew::services::fetch::{Request, Response, FetchService, FetchTask};
use serde::Serialize;
use serde::de::DeserializeOwned;
use anyhow::Error;
use gm_assist::ID;

pub trait DetailComponent: Component {
    type Detaillable: Serialize + DeserializeOwned;

    fn create_callback(link: ComponentLink<Self>) -> Callback<Response<Json<Result<Self::Detaillable, Error>>>>;

    fn create_get_task_single_item(link: ComponentLink<Self>, id: &ID) -> FetchTask {
        let get_request = Request::get(BACKEND_URL.to_owned() + Self::get_endpoint(id).as_str())
            .body(Nothing)
            .expect("Failed to build get request!");

        let callback = Self::create_callback(link);

        FetchService::new().fetch(get_request, callback).unwrap()
    }

    fn get_endpoint(id: &ID) -> String;
}