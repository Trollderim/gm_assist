use yew_router::prelude::*;

#[derive(Clone, Switch)]
pub enum RouterTarget {
    #[to = "/#error"]
    Error,

    #[to = "/#loading"]
    Loading,

    #[to = "/#adventure/{id}"]
    AdventureDetail(i32),

    #[to = "/#adventure"]
    AdventureListView,

    #[to = "/#campaign/{id}"]
    CampaignDetail(i32),

    #[to = "/#campaign"]
    CampaignListView,

    #[to = "/#character/{id}"]
    CharacterDetail(i32),

    #[to = "/#character"]
    CharacterListView,

    #[to = "/#help"]
    HelpView,

    #[to = "/#location/{id}"]
    LocationDetail(i32),

    #[to = "/#location"]
    LocationListView,

    #[to = "/#map/{id}"]
    MapSingleView(i32),

    #[to = "/#map"]
    MapListView,

    #[to = "/#scene/{id}"]
    SceneDetail(i32),

    #[to = "/#scene"]
    SceneListView,

    #[to = "/#event/{id}"]
    TimelineDetail(i32),

    #[to = "/#timeline"]
    TimelineListView,
}