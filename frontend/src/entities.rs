use gm_assist::scene::{Scene, SceneList};
use gm_assist::character::{Character, CharacterList};
use gm_assist::campaign::{Campaign, CampaignList};
use gm_assist::adventure::{Adventure, AdventureList};
use gm_assist::map::{MapList, MapListEntry};

use serde::{Deserialize, Serialize};
use gm_assist::{UniquelyIdentifiable, ID};
use crate::Named;

#[derive(Clone)]
pub enum Entities {
    Adventure(Adventure),
    Campaign(Campaign),
    Character(Character),
    Map(MapListEntry),
    Scene(Scene),
}

impl Entities {
    pub fn get_endpoint_name(&self) -> &str {
        match self {
            Entities::Adventure(_) => {"/adventure"},
            Entities::Campaign(_) => {"/campaign"},
            Entities::Character(_) => {"/character"},
            Entities::Scene(_) => {"/scene"},
            Entities::Map(_) => {"/map"}
        }
    }
}

impl Named for Entities {
    fn name(&self) -> &str {
        match self {
            Entities::Adventure(adventure) => {adventure.name.as_str()},
            Entities::Campaign(campaign) => {campaign.name.as_str()},
            Entities::Character(character) => {character.name.as_str()},
            Entities::Scene(scene) => {scene.name.as_str()},
            Entities::Map(map) => {map.name.as_str()}
        }
    }
}

impl UniquelyIdentifiable for Entities {
    fn get_id(&self) -> ID {
        match self {
            Entities::Adventure(adventure) => {adventure.id},
            Entities::Campaign(campaign) => {campaign.id},
            Entities::Character(character) => {character.id},
            Entities::Scene(scene) => {scene.id},
            Entities::Map(map) => {map.id}
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub enum EntityList {
    AdventureList(AdventureList),
    CampaignList(CampaignList),
    CharacterList(CharacterList),
    MapList(MapList),
    SceneList(SceneList),
}

impl EntityList {
    pub fn get_endpoint_name(&self) -> &str {
        match self {
            EntityList::AdventureList(_) => {"/adventures"}
            EntityList::CampaignList(_) => {"/campaigns"}
            EntityList::CharacterList(_) => {"/characters"}
            EntityList::MapList(_) => {"/map/all"}
            EntityList::SceneList(_) => {"/scenes"}
        }
    }

    pub fn collect_ids(&self) -> Vec<ID> {
        match self {
            EntityList::AdventureList(adventures) => {
                adventures.iter().map(|adventure| adventure.id).collect()
            }
            EntityList::CampaignList(campaigns) => {
                campaigns.iter().map(|campaign| campaign.id).collect()
            }
            EntityList::CharacterList(characters) => {
                characters.iter().map(|character| character.id).collect()
            }
            EntityList::MapList(maps) => {
                maps.iter().map(|map_entry| map_entry.id).collect()
            }
            EntityList::SceneList(scenes) => {
                scenes.iter().map(|scene| scene.id).collect()
            }
        }
    }
}