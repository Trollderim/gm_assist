use gm_assist::ID;
use yew::services::fetch::{Request, Response, FetchService, FetchTask};
use yew::prelude::*;
use crate::BACKEND_URL;
use yew::format::Nothing;
use anyhow::Error;

pub enum AvailableActions {
    Add,
    Remove
}

fn create_association_task(action: AvailableActions, callback: Callback<Response<Result<String, Error>>>, endpoint: &String) -> FetchTask {
    let request = match action {
        AvailableActions::Add => {Request::put(endpoint)},
        AvailableActions::Remove => {Request::delete(endpoint)},
    };
    let request = request.header("Content-Type", "application/json")
        .header("Access-Control-Allow-Origin", "http://127.0.0.1:8000")
        .body(Nothing)
        .expect("Could not build string");

    FetchService::new().fetch(request, callback).unwrap()
}

pub fn create_adventure_scene_association_task(action: AvailableActions, adventure_id: ID, scene_id: ID, callback: Callback<Response<Result<String, Error>>>)
                                               -> FetchTask {
    let endpoint = format!("{}/adventure_scene?adventure_id_={}&scene_id_={}",
                           BACKEND_URL.to_owned(),
                           adventure_id.value(),
                           scene_id.value());
    create_association_task(action, callback, &endpoint)
}

pub fn create_campaign_adventure_association_task(action: AvailableActions, campaign_id: ID, adventure_id: ID, callback: Callback<Response<Result<String, Error>>>)
                                               -> FetchTask {
    let endpoint = format!("{}/campaign_adventure?campaign_id_={}&adventure_id_={}",
                           BACKEND_URL.to_owned(),
                           campaign_id.value(),
                           adventure_id.value());
    create_association_task(action, callback, &endpoint)
}

pub fn create_scene_map_association_task(action: AvailableActions, scene_id: ID, map_id: ID, callback: Callback<Response<Result<String, Error>>>)
    -> FetchTask {
    let endpoint = format!("{}/scene_map?scene_id_={}&map_id_={}",
                            BACKEND_URL.to_owned(),
                            scene_id.value(),
                            map_id.value());
    create_association_task(action, callback, &endpoint)
}

pub fn create_scene_character_association_task(action: AvailableActions, scene_id: ID, character_id: ID, callback: Callback<Response<Result<String, Error>>>)
    -> FetchTask {
    let endpoint = format!("{}/scene_character?scene_id_={}&character_id_={}",
                                       BACKEND_URL.to_owned(),
                                       scene_id.value(),
                                       character_id.value());
    create_association_task(action, callback, &endpoint)
}