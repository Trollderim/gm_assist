pub mod association;

#[macro_export]
macro_rules! create_default_callback {
    ($link: expr, $success_message: expr, $failure_message: expr) => {
        $link.callback(|response: Response<Result<String, Error>>| {
            if let (meta, Ok(_)) = response.into_parts() {
                if meta.status.is_success() {
                    return $success_message;
                }
            }
            return $failure_message;
        });
    }
}

#[macro_export]
macro_rules! create_default_callback_with_return_parameter {
    ($link: expr, $success_message: expr, $failure_message: expr, $type: ty) => {
        $link.callback(|response: Response<Json<Result<$type, Error>>>| {
            if let (meta, Json(Ok(body))) = response.into_parts() {
                if meta.status.is_success() {
                    return $success_message(body);
                }
            }
            return $failure_message;
        });
    }
}