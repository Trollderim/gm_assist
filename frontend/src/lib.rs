#![recursion_limit="512"]

mod component;
mod api;

pub mod agent;

pub mod entities;

pub mod markdown;

pub mod route;

pub mod string;

pub const BACKEND_URL: &str = "http://127.0.0.1:8080";

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn run_app() -> Result<(), JsValue> {
    yew::start_app::<component::application_root::ApplicationRoot>();

    Ok(())
}

pub trait Named {
    fn name(&self) -> &str;
}