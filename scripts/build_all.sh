#!/bin/bash

cargo build --package backend --bin backend

cd frontend && wasm-pack build --target web && rollup ./main.js --format iife --file ./pkg/frontend.js