# GM Assist
GM Assist is a self-hosted campaign managing tool.
It allows you to create and store all relevant information within your campaign world.

## Current features
* Create Maps, associated with images
* Create and edit annotations on those maps

## Build
### Requirements
* Rust nightly
* Wasm-Pack
* rollup
* Python
* Mysql

### Steps
* Build the backend with cargo
* Build the frontend with wasm-pack and roll it up

### Run
* Run the backend with your database variables set
* Serve the frontend with a simple http server from your frontend folder
