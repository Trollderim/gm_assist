CREATE TABLE `scenes` (
  `id` INTEGER PRIMARY KEY NOT NULL,
  `name` TEXT NOT NULL,
  `description` TEXT NOT NULL
);

CREATE TABLE `adventure_scenes` (
    `id` INTEGER PRIMARY KEY NOT NULL,
    `adventure_id` INTEGER NOT NULL,
    `scene_id` INTEGER NOT NULL,
    FOREIGN KEY (`adventure_id`) REFERENCES adventures(`id`),
    FOREIGN KEY (`scene_id`) REFERENCES scenes(`id`)
);