-- Your SQL goes here

CREATE TABLE `events` (
    `id` INTEGER PRIMARY KEY NOT NULL,
    `name` TEXT NOT NULL,
    `description` TEXT NOT NULL,
    `start_year` BIGINT NOT NULL,
    `start_month` SMALLINT,
    `start_day` SMALLINT,
    `end_year` BIGINT,
    `end_month` SMALLINT,
    `end_day` SMALLINT
);
