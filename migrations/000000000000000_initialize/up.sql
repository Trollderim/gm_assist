-- Your SQL goes here

CREATE TABLE `maps` (
  `id` INTEGER PRIMARY KEY NOT NULL,
  `name` TEXT NOT NULL
);

CREATE TABLE `annotations` (
  `id` INTEGER PRIMARY KEY NOT NULL,
  `x_left` INT NOT NULL,
  `x_right` INT NOT NULL,
  `y_top` INT NOT NULL,
  `y_bottom` INT NOT NULL,
  `content` TEXT NOT NULL,
  `map_id` INTEGER,
  FOREIGN KEY (`map_id`) REFERENCES maps(`id`)
);