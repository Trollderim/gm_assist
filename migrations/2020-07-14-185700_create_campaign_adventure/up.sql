CREATE TABLE `campaign_adventures` (
    `id` INTEGER PRIMARY KEY NOT NULL,
    `campaign_id` INTEGER NOT NULL,
    `adventure_id` INTEGER NOT NULL,
    FOREIGN KEY (`campaign_id`) REFERENCES campaigns(`id`),
    FOREIGN KEY (`adventure_id`) REFERENCES adventures(`id`)
);