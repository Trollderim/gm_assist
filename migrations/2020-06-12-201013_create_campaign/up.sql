CREATE TABLE `campaigns` (
    `id` INTEGER PRIMARY KEY NOT NULL,
    `name` TEXT NOT NULL,
    `description` TEXT NOT NULL
);
