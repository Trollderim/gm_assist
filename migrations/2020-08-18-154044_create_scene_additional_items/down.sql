-- This file should undo anything in `up.sql`

-- Here you can drop column
CREATE TABLE IF NOT EXISTS `new_scenes` (
    `id` INTEGER PRIMARY KEY NOT NULL,
    `name` TEXT NOT NULL,
    `description` TEXT NOT NULL
);
-- copy data from the table to the new_table
INSERT INTO `new_scenes`(id, name, description)
SELECT id, name, description
FROM scenes;

-- drop the table
DROP TABLE scenes;

-- rename the new_table to the table
ALTER TABLE new_scenes RENAME TO scenes;

DROP TABLE `scene_maps`;

DROP TABLE `scene_characters`;