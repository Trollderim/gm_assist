-- Your SQL goes here

ALTER TABLE `scenes` ADD COLUMN `check_description` TEXT NOT NULL DEFAULT '';
ALTER TABLE `scenes` ADD COLUMN `treasure_description` TEXT NOT NULL DEFAULT '';

CREATE TABLE `scene_maps` (
    `id` INTEGER PRIMARY KEY NOT NULL,
    `scene_id` INTEGER NOT NULL,
    `map_id` INTEGER NOT NULL,
    FOREIGN KEY (`scene_id`) REFERENCES scenes(`id`),
    FOREIGN KEY (`map_id`) REFERENCES maps(`id`)
);

CREATE TABLE `scene_characters` (
    `id` INTEGER PRIMARY KEY NOT NULL,
    `scene_id` INTEGER NOT NULL,
    `character_id` INTEGER NOT NULL,
    FOREIGN KEY (`scene_id`) REFERENCES scenes(`id`),
    FOREIGN KEY (`character_id`) REFERENCES characters(`id`)
);