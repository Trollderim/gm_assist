CREATE TABLE `locations` (
    `id` INTEGER PRIMARY KEY NOT NULL,
    `name` TEXT NOT NULL,
    `description` TEXT NOT NULL,
    parent_id INTEGER DEFAULT NULL,
    FOREIGN KEY (parent_id) REFERENCES locations (id)
        ON DELETE CASCADE ON UPDATE CASCADE
);